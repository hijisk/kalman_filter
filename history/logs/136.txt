This is the start of the log file generated at 09_02_14h49m18s

15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 400 particles...
Particle 0:
eta 4.76689016266335, phi 2.6293619468601372,
p 40.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.00833935]
 [-0.01483102]
 [ 0.00749521]]

Simulating particle
Particle track 0 is saved
Initiating Kalman filtering for particle: 0
z: 0.6
The prediction: [[ 0.0890729 ]
 [-0.00933619]]
The measurement: [-0.0088803]
The filtered state: [[ 0.08880875]
 [-0.00888076]]
z: 0.65
The prediction: [[ 0.0900331]
 [-0.0096333]]
The measurement: [0.00547592]
The filtered state: [[ 0.00546684]
 [-0.01008618]]
z: 0.7
The prediction: [[ 0.00478283]
 [-0.01126281]]
The measurement: [-0.01026707]
The filtered state: [[ 0.00524104]
 [-0.0105985 ]]
z: 0.75
The prediction: [[ 0.00488995]
 [-0.01163144]]
The measurement: [0.0061736]
The filtered state: [[ 0.00591671]
 [-0.01126676]]
z: 2.33
The prediction: [[ 0.0145659 ]
 [-0.03760851]]
The measurement: [0.01871065]
The filtered state: [[ 0.01869993]
 [-0.03506107]]
z: 2.37
The prediction: [[ 0.0188706 ]
 [-0.03568252]]
The measurement: [-0.03589311]
The filtered state: [[ 0.01885834]
 [-0.03570885]]
z: 2.6
The prediction: [[ 0.02051153]
 [-0.03919609]]
The measurement: [0.02072451]
The filtered state: [[ 0.02063413]
 [-0.03911905]]
z: 2.64
The prediction: [[ 0.02096287]
 [-0.03973896]]
The measurement: [-0.03918912]
The filtered state: [[ 0.02098238]
 [-0.03968643]]
z: 7.86
The prediction: [[-0.01691987]
 [-0.11859939]]
The measurement: [-0.01602365]
The filtered state: [[-0.01621204]
 [-0.11824008]]
z: 7.87
The prediction: [[-0.0164076 ]
 [-0.11839069]]
The measurement: [-0.11676296]
The filtered state: [[-0.01634211]
 [-0.11839721]]
z: 7.91
The prediction: [[-0.01719626]
 [-0.11901499]]
The measurement: [-0.01715014]
The filtered state: [[-0.01717597]
 [-0.11900462]]
z: 7.92
The prediction: [[-0.01738599]
 [-0.11915528]]
The measurement: [-0.11970612]
The filtered state: [[-0.0173985 ]
 [-0.11914779]]
z: 7.98
The prediction: [[-0.01864275]
 [-0.12006676]]
The measurement: [-0.01872367]
The filtered state: [[-0.01866762]
 [-0.12007924]]
z: 7.99
The prediction: [[-0.01883261]
 [-0.12022989]]
The measurement: [-0.11744104]
The filtered state: [[-0.01878905]
 [-0.12028027]]
z: 8.03
The prediction: [[-0.01968769]
 [-0.12088352]]
The measurement: [-0.01974987]
The filtered state: [[-0.01970239]
 [-0.12089074]]
z: 8.04
The prediction: [[-0.01997503]
 [-0.12104157]]
The measurement: [-0.12271704]
The filtered state: [[-0.01999462]
 [-0.1210067 ]]
z: 8.54
The prediction: [[-0.03111543]
 [-0.12854323]]
The measurement: [-0.03056824]
The filtered state: [[-0.03100034]
 [-0.12852185]]
z: 8.55
The prediction: [[-0.03122957]
 [-0.12868762]]
The measurement: [-0.12603638]
The filtered state: [[-0.03121935]
 [-0.12876247]]
z: 8.59
The prediction: [[-0.03194788]
 [-0.12936602]]
The measurement: [-0.03157716]
The filtered state: [[-0.03188303]
 [-0.12935579]]
z: 8.6
The prediction: [[-0.03211315]
 [-0.12950668]]
The measurement: [-0.13005696]
The filtered state: [[-0.03211463]
 [-0.12949025]]
z: 8.66
The prediction: [[-0.03360823]
 [-0.13041034]]
The measurement: [-0.03320185]
The filtered state: [[-0.03354715]
 [-0.13040497]]
z: 8.67
The prediction: [[-0.0337763]
 [-0.1305558]]
The measurement: [-0.12558269]
The filtered state: [[-0.03377041]
 [-0.13071422]]
z: 8.71
The prediction: [[-0.03484079]
 [-0.13133411]]
The measurement: [-0.03408254]
The filtered state: [[-0.03474102]
 [-0.1313318 ]]
z: 8.72
The prediction: [[-0.03495779]
 [-0.131483  ]]
The measurement: [-0.1299183]
The filtered state: [[-0.03495757]
 [-0.13153572]]
z: 9.23
The prediction: [[-0.04536514]
 [-0.13925221]]
The measurement: [-0.0460727]
The filtered state: [[-0.04544623]
 [-0.13919471]]
z: 9.24
The prediction: [[-0.04568336]
 [-0.13934591]]
The measurement: [-0.13592047]
The filtered state: [[-0.04571326]
 [-0.13949848]]
z: 9.28
The prediction: [[-0.04656043]
 [-0.1401046 ]]
The measurement: [-0.04698463]
The filtered state: [[-0.04660339]
 [-0.14006811]]
z: 9.29
The prediction: [[-0.0469093 ]
 [-0.14021961]]
The measurement: [-0.13706179]
The filtered state: [[-0.04693876]
 [-0.14037264]]
z: 9.35
The prediction: [[-0.04819517]
 [-0.1412984 ]]
The measurement: [-0.04855827]
The filtered state: [[-0.04822758]
 [-0.14126302]]
z: 9.36
The prediction: [[-0.0484079 ]
 [-0.14141475]]
The measurement: [-0.13977745]
The filtered state: [[-0.04842504]
 [-0.14150241]]
z: 9.4
The prediction: [[-0.04937991]
 [-0.1421251 ]]
The measurement: [-0.04958713]
The filtered state: [[-0.04939637]
 [-0.1421032 ]]
z: 9.41
The prediction: [[-0.04956438]
 [-0.14225506]]
The measurement: [-0.13511102]
The filtered state: [[-0.04964586]
 [-0.14267696]]
Particle 0:
eta 4.724207991783837, phi 2.6062812794187242,
p 38.57014928125688, q 1.60217662e-19,
brem = False
output State:
[[-0.04184713]
 [ 0.00113712]
 [ 0.00905794]
 [-0.01527287]
 [ 0.00777307]]
Data added to list for particle: 0
Particle 1:
eta 4.395077879150845, phi 0.9587609461352316,
p 40.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.02019976]
 [ 0.01417928]
 [-0.00749521]]

Simulating particle
Data added to list for particle: 1
data saved to data/toy_output/136