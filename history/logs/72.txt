This is the start of the log file generated at 08_13_14h26m54s

15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 4 particles...
Particle 0:
eta 4.928805314801682, phi 2.824876199456018,
p 50.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.00450698]
 [-0.0137513 ]
 [ 0.00599617]]

Simulating particle
Particle track 0 is saved
Initiating Kalman filtering for particle: 0
Particle 0:
eta 4.937227562793515, phi 2.8029989962264827,
p 49.43064929052623, q 1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.0003505148042045486]
y:   [-0.00011706865602872593]
tx:  [0.00476639694711691]
ty:  [-0.013534927139689977]
q/p: [0.006065234900909093]]
Covariance matrix:
[[ 3.4e-06 -1.4e-09  3.1e-06  1.4e-09  2.3e-06]
 [-1.4e-09  9.9e-06  6.3e-10 -2.8e-06  1.2e-10]
 [ 3.1e-06  6.3e-10 -3.6e-06 -6.0e-10 -2.2e-06]
 [ 1.4e-09 -2.8e-06 -6.0e-10  2.7e-06 -1.1e-10]
 [ 2.3e-06  1.2e-10 -2.2e-06 -1.1e-10 -1.2e-06]]
Data added to list for particle: 0
Particle 1:
eta 4.603239617885872, phi 2.2630187307626977,
p 50.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.01542789]
 [-0.01279094]
 [-0.00599617]]

Simulating particle
Particle track 1 is saved
Initiating Kalman filtering for particle: 1
Particle 1:
eta 4.616381455002919, phi 2.2731666932221866,
p 50.18970136965455, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.00012858251663486217]
y:   [2.9474875856334307e-05]
tx:  [0.015097538141713602]
ty:  [-0.012777779008659845]
q/p: [-0.00597350633834944]]
Covariance matrix:
[[ 6.8e-06  1.6e-08 -2.5e-06 -1.5e-08 -9.2e-07]
 [ 1.6e-08  9.9e-06 -2.5e-10 -2.8e-06  5.6e-11]
 [-2.5e-06 -2.5e-10  2.2e-06  2.4e-10  1.0e-06]
 [-1.5e-08 -2.8e-06  2.4e-10  2.7e-06 -5.4e-11]
 [-9.2e-07  5.6e-11  1.0e-06 -5.4e-11  5.9e-07]]
Data added to list for particle: 1
Particle 2:
eta 3.612731941259116, phi 0.7559677693962069,
p 90.0, q 1.60217662e-19,
brem = False
input State:
[[0.        ]
 [0.        ]
 [0.03704046]
 [0.03928747]
 [0.00333121]]

Simulating particle
Particle track 2 is saved
Initiating Kalman filtering for particle: 2
Particle 2:
eta 3.6207268671474417, phi 0.756354093356327,
p 90.45075898664496, q 1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.0005150811320574517]
y:   [-0.0001266277632375144]
tx:  [0.03676013898165723]
ty:  [0.038959977960358536]
q/p: [0.0033146045717069514]]
Covariance matrix:
[[ 2.5e-06 -7.3e-10  5.9e-07  1.3e-09  6.5e-07]
 [-7.3e-10  3.7e-06 -3.8e-10 -1.3e-06 -5.5e-11]
 [ 5.9e-07 -3.8e-10 -1.4e-06  6.6e-10 -1.0e-06]
 [ 1.3e-09 -1.3e-06  6.6e-10  2.3e-06  9.6e-11]
 [ 6.5e-07 -5.5e-11 -1.0e-06  9.6e-11 -6.0e-07]]
Data added to list for particle: 2
Particle 3:
eta 2.470682850546522, phi 2.484522746694267,
p 90.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.10400131]
 [-0.13481792]
 [-0.00333121]]

Simulating particle
Particle track 3 is saved
Initiating Kalman filtering for particle: 3
Particle 3:
eta 2.4787710468620494, phi 2.4851622954598116,
p 92.81991483368718, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.0014410920960700973]
y:   [0.0005074531419457841]
tx:  [0.10306607176931909]
ty:  [-0.13378238489436664]
q/p: [-0.0032300018782465785]]
Covariance matrix:
[[ 4.9e-06  9.5e-09 -1.1e-06 -2.6e-08 -2.7e-07]
 [ 9.5e-09  3.9e-06 -4.5e-10 -8.1e-07  7.8e-11]
 [-1.1e-06 -4.5e-10  1.3e-06  1.2e-09  6.0e-07]
 [-2.6e-08 -8.1e-07  1.2e-09  2.2e-06 -2.1e-10]
 [-2.7e-07  7.8e-11  6.0e-07 -2.1e-10  3.5e-07]]
Data added to list for particle: 3
data saved to data/toy_output/72