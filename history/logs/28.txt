This is the start of the log file generated at 08_06_14h07m18s

15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 470 particles...
Particle 0:
eta 3.2022502902759338, phi 0.40003851879051455,
p 3.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.03173111]
 [-0.07504308]
 [-0.09993617]]

Simulating particle
Particle track 0 is saved
Initiating Kalman filtering for particle: 0
Particle 0:
eta 3.2221549660747697, phi 0.472313053027325,
p 3.3116363284831887, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.003954611865346581]
y:   [-0.0077407961752150225]
tx:  [-0.029191160226764226]
ty:  [-0.07433910085376264]
q/p: [-0.09053183064603494]]
Covariance matrix:
[[ 4.4e-06  6.4e-11 -2.9e-06 -1.1e-09 -9.1e-07]
 [ 6.4e-11  3.9e-06 -5.5e-10 -1.6e-06 -3.3e-09]
 [-2.9e-06 -5.5e-10  8.6e-06  7.8e-09  2.1e-06]
 [-1.1e-09 -1.6e-06  7.8e-09  1.1e-05  7.9e-09]
 [-9.1e-07 -3.3e-09  2.1e-06  7.9e-09  2.4e-06]]
Data added to list for particle: 0
Particle 1:
eta 2.3049871000890154, phi -0.34873446025400356,
p 3.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.06886311]
 [-0.18939511]
 [-0.09993617]]

Simulating particle
Particle track 1 is saved
Initiating Kalman filtering for particle: 1
Particle 1:
eta 2.2985553069944706, phi -0.21840303593999405,
p 3.0101314244822537, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.0019555412369399116]
y:   [-0.008810998758772323]
tx:  [0.07141127248555429]
ty:  [-0.18986726023823652]
q/p: [-0.09959980378699393]]
Covariance matrix:
[[ 5.4e-06 -6.9e-10 -2.8e-06  7.4e-09 -9.0e-07]
 [-6.9e-10  5.1e-06  5.1e-09 -1.5e-06 -1.4e-08]
 [-2.8e-06  5.1e-09  8.8e-06 -6.4e-08  2.1e-06]
 [ 7.4e-09 -1.5e-06 -6.4e-08  1.2e-05  5.7e-08]
 [-9.0e-07 -1.4e-08  2.1e-06  5.7e-08  2.6e-06]]
Data added to list for particle: 1
Particle 2:
eta 1.936807673737285, phi 1.2656204693083335,
p 3.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.28084122]
 [ 0.08846963]
 [-0.09993617]]

Simulating particle
Particle track 2 is saved
Initiating Kalman filtering for particle: 2
Particle 2:
eta 1.9196746305116257, phi 1.1558253475364855,
p 2.1831696677332264, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.009984805719344817]
y:   [0.004398846783062768]
tx:  [0.2862991277462246]
ty:  [0.08880707890977765]
q/p: [-0.13732716411491125]]
Covariance matrix:
[[ 7.8e-06  8.2e-10 -3.6e-06 -1.3e-08 -1.1e-06]
 [ 8.2e-10  7.5e-06 -6.7e-09 -1.6e-06  1.2e-08]
 [-3.6e-06 -6.7e-09  1.0e-05  1.5e-07  2.1e-06]
 [-1.3e-08 -1.6e-06  1.5e-07  1.3e-05 -7.3e-08]
 [-1.1e-06  1.2e-08  2.1e-06 -7.3e-08  2.8e-06]]
Data added to list for particle: 2
Particle 3:
eta 1.8891407535212488, phi -1.3884684273915149,
p 3.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.30434879]
 [-0.05611447]
 [-0.09993617]]

Simulating particle
Particle track 3 is saved
Initiating Kalman filtering for particle: 3
Particle 3:
eta 1.869334754788465, phi -1.278682361683068,
p 2.123186182429337, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.009590476494699095]
y:   [-0.002884014115732338]
tx:  [0.3111101513593094]
ty:  [-0.05519273940661059]
q/p: [-0.1412068813053681]]
Covariance matrix:
[[ 1.5e-05 -6.9e-10 -6.1e-06  8.7e-09 -1.9e-06]
 [-6.9e-10  7.5e-06  4.8e-09 -1.7e-06 -8.3e-09]
 [-6.1e-06  4.8e-09  1.1e-05 -1.0e-07  2.3e-06]
 [ 8.7e-09 -1.7e-06 -1.0e-07  1.4e-05  5.3e-08]
 [-1.9e-06 -8.3e-09  2.3e-06  5.3e-08  2.9e-06]]
Data added to list for particle: 3
Particle 4:
eta 3.900683555555604, phi 0.715662868568105,
p 3.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.02655487]
 [-0.03054308]
 [-0.09993617]]

Simulating particle
Particle track 4 is saved
Initiating Kalman filtering for particle: 4
Particle 4:
eta 3.9343653883719236, phi 0.7302054408391406,
p 3.0367088412564436, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.005465875910257818]
y:   [-0.006105162281965948]
tx:  [-0.024929368789059284]
ty:  [-0.030162493949129374]
q/p: [-0.09872810167979447]]
Covariance matrix:
[[ 4.2e-06  7.0e-11 -3.3e-06 -4.4e-10 -1.1e-06]
 [ 7.0e-11  4.6e-06 -3.5e-10 -2.8e-06 -7.6e-10]
 [-3.3e-06 -3.5e-10  8.5e-06  2.5e-09  2.4e-06]
 [-4.4e-10 -2.8e-06  2.5e-09  9.3e-06  1.1e-09]
 [-1.1e-06 -7.6e-10  2.4e-06  1.1e-09  2.4e-06]]
Data added to list for particle: 4
Particle 5:
eta 3.938900129067886, phi -0.0643454081379582,
p 3.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.00250478]
 [ 0.03887339]
 [-0.09993617]]

Simulating particle
Particle track 5 is saved
Initiating Kalman filtering for particle: 5
Particle 5:
eta 3.9927398075552207, phi -0.17997706363778757,
p 2.9593234360807767, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.0016547708350808433]
y:   [0.009094854249664764]
tx:  [0.0008958697037546766]
ty:  [0.03689988542105489]
q/p: [-0.10130981142384111]]
Covariance matrix:
[[ 5.5e-06  1.6e-10 -4.3e-06 -5.1e-10 -1.5e-06]
 [ 1.6e-10  4.4e-06 -5.3e-10 -2.5e-06  1.2e-09]
 [-4.3e-06 -5.3e-10  9.5e-06  1.6e-09  2.7e-06]
 [-5.1e-10 -2.5e-06  1.6e-09  9.1e-06 -2.5e-09]
 [-1.5e-06  1.2e-09  2.7e-06 -2.5e-09  2.5e-06]]
Data added to list for particle: 5
Particle 6:
eta 1.2091182420871824, phi 0.8973432698458093,
p 3.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.51222484]
 [ 0.4086991 ]
 [-0.09993617]]

Simulating particle
Particle track 6 is saved
Initiating Kalman filtering for particle: 6
Particle 6:
eta 1.1480063981508875, phi -1.3338882601798414,
p 2.0772847155566683, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.041683524735741315]
y:   [0.010064156592767251]
tx:  [0.5852132544768174]
ty:  [0.3941294996437532]
q/p: [-0.14432710981130706]]
Covariance matrix:
[[ 1.5e-05  1.3e-08 -5.6e-06 -8.8e-08 -1.1e-06]
 [ 1.3e-08  1.5e-05 -1.0e-07 -3.6e-06  1.1e-07]
 [-5.6e-06 -1.0e-07  1.5e-05  1.7e-06  1.3e-06]
 [-8.8e-08 -3.6e-06  1.7e-06  2.5e-05 -5.2e-07]
 [-1.1e-06  1.1e-07  1.3e-06 -5.2e-07  2.4e-06]]
Data added to list for particle: 6
Particle 7:
eta 2.7884034427981272, phi -1.2808190526463035,
p 3.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.1183498 ]
 [-0.03531416]
 [-0.09993617]]

Simulating particle
Particle track 7 is saved
Initiating Kalman filtering for particle: 7
Particle 7:
eta 2.774188589884984, phi -1.2962186227616554,
p 2.794024026529894, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.00805817571586387]
y:   [-0.002269929723088178]
tx:  [0.11951277440542098]
ty:  [-0.037600326215539534]
q/p: [-0.1073034792846257]]
Covariance matrix:
[[ 4.1e-06  7.4e-09 -2.4e-06 -5.4e-08 -7.4e-07]
 [ 7.4e-09  4.2e-06 -6.7e-08 -2.8e-06  1.9e-08]
 [-2.4e-06 -6.7e-08  9.5e-06  4.9e-07  2.1e-06]
 [-5.4e-08 -2.8e-06  4.9e-07  1.7e-05 -1.4e-07]
 [-7.4e-07  1.9e-08  2.1e-06 -1.4e-07  2.7e-06]]
Data added to list for particle: 7
Particle 8:
eta 3.720915300199664, phi -1.180378366011914,
p 3.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.04480599]
 [-0.01843962]
 [-0.09993617]]

Simulating particle
Particle track 8 is saved
Initiating Kalman filtering for particle: 8
Particle 8:
eta 3.7246405959172666, phi -1.1102897644825456,
p 2.8589943859528906, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.006711935527403021]
y:   [-0.0033296558720321404]
tx:  [0.045134650164143496]
ty:  [-0.017117623543140476]
q/p: [-0.10486501852698536]]
Covariance matrix:
[[ 4.4e-06 -2.3e-10 -3.6e-06  9.7e-10 -1.2e-06]
 [-2.3e-10  3.6e-06  8.4e-10 -2.3e-06 -2.4e-10]
 [-3.6e-06  8.4e-10  8.6e-06 -3.8e-09  2.4e-06]
 [ 9.7e-10 -2.3e-06 -3.8e-09  9.0e-06  1.1e-09]
 [-1.2e-06 -2.4e-10  2.4e-06  1.1e-09  2.3e-06]]
Data added to list for particle: 8
Particle 9:
eta 3.9770934437212677, phi -0.7611156296672708,
p 3.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.02586019]
 [ 0.02714761]
 [-0.09993617]]

Simulating particle
Particle track 9 is saved
Initiating Kalman filtering for particle: 9
Particle 9:
eta 4.0558284696332585, phi -0.8163601445133783,
p 3.033483129201903, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.006613232263390205]
y:   [0.006215890236803628]
tx:  [-0.022605443489129053]
ty:  [0.026264038668879067]
q/p: [-0.09883308608687578]]
Covariance matrix:
[[ 5.5e-06 -5.0e-11 -4.3e-06  4.1e-10 -1.5e-06]
 [-5.0e-11  4.4e-06  2.1e-10 -2.4e-06  7.0e-10]
 [-4.3e-06  2.1e-10  9.4e-06 -1.8e-09  2.7e-06]
 [ 4.1e-10 -2.4e-06 -1.8e-09  8.9e-06 -1.1e-09]
 [-1.5e-06  7.0e-10  2.7e-06 -1.1e-09  2.5e-06]]
Data added to list for particle: 9
Particle 10:
eta 2.4775317582303176, phi -0.9600688973943043,
p 4.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.13852547]
 [ 0.09696813]
 [-0.07495212]]

Simulating particle
Particle track 10 is saved
Initiating Kalman filtering for particle: 10
Particle 10:
eta 2.5002207450265583, phi -0.9927877650818989,
p 4.482756035168606, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.0068923614273620785]
y:   [0.004496065865408696]
tx:  [-0.1350840555535939]
ty:  [0.09517755860532388]
q/p: [-0.06688039609994537]]
Covariance matrix:
[[ 5.4e-06 -3.3e-10 -2.6e-06  3.7e-09 -9.2e-07]
 [-3.3e-10  3.9e-06  3.0e-09 -1.2e-06  2.6e-09]
 [-2.6e-06  3.0e-09  6.8e-06 -3.3e-08  2.1e-06]
 [ 3.7e-09 -1.2e-06 -3.3e-08  7.4e-06  7.2e-11]
 [-9.2e-07  2.6e-09  2.1e-06  7.2e-11  1.9e-06]]
Data added to list for particle: 10
Particle 11:
eta 3.411019884092252, phi 1.2275531522213465,
p 4.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.06223206]
 [-0.02224112]
 [-0.07495212]]

Simulating particle
Particle track 11 is saved
Initiating Kalman filtering for particle: 11
Particle 11:
eta 3.4467632185103403, phi 1.2448947222549875,
p 4.372259598323501, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.009121900078740992]
y:   [-0.003082764636395584]
tx:  [-0.05971335602898488]
ty:  [-0.022358014198918856]
q/p: [-0.06857060805960735]]
Covariance matrix:
[[ 4.2e-06  5.8e-11 -2.3e-06 -3.8e-10 -8.8e-07]
 [ 5.8e-11  4.1e-06 -4.8e-10 -1.8e-06 -8.9e-10]
 [-2.3e-06 -4.8e-10  6.9e-06  3.2e-09  2.4e-06]
 [-3.8e-10 -1.8e-06  3.2e-09  7.1e-06  1.6e-09]
 [-8.8e-07 -8.9e-10  2.4e-06  1.6e-09  2.1e-06]]
Data added to list for particle: 11
Particle 12:
eta 3.075105155929963, phi 0.004246661281851349,
p 4.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.0003931 ]
 [-0.09256616]
 [-0.07495212]]

Simulating particle
Particle track 12 is saved
Initiating Kalman filtering for particle: 12
Particle 12:
eta 3.073796793829201, phi 0.07999924451815231,
p 4.32018555939789, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.0007578601119755393]
y:   [-0.009453122821492748]
tx:  [0.0015596437332041618]
ty:  [-0.0926755782979568]
q/p: [-0.06939713471318619]]
Covariance matrix:
[[ 4.4e-06 -9.6e-11 -2.7e-06  4.9e-10 -9.9e-07]
 [-9.6e-11  3.9e-06  4.0e-10 -1.3e-06 -4.5e-09]
 [-2.7e-06  4.0e-10  7.1e-06 -2.3e-09  2.4e-06]
 [ 4.9e-10 -1.3e-06 -2.3e-09  7.5e-06  1.5e-08]
 [-9.9e-07 -4.5e-09  2.4e-06  1.5e-08  2.2e-06]]
Data added to list for particle: 12
Particle 13:
eta 1.7235598218152266, phi 0.3162844093808634,
p 4.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.11464675]
 [-0.35031154]
 [-0.07495212]]

Simulating particle
Particle track 13 is saved
Initiating Kalman filtering for particle: 13
Particle 13:
eta 1.7320228909244897, phi 0.4103301067448152,
p 4.1409135883575345, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.00762721870714826]
y:   [-0.01753287981054177]
tx:  [-0.11042965190823185]
ty:  [-0.34819498540880484]
q/p: [-0.07240153479522717]]
Covariance matrix:
[[ 1.5e-05  2.6e-11 -5.9e-06 -1.1e-08 -2.0e-06]
 [ 2.6e-11  7.5e-06 -4.1e-09 -1.4e-06 -1.7e-08]
 [-5.9e-06 -4.1e-09  8.2e-06  1.2e-07  2.5e-06]
 [-1.1e-08 -1.4e-06  1.2e-07  8.5e-06 -1.6e-09]
 [-2.0e-06 -1.7e-08  2.5e-06 -1.6e-09  1.9e-06]]
Data added to list for particle: 13
Particle 14:
eta 1.0573955892059583, phi -0.17344898188999977,
p 4.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.13634629]
 [-0.77818983]
 [-0.07495212]]

Simulating particle
Particle track 14 is saved
Initiating Kalman filtering for particle: 14
Particle 14:
eta 1.0630514413729797, phi 0.043277309377968226,
p 2.956992348642646, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.0006830145304764779]
y:   [-0.015772423823403884]
tx:  [0.14065126080448115]
ty:  [-0.7716642436111117]
q/p: [-0.10138967704435174]]
Covariance matrix:
[[ 1.5e-05 -4.6e-09 -5.6e-06  2.5e-08 -1.6e-06]
 [-4.6e-09  1.5e-05  3.1e-08 -2.9e-06 -7.7e-08]
 [-5.6e-06  3.1e-08  9.0e-06 -4.6e-07  2.0e-06]
 [ 2.5e-08 -2.9e-06 -4.6e-07  1.5e-05  2.2e-07]
 [-1.6e-06 -7.7e-08  2.0e-06  2.2e-07  1.7e-06]]
Data added to list for particle: 14
Particle 15:
eta 3.3321159304451826, phi 0.1952932744437674,
p 4.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.01387995]
 [ 0.07016649]
 [-0.07495212]]

Simulating particle
Particle track 15 is saved
Initiating Kalman filtering for particle: 15
Particle 15:
eta 3.3333869392716333, phi 0.14936618672789115,
p 4.087914869088002, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.0014805636274765745]
y:   [0.00983848260988257]
tx:  [0.016044893805388927]
ty:  [0.06960983849902105]
q/p: [-0.07334020126460783]]
Covariance matrix:
[[ 4.2e-06  9.7e-11 -2.3e-06 -5.7e-10 -9.0e-07]
 [ 9.7e-11  5.1e-06 -5.8e-10 -1.3e-06  2.9e-09]
 [-2.3e-06 -5.8e-10  6.9e-06  4.1e-09  2.5e-06]
 [-5.7e-10 -1.3e-06  4.1e-09  6.9e-06 -8.3e-09]
 [-9.0e-07  2.9e-09  2.5e-06 -8.3e-09  2.2e-06]]
Data added to list for particle: 15
Particle 16:
eta 2.575284017012744, phi -0.9795360674395477,
p 4.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.12715281]
 [ 0.08536816]
 [-0.07495212]]

Simulating particle
Particle track 16 is saved
Initiating Kalman filtering for particle: 16
Particle 16:
eta 2.5866430592964447, phi -1.0068476635347368,
p 4.492814622441324, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.00989519108887386]
y:   [0.0062583517679915416]
tx:  [-0.12459908644257407]
ty:  [0.08601026295278441]
q/p: [-0.06673066316913513]]
Covariance matrix:
[[ 4.1e-06 -2.7e-10 -2.1e-06  2.9e-09 -7.5e-07]
 [-2.7e-10  5.2e-06  2.6e-09 -1.4e-06  3.2e-09]
 [-2.1e-06  2.6e-09  6.7e-06 -2.7e-08  2.1e-06]
 [ 2.9e-09 -1.4e-06 -2.7e-08  7.4e-06 -7.3e-10]
 [-7.5e-07  3.2e-09  2.1e-06 -7.3e-10  1.9e-06]]
Data added to list for particle: 16
Particle 17:
eta 3.3263277468818795, phi -0.1647184083080022,
p 4.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.01179673]
 [ 0.07096865]
 [-0.07495212]]

Simulating particle
Particle track 17 is saved
Initiating Kalman filtering for particle: 17
Particle 17:
eta 3.3578187668055435, phi -0.20202328381933793,
p 4.281349889713333, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.0020695688394574588]
y:   [0.010104461766960695]
tx:  [-0.01018803447844998]
ty:  [0.06895814707515635]
q/p: [-0.07002662874432142]]
Covariance matrix:
[[ 4.2e-06  3.4e-11 -2.4e-06  7.2e-11 -8.9e-07]
 [ 3.4e-11  5.1e-06 -6.7e-11 -1.4e-06  3.5e-09]
 [-2.4e-06 -6.7e-11  7.0e-06 -1.3e-09  2.4e-06]
 [ 7.2e-11 -1.4e-06 -1.3e-09  7.2e-06 -9.0e-09]
 [-8.9e-07  3.5e-09  2.4e-06 -9.0e-09  2.2e-06]]
Data added to list for particle: 17
Particle 18:
eta 3.1648444523881625, phi -0.7918762032056169,
p 4.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.06020196]
 [-0.05942698]
 [-0.07495212]]

Simulating particle
Particle track 18 is saved
Initiating Kalman filtering for particle: 18
Particle 18:
eta 3.152587098399334, phi -0.728936897211462,
p 3.9550606740522913, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.005399239137857146]
y:   [-0.0060461477402871215]
tx:  [0.06114564449204757]
ty:  [-0.05996076678921375]
q/p: [-0.0758037673652976]]
Covariance matrix:
[[ 4.4e-06 -2.0e-10 -2.6e-06  1.7e-09 -1.0e-06]
 [-2.0e-10  3.9e-06  1.1e-09 -1.2e-06 -2.5e-09]
 [-2.6e-06  1.1e-09  7.1e-06 -1.0e-08  2.5e-06]
 [ 1.7e-09 -1.2e-06 -1.0e-08  7.1e-06  1.0e-08]
 [-1.0e-06 -2.5e-09  2.5e-06  1.0e-08  2.2e-06]]
Data added to list for particle: 18
Particle 19:
eta 2.216668661286149, phi 0.5536554984633313,
p 4.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.11597162]
 [-0.18761199]
 [-0.07495212]]

Simulating particle
Particle track 19 is saved
Initiating Kalman filtering for particle: 19
Particle 19:
eta 2.2324101893129535, phi 0.6139072924424498,
p 4.394479092350791, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.00628852635707164]
y:   [-0.008923049941771009]
tx:  [-0.11265725425828552]
ty:  [-0.18550796414705215]
q/p: [-0.06822389934073321]]
Covariance matrix:
[[ 7.8e-06  1.4e-10 -3.3e-06 -3.5e-09 -1.2e-06]
 [ 1.4e-10  5.1e-06 -2.2e-09 -1.2e-06 -7.4e-09]
 [-3.3e-06 -2.2e-09  7.1e-06  3.7e-08  2.2e-06]
 [-3.5e-09 -1.2e-06  3.7e-08  7.7e-06  8.1e-09]
 [-1.2e-06 -7.4e-09  2.2e-06  8.1e-09  1.9e-06]]
Data added to list for particle: 19
Particle 20:
eta 2.7568324595033107, phi -0.6707252116705741,
p 5.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.07924762]
 [-0.09987913]
 [-0.0599617 ]]

Simulating particle
Particle track 20 is saved
Initiating Kalman filtering for particle: 20
Particle 20:
eta 2.75637817826244, phi -0.6441440969185792,
p 4.946684519537813, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.005481343944258521]
y:   [-0.007298676868520751]
tx:  [0.07985242118192455]
ty:  [-0.09947116260653857]
q/p: [-0.060607968441761306]]
Covariance matrix:
[[ 4.1e-06 -2.5e-10 -2.1e-06  1.6e-09 -8.3e-07]
 [-2.5e-10  3.9e-06  2.0e-09 -1.2e-06 -5.1e-09]
 [-2.1e-06  2.0e-09  6.0e-06 -1.4e-08  2.5e-06]
 [ 1.6e-09 -1.2e-06 -1.4e-08  5.7e-06  1.9e-08]
 [-8.3e-07 -5.1e-09  2.5e-06  1.9e-08  2.0e-06]]
Data added to list for particle: 20
Particle 21:
eta 1.7093515478983665, phi -0.7212701125208257,
p 5.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.24711463]
 [-0.28103003]
 [-0.0599617 ]]

Simulating particle
Particle track 21 is saved
Initiating Kalman filtering for particle: 21
Particle 21:
eta 1.7128160915004231, phi -0.6401359930408768,
p 3.829889666514653, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.010539976745046658]
y:   [-0.014152269642748867]
tx:  [0.24887800416959063]
ty:  [-0.277617035685836]
q/p: [-0.07828123663007094]]
Covariance matrix:
[[ 1.5e-05 -1.2e-09 -5.9e-06  1.4e-08 -2.1e-06]
 [-1.2e-09  7.5e-06  7.5e-09 -1.3e-06 -2.1e-08]
 [-5.9e-06  7.5e-09  7.7e-06 -1.5e-07  2.7e-06]
 [ 1.4e-08 -1.3e-06 -1.5e-07  6.6e-06  1.1e-07]
 [-2.1e-06 -2.1e-08  2.7e-06  1.1e-07  2.1e-06]]
Data added to list for particle: 21
Particle 22:
eta 1.2601354695411449, phi -0.641443953058293,
p 5.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.36909358]
 [ 0.49423969]
 [-0.0599617 ]]

Simulating particle
Particle track 22 is saved
Initiating Kalman filtering for particle: 22
Particle 22:
eta 1.2698036524131637, phi -0.743581509845603,
p 4.577750374351976, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.009085721228223861]
y:   [0.009879234799185893]
tx:  [-0.36178978073437784]
ty:  [0.4909958806309064]
q/p: [-0.06549253994522096]]
Covariance matrix:
[[ 1.5e-05 -1.1e-09 -5.5e-06  2.5e-08 -1.6e-06]
 [-1.1e-09  1.5e-05  2.1e-08 -2.3e-06  1.4e-08]
 [-5.5e-06  2.1e-08  7.4e-06 -4.2e-07  2.0e-06]
 [ 2.5e-08 -2.3e-06 -4.2e-07  7.2e-06  8.1e-08]
 [-1.6e-06  1.4e-08  2.0e-06  8.1e-08  1.1e-06]]
Data added to list for particle: 22
Particle 23:
eta 3.064820661394505, phi -0.8155986645145685,
p 5.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.06810116]
 [ 0.06410718]
 [-0.0599617 ]]

Simulating particle
Particle track 23 is saved
Initiating Kalman filtering for particle: 23
Particle 23:
eta 3.084747124916836, phi -0.8332309474450288,
p 5.401610002310394, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.00732582261609099]
y:   [0.006656498715951017]
tx:  [-0.0662675899536037]
ty:  [0.06334756456710626]
q/p: [-0.05550354415132931]]
Covariance matrix:
[[ 5.4e-06 -5.8e-11 -2.6e-06  7.9e-10 -1.0e-06]
 [-5.8e-11  3.9e-06  6.2e-10 -1.1e-06  2.1e-09]
 [-2.6e-06  6.2e-10  6.3e-06 -7.1e-09  2.5e-06]
 [ 7.9e-10 -1.1e-06 -7.1e-09  5.6e-06 -4.6e-09]
 [-1.0e-06  2.1e-09  2.5e-06 -4.6e-09  2.0e-06]]
Data added to list for particle: 23
Particle 24:
eta 1.3417097162803482, phi 0.36640831861459827,
p 5.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.20103612]
 [-0.52389054]
 [-0.0599617 ]]

Simulating particle
Particle track 24 is saved
Initiating Kalman filtering for particle: 24
Particle 24:
eta 1.3499340760365395, phi 0.47446397720520134,
p 4.612808895810346, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.005406858379181135]
y:   [-0.010527484722703295]
tx:  [-0.19699041475815754]
ty:  [-0.5198026196695751]
q/p: [-0.06499477997534271]]
Covariance matrix:
[[ 1.5e-05  9.7e-11 -5.5e-06 -1.4e-08 -1.8e-06]
 [ 9.7e-11  1.5e-05 -1.1e-08 -2.3e-06 -2.7e-08]
 [-5.5e-06 -1.1e-08  7.3e-06  2.3e-07  2.2e-06]
 [-1.4e-08 -2.3e-06  2.3e-07  7.3e-06 -3.5e-08]
 [-1.8e-06 -2.7e-08  2.2e-06 -3.5e-08  1.4e-06]]
Data added to list for particle: 24
Particle 25:
eta 1.1263507655840503, phi 0.09855065581894497,
p 5.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.07129369]
 [-0.72107821]
 [-0.0599617 ]]

Simulating particle
Particle track 25 is saved
Initiating Kalman filtering for particle: 25
Particle 25:
eta 1.134856605386138, phi 0.21549665732739404,
p 4.229398007286909, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.0031993831596957593]
y:   [-0.01461602240120464]
tx:  [-0.06648741205140932]
ty:  [-0.7139470689139801]
q/p: [-0.07088680203067939]]
Covariance matrix:
[[ 1.5e-05 -9.3e-10 -5.5e-06 -6.3e-09 -1.7e-06]
 [-9.3e-10  1.5e-05 -3.1e-09 -2.5e-06 -4.2e-08]
 [-5.5e-06 -3.1e-09  7.4e-06  1.2e-07  2.2e-06]
 [-6.3e-09 -2.5e-06  1.2e-07  8.8e-06  1.5e-08]
 [-1.7e-06 -4.2e-08  2.2e-06  1.5e-08  1.4e-06]]
Data added to list for particle: 25
Particle 26:
eta 1.6936605876092672, phi 1.4280675490213546,
p 5.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.37668332]
 [ 0.05413163]
 [-0.0599617 ]]

Simulating particle
Particle track 26 is saved
Initiating Kalman filtering for particle: 26
Particle 26:
eta 1.6889070432763336, phi 1.4046184293951418,
p 3.205207235650128, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.016189987174305736]
y:   [0.002715460061085166]
tx:  [0.37870205292050596]
ty:  [0.053725791789082815]
q/p: [-0.09353794535244314]]
Covariance matrix:
[[ 1.5e-05  3.3e-10 -6.0e-06 -4.1e-09 -2.0e-06]
 [ 3.3e-10  7.5e-06 -2.1e-09 -1.3e-06  4.7e-09]
 [-6.0e-06 -2.1e-09  7.9e-06  4.6e-08  2.6e-06]
 [-4.1e-09 -1.3e-06  4.6e-08  6.4e-06 -2.9e-08]
 [-2.0e-06  4.7e-09  2.6e-06 -2.9e-08  2.0e-06]]
Data added to list for particle: 26
Particle 27:
eta 1.244110082712637, phi -0.7209197774139322,
p 5.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.       ]
 [ 0.       ]
 [-0.414927 ]
 [ 0.4722074]
 [-0.0599617]]

Simulating particle
Particle track 27 is saved
Initiating Kalman filtering for particle: 27
Particle 27:
eta 1.2529253161050538, phi -0.8208962667133721,
p 4.5990795591545135, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.01011017778628023]
y:   [0.009416718693644004]
tx:  [-0.4084383581502146]
ty:  [0.4692446869352101]
q/p: [-0.06518880471522291]]
Covariance matrix:
[[ 1.5e-05 -1.4e-09 -5.5e-06  2.7e-08 -1.5e-06]
 [-1.4e-09  1.5e-05  2.3e-08 -2.3e-06  1.1e-08]
 [-5.5e-06  2.3e-08  7.5e-06 -4.6e-07  1.9e-06]
 [ 2.7e-08 -2.3e-06 -4.6e-07  7.1e-06  8.7e-08]
 [-1.5e-06  1.1e-08  1.9e-06  8.7e-08  1.0e-06]]
Data added to list for particle: 27
Particle 28:
eta 2.1745556214996444, phi 1.5597821494770256,
p 5.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.23027831]
 [-0.00253643]
 [-0.0599617 ]]

Simulating particle
Particle track 28 is saved
Initiating Kalman filtering for particle: 28
Particle 28:
eta 2.1922130018140784, phi 1.557138534820898,
p 5.192464598804214, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.011213587254056244]
y:   [-0.00015316236551160897]
tx:  [-0.2261515465730163]
ty:  [-0.0018252965606093358]
q/p: [-0.05773915133105398]]
Covariance matrix:
[[ 7.8e-06  3.5e-12 -3.3e-06 -6.4e-11 -1.2e-06]
 [ 3.5e-12  5.1e-06 -6.7e-11 -1.2e-06 -8.8e-11]
 [-3.3e-06 -6.7e-11  6.3e-06  7.9e-10  2.2e-06]
 [-6.4e-11 -1.2e-06  7.9e-10  6.5e-06 -9.8e-12]
 [-1.2e-06 -8.8e-11  2.2e-06 -9.8e-12  1.6e-06]]
Data added to list for particle: 28
Particle 29:
eta 3.4453981543774046, phi 0.9280978315951063,
p 5.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.05110998]
 [ 0.03826843]
 [-0.0599617 ]]

Simulating particle
Particle track 29 is saved
Initiating Kalman filtering for particle: 29
Particle 29:
eta 3.447287811964085, phi 0.881606563018424,
p 4.9118509920354425, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.006645846209360853]
y:   [0.005476010217067117]
tx:  [0.051748226539109286]
ty:  [0.03719428433695287]
q/p: [-0.061037783869591314]]
Covariance matrix:
[[ 3.7e-06  1.4e-10 -2.4e-06 -6.6e-10 -1.0e-06]
 [ 1.4e-10  4.1e-06 -6.9e-10 -1.4e-06  8.9e-10]
 [-2.4e-06 -6.9e-10  6.1e-06  3.7e-09  2.6e-06]
 [-6.6e-10 -1.4e-06  3.7e-09  4.9e-06 -2.7e-09]
 [-1.0e-06  8.9e-10  2.6e-06 -2.7e-09  1.9e-06]]
Data added to list for particle: 29
Particle 30:
eta 2.2677371296117443, phi -0.3638138715972879,
p 6.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.07449067]
 [ 0.19563509]
 [-0.04996808]]

Simulating particle
Particle track 30 is saved
Initiating Kalman filtering for particle: 30
Particle 30:
eta 2.27461116864819, phi -0.37495252502473664,
p 6.183217579915033, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.003614822049137972]
y:   [0.009184658413209208]
tx:  [-0.07309086300973618]
ty:  [0.19459840630763686]
q/p: [-0.0484874574405025]]
Covariance matrix:
[[ 5.4e-06 -4.3e-11 -2.5e-06  1.6e-09 -1.0e-06]
 [-4.3e-11  5.1e-06  7.9e-10 -9.8e-07  6.2e-09]
 [-2.5e-06  7.9e-10  5.7e-06 -1.5e-08  2.4e-06]
 [ 1.6e-09 -9.8e-07 -1.5e-08  4.7e-06 -1.2e-08]
 [-1.0e-06  6.2e-09  2.4e-06 -1.2e-08  1.8e-06]]
Data added to list for particle: 30
Particle 31:
eta 2.996596789576226, phi 0.5495892897833835,
p 6.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.05231914]
 [-0.08541339]
 [-0.04996808]]

Simulating particle
Particle track 31 is saved
Initiating Kalman filtering for particle: 31
Particle 31:
eta 3.011231489882253, phi 0.5689762559675452,
p 6.359626178684279, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.004057122715850738]
y:   [-0.006343961079894325]
tx:  [-0.050957868170523814]
ty:  [-0.08452947587871129]
q/p: [-0.04714247203025435]]
Covariance matrix:
[[ 4.1e-06  4.0e-11 -2.0e-06 -4.8e-10 -8.3e-07]
 [ 4.0e-11  3.9e-06 -5.5e-10 -1.1e-06 -2.7e-09]
 [-2.0e-06 -5.5e-10  5.6e-06  5.1e-09  2.5e-06]
 [-4.8e-10 -1.1e-06  5.1e-09  4.7e-06  6.8e-09]
 [-8.3e-07 -2.7e-09  2.5e-06  6.8e-09  1.8e-06]]
Data added to list for particle: 31
Particle 32:
eta 2.3016793357583976, phi 0.548549754439876,
p 6.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.10544089]
 [ 0.1725395 ]
 [-0.04996808]]

Simulating particle
Particle track 32 is saved
Initiating Kalman filtering for particle: 32
Particle 32:
eta 2.3050822784741074, phi 0.48616429342447237,
p 5.719583302269121, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.004284327177189793]
y:   [0.008107021024478998]
tx:  [0.10532350010022744]
ty:  [0.1717897338898159]
q/p: [-0.052417891900019095]]
Covariance matrix:
[[ 5.4e-06  2.8e-10 -2.5e-06 -2.8e-09 -1.0e-06]
 [ 2.8e-10  5.1e-06 -1.9e-09 -9.9e-07  7.3e-09]
 [-2.5e-06 -1.9e-09  5.8e-06  2.4e-08  2.5e-06]
 [-2.8e-09 -9.9e-07  2.4e-08  4.8e-06 -3.0e-08]
 [-1.0e-06  7.3e-09  2.5e-06 -3.0e-08  1.9e-06]]
Data added to list for particle: 32
Particle 33:
eta 1.564339002377996, phi -0.4890553152808788,
p 6.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.20558534]
 [-0.38631132]
 [-0.04996808]]

Simulating particle
Particle track 33 is saved
Initiating Kalman filtering for particle: 33
Particle 33:
eta 1.5658246153503146, phi -0.40842602297740094,
p 4.686731069285976, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.003379833657586441]
y:   [-0.00780992810391992]
tx:  [0.20742163431276553]
ty:  [-0.38452311539330053]
q/p: [-0.06396963999412342]]
Covariance matrix:
[[ 7.6e-06 -1.1e-09 -3.0e-06  8.3e-09 -1.1e-06]
 [-1.1e-09  1.5e-05  9.1e-09 -2.2e-06 -4.1e-08]
 [-3.0e-06  9.1e-09  6.1e-06 -1.3e-07  2.3e-06]
 [ 8.3e-09 -2.2e-06 -1.3e-07  5.8e-06  1.0e-07]
 [-1.1e-06 -4.1e-08  2.3e-06  1.0e-07  1.7e-06]]
Data added to list for particle: 33
Particle 34:
eta 3.4994244137773762, phi 1.0675275845715029,
p 6.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.0529853 ]
 [-0.02917129]
 [-0.04996808]]

Simulating particle
Data added to list for particle: 34
data saved to data/toy_output/28