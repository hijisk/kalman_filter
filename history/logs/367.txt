This is the start of the log file generated at 09_24_15h27m21s

15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 2 particles...
Particle 0:
eta 3.9147295868298104, phi 0.08848369612741777,
p 3.0, q 1.60217662e-19,
brem = False
input State:
[[0.001     ]
 [0.001     ]
 [0.00352658]
 [0.03975164]
 [0.09993617]]

Simulating particle
Particle track 0 is saved
Skip particle 0 because:The particle did not register in any subdetector
Data added to pickle list for particle: 0
Particle 1:
eta 2.4894017427830093, phi 0.3231503992254694,
p 53.0, q -1.60217662e-19,
brem = False
input State:
[[-0.001     ]
 [ 0.001     ]
 [ 0.05305367]
 [ 0.15842144]
 [-0.00565676]]

Simulating particle
Particle track 1 is saved
Skip particle 1 because:The particle did not register in any subdetector
Data added to pickle list for particle: 1
data saved to data/toy_output/367
2 particles skipped because: The particle did not register in any subdetector