This is the start of the log file generated at 11_04_19h26m35s

magfield_ shape: cs, sim_stepsize: 0.001, detector_length: 11
15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 90 particles, from 10 to 100 GeV. Brem = False, ms = False

Particle 0:
eta 2.212272204424275, phi 0.9805256187819839,
p 10.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [-0.001     ]
 [ 0.18406778]
 [ 0.1233158 ]
 [ 0.02998085]]

Simulating particle
Initiating Kalman filtering for particle: 0
z = 0
F[0,2]: 
O(h): 0.001
O(h^2): 2.1575508851841078e-16
O(h^3): 3.896145480070049e-28
F[0,3]: O(h^2): 4.9995276484560486e-17
O(h^3): 1.4452513574676562e-29
F[0,4]: O(h^2): -1.4129628860594417e-14
O(h^3): -2.3223894704546866e-26
F[2,2]: 
O(h^0): 1
O(h^1): 4.3151017703682164e-13
O(h^2): 1.1688436440210148e-24
F[2,3] 
O(h^1): 9.999055296912099e-14
O(h^2): 4.33575407240297e-26
F[2,4] 
O(h^1): -2.8259257721188838e-11
O(h^2): -1.2253676783402499e-23
Exception occured at particle 0
Data added to pickle list for particle: 0