This is the start of the log file generated at 09_02_14h15m29s

15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 400 particles...
Particle 0:
eta 2.7979735307727704, phi -2.234186583597159,
p 40.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.09637782]
 [-0.07532424]
 [ 0.00749521]]

Simulating particle
Particle track 0 is saved
Skip particle 0 because:The particle did not register in any subdetector
Data added to list for particle: 0
Particle 1:
eta 4.7804319733191285, phi -1.2973647389771878,
p 40.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.01616233]
 [ 0.00453282]
 [-0.00749521]]

Simulating particle
Particle track 1 is saved
Skip particle 1 because:The particle did not register in any subdetector
Data added to list for particle: 1
Particle 2:
eta 2.7383241451685048, phi -2.3586353519824415,
p 40.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.0916292 ]
 [-0.0920776 ]
 [ 0.00749521]]

Simulating particle
Particle track 2 is saved
Skip particle 2 because:The particle did not register in any subdetector
Data added to list for particle: 2
Particle 3:
eta 2.3710714804615907, phi -1.5991059805595753,
p 40.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.18832861]
 [-0.00533294]
 [-0.00749521]]

Simulating particle
Particle track 3 is saved
Skip particle 3 because:The particle did not register in any subdetector
Data added to list for particle: 3
Particle 4:
eta 2.826893043894924, phi 0.8810169409160228,
p 40.0, q 1.60217662e-19,
brem = False
input State:
[[0.        ]
 [0.        ]
 [0.09164789]
 [0.07560633]
 [0.00749521]]

Simulating particle
Particle track 4 is saved
Skip particle 4 because:The particle did not register in any subdetector
Data added to list for particle: 4
Particle 5:
eta 2.0548945047889466, phi -0.6673324159685515,
p 40.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.16121393]
 [ 0.20460662]
 [-0.00749521]]

Simulating particle
Particle track 5 is saved
Skip particle 5 because:The particle did not register in any subdetector
Data added to list for particle: 5
Particle 6:
eta 2.0396915735947063, phi 0.01335957657168384,
p 40.0, q 1.60217662e-19,
brem = False
input State:
[[0.        ]
 [0.        ]
 [0.00353503]
 [0.26459075]
 [0.00749521]]

Simulating particle
Particle track 6 is saved
Data added to list for particle: 6
data saved to data/toy_output/130
6 particles skipped because: The particle did not register in any subdetector