This is the start of the log file generated at 02_11_23h23m30s

magfield_ shape: cs, sim_stepsize: 0.01, detector_length: 11
15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 T_station-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 100 particles, from 1 to 100 GeV. Brem = False, ms = True

Particle 0:
eta 3.25750024495229, phi 0.11814323537469913,
p 1.0, q 1.60217662e-19,
brem = False
input State:
[[0.001     ]
 [0.001     ]
 [0.00908568]
 [0.07654579]
 [0.2998085 ]]

Simulating particle
Skip particle 0 because:does not pass magnet
Data added to pickle list for particle: 0
Data added to condensed list for particle: 0
Particle 1:
eta 4.926138045870457, phi 1.2922009383939648,
p 1.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.01395024]
 [ 0.00399025]
 [-0.2998085 ]]

Simulating particle
Skip particle 1 because:does not pass magnet
Data added to pickle list for particle: 1
Data added to condensed list for particle: 1
Particle 2:
eta 2.1866046301144, phi -3.0405645602680322,
p 1.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.02294111]
 [-0.22630342]
 [ 0.2998085 ]]

Simulating particle
Skip particle 2 because:does not pass magnet
Data added to pickle list for particle: 2
Data added to condensed list for particle: 2
Particle 3:
eta 2.266863503381481, phi -0.44534247300114554,
p 1.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.09025596]
 [ 0.18908757]
 [-0.2998085 ]]

Simulating particle
Skip particle 3 because:does not pass magnet
Data added to pickle list for particle: 3
Data added to condensed list for particle: 3
Particle 4:
eta 4.216925075893776, phi -1.7911994665008915,
p 1.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.02878075]
 [-0.00644812]
 [ 0.2998085 ]]

Simulating particle
Skip particle 4 because:does not pass magnet
Data added to pickle list for particle: 4
Data added to condensed list for particle: 4
Particle 5:
eta 2.269615574295471, phi 0.7908910262065602,
p 1.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.14854904]
 [ 0.14692602]
 [-0.2998085 ]]

Simulating particle
Skip particle 5 because:does not pass magnet
Data added to pickle list for particle: 5
Data added to condensed list for particle: 5
Particle 6:
eta 2.3710588067128793, phi 2.9239512914386716,
p 1.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.0406821 ]
 [-0.18396192]
 [ 0.2998085 ]]

Simulating particle
Skip particle 6 because:does not pass magnet
Data added to pickle list for particle: 6
Data added to condensed list for particle: 6
Particle 7:
eta 3.2663984840426368, phi -0.7047864521856353,
p 1.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.04949623]
 [ 0.05819637]
 [-0.2998085 ]]

Simulating particle
Skip particle 7 because:does not pass magnet
Data added to pickle list for particle: 7
Data added to condensed list for particle: 7
Particle 8:
eta 2.296913192865989, phi -1.3359617117910618,
p 1.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.19761565]
 [ 0.04727932]
 [ 0.2998085 ]]

Simulating particle
Skip particle 8 because:does not pass magnet
Data added to pickle list for particle: 8
Data added to condensed list for particle: 8
Particle 9:
eta 2.5443257458865762, phi -0.9245556198820566,
p 1.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.12616089]
 [ 0.09516183]
 [-0.2998085 ]]

Simulating particle
Skip particle 9 because:does not pass magnet
Data added to pickle list for particle: 9
Data added to condensed list for particle: 9
Particle 10:
eta 4.556753052394228, phi 0.6199889134370835,
p 1.0, q 1.60217662e-19,
brem = False
input State:
[[0.001     ]
 [0.001     ]
 [0.01219834]
 [0.01708709]
 [0.2998085 ]]

Simulating particle
Skip particle 10 because:does not pass magnet
Data added to pickle list for particle: 10
Data added to condensed list for particle: 10
Particle 11:
eta 2.4294322994487105, phi 1.8091433349494621,
p 1.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.17253187]
 [-0.04191928]
 [-0.2998085 ]]

Simulating particle
Skip particle 11 because:does not pass magnet
Data added to pickle list for particle: 11
Data added to condensed list for particle: 11
Particle 12:
eta 4.388090609273186, phi -1.3000608496688133,
p 1.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.02394742]
 [ 0.00664661]
 [ 0.2998085 ]]

Simulating particle
Skip particle 12 because:does not pass magnet
Data added to pickle list for particle: 12
Data added to condensed list for particle: 12
Particle 13:
eta 4.711836408003367, phi -0.40706862652291587,
p 1.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.00711782]
 [ 0.0165089 ]
 [-0.2998085 ]]

Simulating particle
Skip particle 13 because:does not pass magnet
Data added to pickle list for particle: 13
Data added to condensed list for particle: 13
Particle 14:
eta 4.5370398692240865, phi 2.187694228361394,
p 1.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.01746572]
 [-0.01238732]
 [ 0.2998085 ]]

Simulating particle
Skip particle 14 because:does not pass magnet
Data added to pickle list for particle: 14
Data added to condensed list for particle: 14
Particle 15:
eta 2.6955885442733916, phi -0.22317325098431726,
p 1.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.03001686]
 [ 0.13225981]
 [-0.2998085 ]]

Simulating particle
Skip particle 15 because:does not pass magnet
Data added to pickle list for particle: 15
Data added to condensed list for particle: 15
Particle 16:
eta 2.987859374556467, phi 2.580031963787091,
p 1.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.05380833]
 [-0.08552873]
 [ 0.2998085 ]]

Simulating particle
Skip particle 16 because:does not pass magnet
Data added to pickle list for particle: 16
Data added to condensed list for particle: 16
Particle 17:
eta 2.793426737862732, phi 0.7701329312181494,
p 1.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.08555465]
 [ 0.08820737]
 [-0.2998085 ]]

Simulating particle
Skip particle 17 because:does not pass magnet
Data added to pickle list for particle: 17
Data added to condensed list for particle: 17
Particle 18:
eta 4.843046467919699, phi -1.3398213462757393,
p 1.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.01534827]
 [ 0.00360948]
 [ 0.2998085 ]]

Simulating particle
Skip particle 18 because:does not pass magnet
Data added to pickle list for particle: 18
Data added to condensed list for particle: 18
Particle 19:
eta 4.111814113491233, phi -1.7023144963331327,
p 1.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.0324819 ]
 [-0.00429676]
 [-0.2998085 ]]

Simulating particle
Skip particle 19 because:does not pass magnet
Data added to pickle list for particle: 19
Data added to condensed list for particle: 19
Particle 20:
eta 2.1442632626635123, phi -1.9604845399373665,
p 21.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.21975815]
 [-0.09025258]
 [ 0.0142766 ]]

Simulating particle
Skip particle 20 because:does not pass magnet
Data added to pickle list for particle: 20
Data added to condensed list for particle: 20
Particle 21:
eta 3.4129700713355517, phi -1.893159645183378,
p 21.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.06256046]
 [-0.02089609]
 [-0.0142766 ]]

Simulating particle
Skip particle 21 because:does not pass magnet
Data added to pickle list for particle: 21
Data added to condensed list for particle: 21
Particle 22:
eta 2.0514641211842077, phi 2.9970487297095394,
p 21.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.03765419]
 [-0.25868667]
 [ 0.0142766 ]]

Simulating particle
Skip particle 22 because:does not pass magnet
Data added to pickle list for particle: 22
Data added to condensed list for particle: 22
Particle 23:
eta 4.389892506084975, phi 1.3727172447320268,
p 21.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.02432286]
 [ 0.00488186]
 [-0.0142766 ]]

Simulating particle
Skip particle 23 because:does not pass magnet
Data added to pickle list for particle: 23
Data added to condensed list for particle: 23
Particle 24:
eta 4.011256333875904, phi 0.7742878541078951,
p 21.0, q 1.60217662e-19,
brem = False
input State:
[[0.001     ]
 [0.001     ]
 [0.02533447]
 [0.02590377]
 [0.0142766 ]]

Simulating particle
Skip particle 24 because:does not pass magnet
Data added to pickle list for particle: 24
Data added to condensed list for particle: 24
Particle 25:
eta 2.8740525703788093, phi -1.3989700095543605,
p 21.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.11163208]
 [ 0.01937236]
 [-0.0142766 ]]

Simulating particle
Skip particle 25 because:does not pass magnet
Data added to pickle list for particle: 25
Data added to condensed list for particle: 25
Particle 26:
eta 3.6213796910166534, phi -2.582418685551008,
p 21.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.02839681]
 [-0.04537686]
 [ 0.0142766 ]]

Simulating particle
Skip particle 26 because:does not pass magnet
Data added to pickle list for particle: 26
Data added to condensed list for particle: 26
Particle 27:
eta 2.689122963680119, phi -2.0857259643214827,
p 21.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.11880937]
 [-0.06722813]
 [-0.0142766 ]]

Simulating particle
Skip particle 27 because:does not pass magnet
Data added to pickle list for particle: 27
Data added to condensed list for particle: 27
Particle 28:
eta 4.609923995797154, phi -1.0227703683059506,
p 21.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.01699182]
 [ 0.01037168]
 [ 0.0142766 ]]

Simulating particle
Skip particle 28 because:does not pass magnet
Data added to pickle list for particle: 28
Data added to condensed list for particle: 28
Particle 29:
eta 2.241094777675812, phi -1.5361054786766701,
p 21.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.21498729]
 [ 0.00746108]
 [-0.0142766 ]]

Simulating particle
Skip particle 29 because:does not pass magnet
Data added to pickle list for particle: 29
Data added to condensed list for particle: 29
Particle 30:
eta 4.905269595248027, phi 2.921596479227406,
p 21.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.00323317]
 [-0.01445862]
 [ 0.0142766 ]]

Simulating particle
Skip particle 30 because:does not pass magnet
Data added to pickle list for particle: 30
Data added to condensed list for particle: 30
Particle 31:
eta 3.488804303878164, phi -0.5141600312671881,
p 21.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.03006481]
 [ 0.05322778]
 [-0.0142766 ]]

Simulating particle
Skip particle 31 because:does not pass magnet
Data added to pickle list for particle: 31
Data added to condensed list for particle: 31
Particle 32:
eta 2.303042208786111, phi -2.740613175267986,
p 21.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.07881582]
 [-0.18590906]
 [ 0.0142766 ]]

Simulating particle
Skip particle 32 because:does not pass magnet
Data added to pickle list for particle: 32
Data added to condensed list for particle: 32
Particle 33:
eta 4.84827408634912, phi -1.1228875235319176,
p 21.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.01413754]
 [ 0.00679278]
 [-0.0142766 ]]

Simulating particle
Skip particle 33 because:does not pass magnet
Data added to pickle list for particle: 33
Data added to condensed list for particle: 33
Particle 34:
eta 3.5564319034645617, phi 2.5938458042759818,
p 21.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.02975   ]
 [-0.04876974]
 [ 0.0142766 ]]

Simulating particle
Skip particle 34 because:does not pass magnet
Data added to pickle list for particle: 34
Data added to condensed list for particle: 34
Particle 35:
eta 2.083399392482082, phi -2.553371350430151,
p 21.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.14034824]
 [-0.21042264]
 [-0.0142766 ]]

Simulating particle
Skip particle 35 because:does not pass magnet
Data added to pickle list for particle: 35
Data added to condensed list for particle: 35
Particle 36:
eta 3.8516879083401623, phi 2.2214634830768913,
p 21.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.03382187]
 [-0.02574716]
 [ 0.0142766 ]]

Simulating particle
Skip particle 36 because:does not pass magnet
Data added to pickle list for particle: 36
Data added to condensed list for particle: 36
Particle 37:
eta 4.1260553666007524, phi 1.7243246884666878,
p 21.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.03192137]
 [-0.00493971]
 [-0.0142766 ]]

Simulating particle
Skip particle 37 because:does not pass magnet
Data added to pickle list for particle: 37
Data added to condensed list for particle: 37
Particle 38:
eta 2.774871303279853, phi 2.9847472097931567,
p 21.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.01955692]
 [-0.12366499]
 [ 0.0142766 ]]

Simulating particle
Skip particle 38 because:does not pass magnet
Data added to pickle list for particle: 38
Data added to condensed list for particle: 38
Particle 39:
eta 2.574978702132306, phi 0.029433850998610192,
p 21.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.0045086 ]
 [ 0.15313304]
 [-0.0142766 ]]

Simulating particle
Skip particle 39 because:does not pass magnet
Data added to pickle list for particle: 39
Data added to condensed list for particle: 39
Particle 40:
eta 3.649075978901216, phi 2.974350752614195,
p 41.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.00866701]
 [-0.05133911]
 [ 0.0073124 ]]

Simulating particle
Skip particle 40 because:does not pass magnet
Data added to pickle list for particle: 40
Data added to condensed list for particle: 40
Particle 41:
eta 4.771347135524609, phi 2.921064188746818,
p 41.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.00370536]
 [-0.01652891]
 [-0.0073124 ]]

Simulating particle
Skip particle 41 because:does not pass magnet
Data added to pickle list for particle: 41
Data added to condensed list for particle: 41
Particle 42:
eta 3.0158434207192046, phi -1.0927726454273257,
p 41.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.08723222]
 [ 0.04519514]
 [ 0.0073124 ]]

Simulating particle
Skip particle 42 because:does not pass magnet
Data added to pickle list for particle: 42
Data added to condensed list for particle: 42
Particle 43:
eta 3.331362482338815, phi 2.6797744684461153,
p 41.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.03189446]
 [-0.06408173]
 [-0.0073124 ]]

Simulating particle
Skip particle 43 because:does not pass magnet
Data added to pickle list for particle: 43
Data added to condensed list for particle: 43
Particle 44:
eta 2.841584126452196, phi 1.0042894000688685,
p 41.0, q 1.60217662e-19,
brem = False
input State:
[[0.001     ]
 [0.001     ]
 [0.09877696]
 [0.06282722]
 [0.0073124 ]]

Simulating particle
Skip particle 44 because:does not pass magnet
Data added to pickle list for particle: 44
Data added to condensed list for particle: 44
Particle 45:
eta 2.6399776478609036, phi 2.7974129230505445,
p 41.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.04840569]
 [-0.13504296]
 [-0.0073124 ]]

Simulating particle
Skip particle 45 because:does not pass magnet
Data added to pickle list for particle: 45
Data added to condensed list for particle: 45
Particle 46:
eta 4.420431410815728, phi -0.6299421414311394,
p 41.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.01417462]
 [ 0.01944323]
 [ 0.0073124 ]]

Simulating particle
Skip particle 46 because:does not pass magnet
Data added to pickle list for particle: 46
Data added to condensed list for particle: 46
Particle 47:
eta 3.481363472020567, phi -2.894524106153307,
p 41.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.0150624 ]
 [-0.05971892]
 [-0.0073124 ]]

Simulating particle
Skip particle 47 because:does not pass magnet
Data added to pickle list for particle: 47
Data added to condensed list for particle: 47
Particle 48:
eta 4.0140402324092745, phi -2.569508614374881,
p 41.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.01956153]
 [-0.03037916]
 [ 0.0073124 ]]

Simulating particle
Skip particle 48 because:does not pass magnet
Data added to pickle list for particle: 48
Data added to condensed list for particle: 48
Particle 49:
eta 2.1388882576720034, phi 3.0907034862179597,
p 41.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.01215144]
 [-0.23857635]
 [-0.0073124 ]]

Simulating particle
Skip particle 49 because:does not pass magnet
Data added to pickle list for particle: 49
Data added to condensed list for particle: 49
Particle 50:
eta 3.829383042910363, phi -0.24650040181624522,
p 41.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.01060634]
 [ 0.04215265]
 [ 0.0073124 ]]

Simulating particle
Skip particle 50 because:does not pass magnet
Data added to pickle list for particle: 50
Data added to condensed list for particle: 50
Particle 51:
eta 2.982358090117098, phi -2.4768135562641107,
p 41.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.06268005]
 [-0.07997034]
 [-0.0073124 ]]

Simulating particle
Skip particle 51 because:does not pass magnet
Data added to pickle list for particle: 51
Data added to condensed list for particle: 51
Particle 52:
eta 4.370844388398341, phi -2.244225673512392,
p 41.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.0197651 ]
 [-0.01576961]
 [ 0.0073124 ]]

Simulating particle
Skip particle 52 because:does not pass magnet
Data added to pickle list for particle: 52
Data added to condensed list for particle: 52
Particle 53:
eta 3.3072990292529774, phi 2.161984333449077,
p 41.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.06088286]
 [-0.0408693 ]
 [-0.0073124 ]]

Simulating particle
Skip particle 53 because:does not pass magnet
Data added to pickle list for particle: 53
Data added to condensed list for particle: 53
Particle 54:
eta 3.14322926947731, phi -3.1139932850038945,
p 41.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.00238559]
 [-0.08641449]
 [ 0.0073124 ]]

Simulating particle
Skip particle 54 because:does not pass magnet
Data added to pickle list for particle: 54
Data added to condensed list for particle: 54
Particle 55:
eta 4.090082487088636, phi 0.8219732606234402,
p 41.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.02452751]
 [ 0.02279588]
 [-0.0073124 ]]

Simulating particle
Skip particle 55 because:does not pass magnet
Data added to pickle list for particle: 55
Data added to condensed list for particle: 55
Particle 56:
eta 2.2504887948604555, phi -1.1394383034292512,
p 41.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.19354345]
 [ 0.08908141]
 [ 0.0073124 ]]

Simulating particle
Data added to pickle list for particle: 56
Data added to condensed list for particle: 56
56 particles skipped because: only upstream