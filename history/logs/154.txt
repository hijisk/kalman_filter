This is the start of the log file generated at 09_04_17h17m53s

15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 1 particles...
Particle 0:
eta 2.529744823342673, phi -1.1471670849382551,
p 50.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.14620007]
 [ 0.06592639]
 [ 0.00599617]]

Simulating particle
Particle track 0 is saved
Initiating Kalman filtering for particle: 0
z: 0.05
The prediction: [[-0.00731049]
 [ 0.00310896]]
The measurement: [0.00332348]
The filtered state: [[-0.00731049]
 [ 0.00332327]]
z: 0.08
The prediction: [[-0.01169678]
 [ 0.00530042]]
The measurement: [-0.0116736]
The filtered state: [[-0.01167363]
 [ 0.00530042]]
z: 0.11
The prediction: [[-0.01605993]
 [ 0.00727757]]
The measurement: [0.00723357]
The filtered state: [[-0.01605993]
 [ 0.00725507]]
z: 0.14
The prediction: [[-0.02059246]
 [ 0.0092976 ]]
The measurement: [-0.02039863]
The filtered state: [[-0.02053201]
 [ 0.0092976 ]]
z: 0.17
The prediction: [[-0.02495411]
 [ 0.01127424]]
The measurement: [0.01117867]
The filtered state: [[-0.02495411]
 [ 0.01123815]]
z: 0.2
The prediction: [[-0.02937622]
 [ 0.0132127 ]]
The measurement: [-0.02935268]
The filtered state: [[-0.02919659]
 [ 0.0132127 ]]
z: 0.23
The prediction: [[-0.03353337]
 [ 0.01518726]]
The measurement: [0.01522574]
The filtered state: [[-0.03353337]
 [ 0.01520023]]
z: 0.26
The prediction: [[-0.03787017]
 [ 0.01717589]]
The measurement: [-0.03797299]
The filtered state: [[-0.0379696 ]
 [ 0.01717589]]
z: 2.33
The prediction: [[-0.33968684]
 [ 0.1535624 ]]
The measurement: [-0.34126739]
The filtered state: [[-0.34126547]
 [ 0.15356241]]
z: 2.37
The prediction: [[-0.3472897 ]
 [ 0.15626248]]
The measurement: [0.1547025]
The filtered state: [[-0.3472897 ]
 [ 0.15483651]]
z: 2.6
The prediction: [[-0.38111549]
 [ 0.16983703]]
The measurement: [-0.38090174]
The filtered state: [[-0.38099577]
 [ 0.16983702]]
z: 2.64
The prediction: [[-0.38703037]
 [ 0.17251103]]
The measurement: [0.17373986]
The filtered state: [[-0.38703037]
 [ 0.17316807]]
z: 7.86
The prediction: [[-1.23365244]
 [ 0.51505163]]
The measurement: [-1.21804558]
The filtered state: [[-1.22378971]
 [ 0.51501595]]
z: 7.87
The prediction: [[-1.22554408]
 [ 0.51567073]]
The measurement: [0.51845095]
The filtered state: [[-1.22554435]
 [ 0.51725908]]
z: 7.91
The prediction: [[-1.23273966]
 [ 0.51995226]]
The measurement: [-1.22623832]
The filtered state: [[-1.23020655]
 [ 0.51994818]]
z: 7.92
The prediction: [[-1.23195965]
 [ 0.52060504]]
The measurement: [0.52167666]
The filtered state: [[-1.23195969]
 [ 0.52099801]]
z: 7.98
The prediction: [[-1.24265839]
 [ 0.52500803]]
The measurement: [-1.23844249]
The filtered state: [[-1.24146479]
 [ 0.52500671]]
z: 7.99
The prediction: [[-1.24321837]
 [ 0.52566409]]
The measurement: [0.52730842]
The filtered state: [[-1.24321841]
 [ 0.52611129]]
z: 8.03
The prediction: [[-1.25023483]
 [ 0.52874313]]
The measurement: [-1.24633507]
The filtered state: [[-1.24936818]
 [ 0.52874238]]
z: 8.04
The prediction: [[-1.25112206]
 [ 0.52940034]]
The measurement: [0.5299714]
The filtered state: [[-1.25112207]
 [ 0.52952369]]
z: 8.54
The prediction: [[-1.33905388]
 [ 0.56242957]]
The measurement: [-1.33381042]
The filtered state: [[-1.33805361]
 [ 0.56242824]]
z: 8.55
The prediction: [[-1.33999244]
 [ 0.56315217]]
The measurement: [0.56271582]
The filtered state: [[-1.33999243]
 [ 0.56306616]]
z: 8.59
The prediction: [[-1.34704421]
 [ 0.5656982 ]]
The measurement: [-1.34213896]
The filtered state: [[-1.34625628]
 [ 0.5656973 ]]
z: 8.6
The prediction: [[-1.34801941]
 [ 0.56635531]]
The measurement: [0.56768602]
The filtered state: [[-1.34801943]
 [ 0.56657666]]
z: 8.66
The prediction: [[-1.3587776 ]
 [ 0.57059215]]
The measurement: [-1.3548084]
The filtered state: [[-1.35822684]
 [ 0.57059158]]
z: 8.67
The prediction: [[-1.3599909 ]
 [ 0.57124985]]
The measurement: [0.57118624]
The filtered state: [[-1.3599909 ]
 [ 0.57124065]]
z: 8.71
The prediction: [[-1.36722501]
 [ 0.57393954]]
The measurement: [-1.36317337]
The filtered state: [[-1.3667307 ]
 [ 0.57393906]]
z: 8.72
The prediction: [[-1.36849545]
 [ 0.57459732]]
The measurement: [0.57482704]
The filtered state: [[-1.36849545]
 [ 0.57462668]]
z: 9.23
The prediction: [[-1.45865585]
 [ 0.6082    ]]
The measurement: [-1.45247541]
The filtered state: [[-1.45801618]
 [ 0.6081989 ]]
z: 9.24
The prediction: [[-1.45978749]
 [ 0.60885719]]
The measurement: [0.60795104]
The filtered state: [[-1.45978748]
 [ 0.608743  ]]
z: 9.28
The prediction: [[-1.46687364]
 [ 0.61137568]]
The measurement: [-1.46082474]
The filtered state: [[-1.46631203]
 [ 0.61137477]]
z: 9.29
The prediction: [[-1.46808452]
 [ 0.61203294]]
The measurement: [0.6142846]
The filtered state: [[-1.46808455]
 [ 0.6122875 ]]
z: 9.35
The prediction: [[-1.47889866]
 [ 0.61630408]]
The measurement: [-1.47314207]
The filtered state: [[-1.47841797]
 [ 0.61630332]]
z: 9.36
The prediction: [[-1.48019181]
 [ 0.61696178]]
The measurement: [0.61721388]
The filtered state: [[-1.48019181]
 [ 0.61698775]]
z: 9.4
The prediction: [[-1.48746546]
 [ 0.61968754]]
The measurement: [-1.48151634]
The filtered state: [[-1.4870134 ]
 [ 0.61968685]]
z: 9.41
The prediction: [[-1.48878845]
 [ 0.62034533]]
The measurement: [0.61677191]
The filtered state: [[-1.48878841]
 [ 0.62000803]]
Particle 0:
eta 2.5545962307311316, phi -1.1364631325628438,
p 43.634916242431906, q 1.60217662e-19,
brem = False
output State:
[[-4.84186602e-03]
 [-6.07538774e-05]
 [-1.41871110e-01]
 [ 6.58106186e-02]
 [ 6.87083934e-03]]
Data added to list for particle: 0
data saved to data/toy_output/154