This is the start of the log file generated at 08_13_14h20m32s

15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 4 particles...
Particle 0:
eta 2.127284249986104, phi 1.1898855059511075,
p 50.0, q 1.60217662e-19,
brem = False
input State:
[[0.        ]
 [0.        ]
 [0.22442629]
 [0.08987582]
 [0.00599617]]

Simulating particle
Particle track 0 is saved
Initiating Kalman filtering for particle: 0
Particle 0:
eta 2.1265314273096743, phi 1.1929915631614596,
p 47.73910513619013, q 1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.002452661292937277]
y:   [-0.00021959416372106092]
tx:  [0.22487847341968947]
ty:  [0.08924740813992797]
q/p: [0.00628014493351317]]
Covariance matrix:
[[ 5.0e-06  6.0e-09  2.3e-06 -1.5e-08  1.8e-06]
 [ 6.0e-09  5.1e-06 -2.0e-09 -9.0e-07 -5.9e-10]
 [ 2.3e-06 -2.0e-09 -5.5e-06  5.4e-09 -3.3e-06]
 [-1.5e-08 -9.0e-07  5.4e-09  2.2e-06  1.6e-09]
 [ 1.8e-06 -5.9e-10 -3.3e-06  1.6e-09 -1.8e-06]]
Data added to list for particle: 0
Particle 1:
eta 2.5566733280453677, phi 0.9724485339304779,
p 50.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.12895021]
 [ 0.08790717]
 [-0.00599617]]

Simulating particle
Particle track 1 is saved
Initiating Kalman filtering for particle: 1
Particle 1:
eta 2.565920029837708, phi 0.9716094592301207,
p 51.88944995520298, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.00014816862242814368]
y:   [-0.00045948124681395304]
tx:  [0.1276760581248492]
ty:  [0.08719556697372541]
q/p: [-0.005777831515083059]]
Covariance matrix:
[[ 3.9e-06 -9.4e-10 -1.1e-06  2.0e-09 -3.7e-07]
 [-9.4e-10  5.2e-06  5.4e-10 -1.1e-06 -1.8e-10]
 [-1.1e-06  5.4e-10  1.8e-06 -9.9e-10  9.0e-07]
 [ 2.0e-09 -1.1e-06 -9.9e-10  2.3e-06  3.7e-10]
 [-3.7e-07 -1.8e-10  9.0e-07  3.7e-10  5.3e-07]]
Data added to list for particle: 1
Particle 2:
eta 4.108889821817326, phi -1.6864461433663374,
p 90.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.03264136]
 [-0.00379189]
 [ 0.00333121]]

Simulating particle
Particle track 2 is saved
Initiating Kalman filtering for particle: 2
Particle 2:
eta 4.123476559654517, phi -1.685765673387937,
p 88.62083531669514, q 1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.00011315486088516469]
y:   [2.7883648182687812e-05]
tx:  [-0.0321709671219993]
ty:  [-0.0037150579957426137]
q/p: [0.0033830475438434123]]
Covariance matrix:
[[ 3.1e-06 -1.8e-10  6.8e-07  2.2e-10  7.6e-07]
 [-1.8e-10  6.4e-06  4.6e-11 -2.0e-06  6.7e-12]
 [ 6.8e-07  4.6e-11 -1.5e-06 -5.7e-11 -1.0e-06]
 [ 2.2e-10 -2.0e-06 -5.7e-11  2.5e-06 -8.2e-12]
 [ 7.6e-07  6.7e-12 -1.0e-06 -8.2e-12 -6.0e-07]]
Data added to list for particle: 2
Particle 3:
eta 3.372653301654923, phi -2.671981647418247,
p 90.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.03107942]
 [-0.06124306]
 [-0.00333121]]

Simulating particle
Particle track 3 is saved
Initiating Kalman filtering for particle: 3
Particle 3:
eta 3.3796375091519284, phi -2.6678433511640107,
p 87.78949890322271, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.0009676270125805709]
y:   [0.0005916449425970229]
tx:  [-0.031114016047307895]
ty:  [-0.060687580773528184]
q/p: [-0.003415083842567544]]
Covariance matrix:
[[ 4.0e-06  1.3e-08 -1.0e-06 -2.5e-08 -3.0e-07]
 [ 1.3e-08  4.3e-06 -3.5e-10 -1.3e-06  5.6e-11]
 [-1.0e-06 -3.5e-10  1.3e-06  6.4e-10  6.1e-07]
 [-2.5e-08 -1.3e-06  6.4e-10  2.3e-06 -1.0e-10]
 [-3.0e-07  5.6e-11  6.1e-07 -1.0e-10  3.6e-07]]
Data added to list for particle: 3
data saved to data/toy_output/67