This is the start of the log file generated at 11_25_20h45m58s

magfield_ shape: cs, sim_stepsize: 0.001, detector_length: 11
15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 140 particles, from 30 to 100 GeV. Brem = False, ms = False

Particle 0:
eta 2.4497342277459424, phi 0.5133378478530113,
p 30.000000000000004, q 1.60217662e-19,
brem = False
input State:
[[-0.001     ]
 [-0.001     ]
 [ 0.08541433]
 [ 0.15151125]
 [ 0.00999362]]

Simulating particle
Initiating Kalman filtering for particle: 0
Particle 0:
eta 2.4537761687854855, phi 0.5142615274382449,
p 17.856475863465977, q 1.60217662e-19,
brem = False
output State:
[[-0.00098752]
 [-0.00098841]
 [ 0.08520399]
 [ 0.15081233]
 [ 0.0167899 ]]
Data added to pickle list for particle: 0
Particle 1:
eta 4.5943983340543015, phi -3.043001706713954,
p 30.000000000000004, q -1.60217662e-19,
brem = False
input State:
[[-0.001     ]
 [-0.001     ]
 [-0.00199015]
 [-0.02012048]
 [-0.00999362]]

Simulating particle
Initiating Kalman filtering for particle: 1
Particle 1:
eta 4.59984843074766, phi -3.0421441427678704,
p 48.21065580558597, q -1.60217662e-19,
brem = False
output State:
[[-0.00099464]
 [-0.00100707]
 [-0.00199649]
 [-0.0200094 ]
 [-0.00621872]]
Data added to pickle list for particle: 1
Particle 2:
eta 4.121880185884201, phi 2.4288482997864307,
p 31.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.02121065]
 [-0.02454051]
 [ 0.00967124]]

Simulating particle
Initiating Kalman filtering for particle: 2
Data added to pickle list for particle: 2