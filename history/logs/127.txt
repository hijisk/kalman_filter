This is the start of the log file generated at 09_02_14h12m10s

15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 400 particles...
Particle 0:
eta 3.07371916293671, phi 0.21014525200368508,
p 40.0, q 1.60217662e-19,
brem = False
input State:
[[0.        ]
 [0.        ]
 [0.01933655]
 [0.09065668]
 [0.00749521]]

Simulating particle
Particle track 0 is saved
Skip particle 0 because:The particle did not register in any subdetector
Data added to list for particle: 0
Particle 1:
eta 4.9848047689258665, phi 0.3356518997331075,
p 40.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.00450693]
 [ 0.01291931]
 [-0.00749521]]

Simulating particle
Particle track 1 is saved
Skip particle 1 because:The particle did not register in any subdetector
Data added to list for particle: 1
Particle 2:
eta 4.364072623284266, phi -1.9059074273714245,
p 40.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.02404095]
 [-0.00837216]
 [ 0.00749521]]

Simulating particle
Particle track 2 is saved
Skip particle 2 because:The particle did not register in any subdetector
Data added to list for particle: 2
Particle 3:
eta 4.373955541782991, phi 1.3417699134047067,
p 40.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.0245484 ]
 [ 0.00572264]
 [-0.00749521]]

Simulating particle
Particle track 3 is saved
Skip particle 3 because:The particle did not register in any subdetector
Data added to list for particle: 3
Particle 4:
eta 2.901884146758929, phi -1.0995231426085474,
p 40.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.09816189]
 [ 0.05002022]
 [ 0.00749521]]

Simulating particle
Particle track 4 is saved
Skip particle 4 because:The particle did not register in any subdetector
Data added to list for particle: 4
Particle 5:
eta 3.5769083533970183, phi 1.398270135475958,
p 40.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.0551369 ]
 [ 0.00960808]
 [-0.00749521]]

Simulating particle
Particle track 5 is saved
Skip particle 5 because:The particle did not register in any subdetector
Data added to list for particle: 5
Particle 6:
eta 4.568129328274657, phi -1.0160526597376152,
p 40.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.01764414]
 [ 0.01093321]
 [ 0.00749521]]

Simulating particle
Particle track 6 is saved
Skip particle 6 because:The particle did not register in any subdetector
Data added to list for particle: 6
Particle 7:
eta 3.5980024674740556, phi 1.6527254043209942,
p 40.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.05461398]
 [-0.00448451]
 [-0.00749521]]

Simulating particle
Particle track 7 is saved
Skip particle 7 because:The particle did not register in any subdetector
Data added to list for particle: 7
Particle 8:
eta 4.290020761613976, phi 0.6787841486911678,
p 40.0, q 1.60217662e-19,
brem = False
input State:
[[0.        ]
 [0.        ]
 [0.01721207]
 [0.02133766]
 [0.00749521]]

Simulating particle
Particle track 8 is saved
Skip particle 8 because:The particle did not register in any subdetector
Data added to list for particle: 8
Particle 9:
eta 3.907337329360826, phi -2.822188198619384,
p 40.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.01262414]
 [-0.03817068]
 [-0.00749521]]

Simulating particle
Particle track 9 is saved
Skip particle 9 because:The particle did not register in any subdetector
Data added to list for particle: 9
Particle 10:
eta 4.349938642107885, phi -0.6386454457609833,
p 40.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.01539123]
 [ 0.02073059]
 [ 0.00749521]]

Simulating particle
Particle track 10 is saved
Skip particle 10 because:The particle did not register in any subdetector
Data added to list for particle: 10
Particle 11:
eta 3.9590946553809956, phi 0.0003009316536297799,
p 40.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.00000000e+00]
 [ 0.00000000e+00]
 [ 1.14879632e-05]
 [ 3.81746578e-02]
 [-7.49521248e-03]]

Simulating particle
Particle track 11 is saved
Skip particle 11 because:The particle did not register in any subdetector
Data added to list for particle: 11
Particle 12:
eta 4.531840028798577, phi -1.5535457572373266,
p 40.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.02152101]
 [ 0.00037129]
 [ 0.00749521]]

Simulating particle
Particle track 12 is saved
Skip particle 12 because:The particle did not register in any subdetector
Data added to list for particle: 12
Particle 13:
eta 4.891037638393025, phi -1.2450044459218537,
p 40.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.01423758]
 [ 0.00480988]
 [-0.00749521]]

Simulating particle
Particle track 13 is saved
Skip particle 13 because:The particle did not register in any subdetector
Data added to list for particle: 13
Particle 14:
eta 3.4116582830692064, phi 1.022834260918041,
p 40.0, q 1.60217662e-19,
brem = False
input State:
[[0.        ]
 [0.        ]
 [0.05637502]
 [0.03440594]
 [0.00749521]]

Simulating particle
Particle track 14 is saved
Skip particle 14 because:The particle did not register in any subdetector
Data added to list for particle: 14
Particle 15:
eta 3.565414007463746, phi 1.8396629958686115,
p 40.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.05458178]
 [-0.01503938]
 [-0.00749521]]

Simulating particle
Particle track 15 is saved
Skip particle 15 because:The particle did not register in any subdetector
Data added to list for particle: 15
Particle 16:
eta 2.0618489396270263, phi 1.4367710580490363,
p 40.0, q 1.60217662e-19,
brem = False
input State:
[[0.        ]
 [0.        ]
 [0.25630344]
 [0.03455831]
 [0.00749521]]

Simulating particle
Particle track 16 is saved
Skip particle 16 because:The particle did not register in any subdetector
Data added to list for particle: 16
Particle 17:
eta 2.5494830276283467, phi -1.8689690880267007,
p 40.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.15026688]
 [-0.04618232]
 [-0.00749521]]

Simulating particle
Particle track 17 is saved
Skip particle 17 because:The particle did not register in any subdetector
Data added to list for particle: 17
Particle 18:
eta 2.276795206901344, phi -1.9178692917304874,
p 40.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.19504164]
 [-0.07054948]
 [ 0.00749521]]

Simulating particle
Particle track 18 is saved
Skip particle 18 because:The particle did not register in any subdetector
Data added to list for particle: 18
Particle 19:
eta 2.3229049568776086, phi 1.6283181902289277,
p 40.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.19754973]
 [-0.01137598]
 [-0.00749521]]

Simulating particle
Particle track 19 is saved
Skip particle 19 because:The particle did not register in any subdetector
Data added to list for particle: 19
Particle 20:
eta 3.3614920284650727, phi -2.0796463934504117,
p 41.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.06065148]
 [-0.03383443]
 [ 0.0073124 ]]

Simulating particle
Particle track 20 is saved
Skip particle 20 because:The particle did not register in any subdetector
Data added to list for particle: 20
Particle 21:
eta 4.426816935345881, phi -2.071452071581016,
p 41.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.02097404]
 [-0.01147604]
 [-0.0073124 ]]

Simulating particle
Particle track 21 is saved
Skip particle 21 because:The particle did not register in any subdetector
Data added to list for particle: 21
Particle 22:
eta 4.862021650148001, phi -2.056368925357391,
p 41.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.01368231]
 [-0.00722035]
 [ 0.0073124 ]]

Simulating particle
Particle track 22 is saved
Skip particle 22 because:The particle did not register in any subdetector
Data added to list for particle: 22
Particle 23:
eta 3.9324251005805966, phi 0.07619806184709389,
p 41.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.00298463]
 [ 0.03909347]
 [-0.0073124 ]]

Simulating particle
Particle track 23 is saved
Skip particle 23 because:The particle did not register in any subdetector
Data added to list for particle: 23
Particle 24:
eta 4.800274930605583, phi 1.4769038847776168,
p 41.0, q 1.60217662e-19,
brem = False
input State:
[[0.        ]
 [0.        ]
 [0.0163836 ]
 [0.00154283]
 [0.0073124 ]]

Simulating particle
Particle track 24 is saved
Skip particle 24 because:The particle did not register in any subdetector
Data added to list for particle: 24
Particle 25:
eta 3.0676051139061444, phi 1.6417817190851272,
p 41.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.09303199]
 [-0.00661503]
 [-0.0073124 ]]

Simulating particle
Particle track 25 is saved
Skip particle 25 because:The particle did not register in any subdetector
Data added to list for particle: 25
Particle 26:
eta 2.0412601886703605, phi -3.054130779931889,
p 41.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.0230767 ]
 [-0.26317556]
 [ 0.0073124 ]]

Simulating particle
Particle track 26 is saved
Skip particle 26 because:The particle did not register in any subdetector
Data added to list for particle: 26
Particle 27:
eta 3.2956300524926427, phi -1.1776456913984765,
p 41.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.06853091]
 [ 0.02842269]
 [-0.0073124 ]]

Simulating particle
Particle track 27 is saved
Skip particle 27 because:The particle did not register in any subdetector
Data added to list for particle: 27
Particle 28:
eta 2.338025182936542, phi 1.7528155780231092,
p 41.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.19163239]
 [-0.03527117]
 [ 0.0073124 ]]

Simulating particle
Particle track 28 is saved
Skip particle 28 because:The particle did not register in any subdetector
Data added to list for particle: 28
Particle 29:
eta 3.942403200604622, phi -0.9679518244749536,
p 41.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.03197514]
 [ 0.02200915]
 [-0.0073124 ]]

Simulating particle
Particle track 29 is saved
Skip particle 29 because:The particle did not register in any subdetector
Data added to list for particle: 29
Particle 30:
eta 4.221113469488922, phi -0.3849567825934914,
p 41.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.01102934]
 [ 0.02722139]
 [ 0.0073124 ]]

Simulating particle
Particle track 30 is saved
Skip particle 30 because:The particle did not register in any subdetector
Data added to list for particle: 30
Particle 31:
eta 3.5967880342477816, phi 2.825687326923017,
p 41.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.01704514]
 [-0.05214954]
 [-0.0073124 ]]

Simulating particle
Particle track 31 is saved
Skip particle 31 because:The particle did not register in any subdetector
Data added to list for particle: 31
Particle 32:
eta 3.9137515337524476, phi 2.987458876699487,
p 41.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.00613281]
 [-0.03947327]
 [ 0.0073124 ]]

Simulating particle
Particle track 32 is saved
Skip particle 32 because:The particle did not register in any subdetector
Data added to list for particle: 32
Particle 33:
eta 3.637316453910525, phi -0.25683396539329184,
p 41.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.01338233]
 [ 0.05095425]
 [-0.0073124 ]]

Simulating particle
Particle track 33 is saved
Skip particle 33 because:The particle did not register in any subdetector
Data added to list for particle: 33
Particle 34:
eta 3.2481307202634424, phi -0.23878309312593382,
p 41.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.01840387]
 [ 0.07560317]
 [ 0.0073124 ]]

Simulating particle
Particle track 34 is saved
Skip particle 34 because:The particle did not register in any subdetector
Data added to list for particle: 34
Particle 35:
eta 3.912250022057559, phi -2.8131050814846374,
p 41.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.       ]
 [ 0.       ]
 [-0.0129067]
 [-0.0378678]
 [-0.0073124]]

Simulating particle
Particle track 35 is saved
Skip particle 35 because:The particle did not register in any subdetector
Data added to list for particle: 35
Particle 36:
eta 4.730426119020456, phi -2.067826230809583,
p 41.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.01551158]
 [-0.00841429]
 [ 0.0073124 ]]

Simulating particle
Particle track 36 is saved
Skip particle 36 because:The particle did not register in any subdetector
Data added to list for particle: 36
Particle 37:
eta 4.10785803592053, phi -1.3862261779409342,
p 41.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.0323361 ]
 [ 0.00603699]
 [-0.0073124 ]]

Simulating particle
Particle track 37 is saved
Skip particle 37 because:The particle did not register in any subdetector
Data added to list for particle: 37
Particle 38:
eta 4.0212503709177945, phi -0.6630736767246541,
p 41.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.02208109]
 [ 0.02827134]
 [ 0.0073124 ]]

Simulating particle
Particle track 38 is saved
Skip particle 38 because:The particle did not register in any subdetector
Data added to list for particle: 38
Particle 39:
eta 3.6376178627042677, phi -1.21350309677041,
p 41.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.04934034]
 [ 0.01841952]
 [-0.0073124 ]]

Simulating particle
Particle track 39 is saved
Skip particle 39 because:The particle did not register in any subdetector
Data added to list for particle: 39
Particle 40:
eta 4.50152201555765, phi 1.7105122899990346,
p 42.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.02197073]
 [-0.00308979]
 [ 0.0071383 ]]

Simulating particle
Particle track 40 is saved
Skip particle 40 because:The particle did not register in any subdetector
Data added to list for particle: 40
Particle 41:
eta 2.2749851454249304, phi -0.6828647097939334,
p 42.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.13112094]
 [ 0.16119899]
 [-0.0071383 ]]

Simulating particle
Particle track 41 is saved
Skip particle 41 because:The particle did not register in any subdetector
Data added to list for particle: 41
Particle 42:
eta 3.2021505351736073, phi -1.4573943633104978,
p 42.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.0809607 ]
 [ 0.00922066]
 [ 0.0071383 ]]

Simulating particle
Particle track 42 is saved
Skip particle 42 because:The particle did not register in any subdetector
Data added to list for particle: 42
Particle 43:
eta 2.1420824169372663, phi 2.3492140913476134,
p 42.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.169535  ]
 [-0.16718452]
 [-0.0071383 ]]

Simulating particle
Particle track 43 is saved
Skip particle 43 because:The particle did not register in any subdetector
Data added to list for particle: 43
Particle 44:
eta 4.3925060005431975, phi 1.0536667712423935,
p 42.0, q 1.60217662e-19,
brem = False
input State:
[[0.       ]
 [0.       ]
 [0.0215078]
 [0.0122327]
 [0.0071383]]

Simulating particle
Particle track 44 is saved
Skip particle 44 because:The particle did not register in any subdetector
Data added to list for particle: 44
Particle 45:
eta 4.149466631200885, phi 2.049792957089518,
p 42.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.0280024 ]
 [-0.01454266]
 [-0.0071383 ]]

Simulating particle
Particle track 45 is saved
Skip particle 45 because:The particle did not register in any subdetector
Data added to list for particle: 45
Particle 46:
eta 4.2657526378529775, phi -0.6992049066595973,
p 42.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.01807778]
 [ 0.02149737]
 [ 0.0071383 ]]

Simulating particle
Particle track 46 is saved
Skip particle 46 because:The particle did not register in any subdetector
Data added to list for particle: 46
Particle 47:
eta 2.20292618174394, phi -0.9458845022826764,
p 42.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.18141514]
 [ 0.13086388]
 [-0.0071383 ]]

Simulating particle
Particle track 47 is saved
Skip particle 47 because:The particle did not register in any subdetector
Data added to list for particle: 47
Particle 48:
eta 4.2090182009962, phi -2.7963713518801767,
p 42.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.01006026]
 [-0.02797451]
 [ 0.0071383 ]]

Simulating particle
Particle track 48 is saved
Skip particle 48 because:The particle did not register in any subdetector
Data added to list for particle: 48
Particle 49:
eta 3.5342621613355294, phi -0.32821861356043835,
p 42.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.01882898]
 [ 0.05529224]
 [-0.0071383 ]]

Simulating particle
Particle track 49 is saved
Skip particle 49 because:The particle did not register in any subdetector
Data added to list for particle: 49
Particle 50:
eta 4.121421020186387, phi -2.9156530486116825,
p 42.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.00726984]
 [-0.03162665]
 [ 0.0071383 ]]

Simulating particle
Particle track 50 is saved
Skip particle 50 because:The particle did not register in any subdetector
Data added to list for particle: 50
Particle 51:
eta 2.513130380729252, phi 2.084064004263994,
p 42.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.14208277]
 [-0.0800859 ]
 [-0.0071383 ]]

Simulating particle
Particle track 51 is saved
Skip particle 51 because:The particle did not register in any subdetector
Data added to list for particle: 51
Particle 52:
eta 2.976753209893121, phi -1.7571685338573446,
p 42.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.10041188]
 [-0.01893371]
 [ 0.0071383 ]]

Simulating particle
Particle track 52 is saved
Skip particle 52 because:The particle did not register in any subdetector
Data added to list for particle: 52
Particle 53:
eta 4.93216246565394, phi 1.5766193041481118,
p 42.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.00000000e+00]
 [ 0.00000000e+00]
 [ 1.44222916e-02]
 [-8.39816267e-05]
 [-7.13829760e-03]]

Simulating particle
Particle track 53 is saved
Skip particle 53 because:The particle did not register in any subdetector
Data added to list for particle: 53
Particle 54:
eta 2.5893436726907293, phi -3.0077341597037903,
p 42.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.02015092]
 [-0.14963877]
 [ 0.0071383 ]]

Simulating particle
Particle track 54 is saved
Skip particle 54 because:The particle did not register in any subdetector
Data added to list for particle: 54
Particle 55:
eta 2.83272862270895, phi -0.7832161744546478,
p 42.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.0833362 ]
 [ 0.08370068]
 [-0.0071383 ]]

Simulating particle
Particle track 55 is saved
Skip particle 55 because:The particle did not register in any subdetector
Data added to list for particle: 55
Particle 56:
eta 2.1201213537505743, phi -0.6860135255340997,
p 42.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.15427361]
 [ 0.1884474 ]
 [ 0.0071383 ]]

Simulating particle
Particle track 56 is saved
Skip particle 56 because:The particle did not register in any subdetector
Data added to list for particle: 56
Particle 57:
eta 3.5751177829573697, phi -0.6431313309919439,
p 42.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.03362436]
 [ 0.04486704]
 [-0.0071383 ]]

Simulating particle
Particle track 57 is saved
Skip particle 57 because:The particle did not register in any subdetector
Data added to list for particle: 57
Particle 58:
eta 3.198882398900598, phi -0.815284371053555,
p 42.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.05950875]
 [ 0.05605399]
 [ 0.0071383 ]]

Simulating particle
Particle track 58 is saved
Skip particle 58 because:The particle did not register in any subdetector
Data added to list for particle: 58
Particle 59:
eta 3.374683579819105, phi -1.9804183029826135,
p 42.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.06286813]
 [-0.0272962 ]
 [-0.0071383 ]]

Simulating particle
Particle track 59 is saved
Skip particle 59 because:The particle did not register in any subdetector
Data added to list for particle: 59
Particle 60:
eta 3.948685071587043, phi 2.983159197810341,
p 43.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.00608594]
 [-0.03809129]
 [ 0.00697229]]

Simulating particle
Particle track 60 is saved
Skip particle 60 because:The particle did not register in any subdetector
Data added to list for particle: 60
Particle 61:
eta 2.753060497930327, phi -0.790695824659952,
p 43.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.09097713]
 [ 0.09001827]
 [-0.00697229]]

Simulating particle
Particle track 61 is saved
Skip particle 61 because:The particle did not register in any subdetector
Data added to list for particle: 61
Particle 62:
eta 3.54324475550912, phi 1.2821318596772,
p 43.0, q 1.60217662e-19,
brem = False
input State:
[[0.        ]
 [0.        ]
 [0.05549201]
 [0.01647885]
 [0.00697229]]

Simulating particle
Particle track 62 is saved
Skip particle 62 because:The particle did not register in any subdetector
Data added to list for particle: 62
Particle 63:
eta 3.9269092016240235, phi 0.04489533664324319,
p 43.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.00176937]
 [ 0.03938454]
 [-0.00697229]]

Simulating particle
Particle track 63 is saved
Skip particle 63 because:The particle did not register in any subdetector
Data added to list for particle: 63
Particle 64:
eta 3.849778552421015, phi -2.9782588767480056,
p 43.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.0069252 ]
 [-0.04202137]
 [ 0.00697229]]

Simulating particle
Particle track 64 is saved
Skip particle 64 because:The particle did not register in any subdetector
Data added to list for particle: 64
Particle 65:
eta 3.9183294008492777, phi -0.1217454588825788,
p 43.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.00482917]
 [ 0.03946992]
 [-0.00697229]]

Simulating particle
Particle track 65 is saved
Skip particle 65 because:The particle did not register in any subdetector
Data added to list for particle: 65
Particle 66:
eta 3.0040488047381806, phi -1.5660246449554236,
p 43.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.0994151 ]
 [ 0.00047438]
 [ 0.00697229]]

Simulating particle
Particle track 66 is saved
Skip particle 66 because:The particle did not register in any subdetector
Data added to list for particle: 66
Particle 67:
eta 4.8150265279637035, phi 0.9746932915660759,
p 43.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.01341846]
 [ 0.0091035 ]
 [-0.00697229]]

Simulating particle
Particle track 67 is saved
Skip particle 67 because:The particle did not register in any subdetector
Data added to list for particle: 67
Particle 68:
eta 3.289679459698447, phi -2.6457644939695006,
p 43.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.03550848]
 [-0.06564729]
 [ 0.00697229]]

Simulating particle
Particle track 68 is saved
Skip particle 68 because:The particle did not register in any subdetector
Data added to list for particle: 68
Particle 69:
eta 3.2310143084648133, phi 3.0940861684805427,
p 43.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.00375912]
 [-0.0790691 ]
 [-0.00697229]]

Simulating particle
Particle track 69 is saved
Skip particle 69 because:The particle did not register in any subdetector
Data added to list for particle: 69
Particle 70:
eta 3.993154987966013, phi -2.0819515726339506,
p 43.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.03217945]
 [-0.0180487 ]
 [ 0.00697229]]

Simulating particle
Particle track 70 is saved
Skip particle 70 because:The particle did not register in any subdetector
Data added to list for particle: 70
Particle 71:
eta 3.8964200957797943, phi -1.928612518279206,
p 43.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.03807144]
 [-0.01423536]
 [-0.00697229]]

Simulating particle
Particle track 71 is saved
Skip particle 71 because:The particle did not register in any subdetector
Data added to list for particle: 71
Particle 72:
eta 3.93344153153505, phi 0.42458548794133333,
p 43.0, q 1.60217662e-19,
brem = False
input State:
[[0.        ]
 [0.        ]
 [0.01613473]
 [0.03568969]
 [0.00697229]]

Simulating particle
Particle track 72 is saved
Skip particle 72 because:The particle did not register in any subdetector
Data added to list for particle: 72
data saved to data/toy_output/127
73 particles skipped because: The particle did not register in any subdetector