This is the start of the log file generated at 11_04_19h31m00s

magfield_ shape: cs, sim_stepsize: 0.001, detector_length: 11
15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 90 particles, from 10 to 100 GeV. Brem = False, ms = False

Particle 0:
eta 3.9825348521616393, phi 1.5767378595275225,
p 10.0, q 1.60217662e-19,
brem = False
input State:
[[-0.001     ]
 [-0.001     ]
 [ 0.03728896]
 [-0.00022156]
 [ 0.02998085]]

Simulating particle
Initiating Kalman filtering for particle: 0
z = 0
F[0,2]: 
O(h): 0.001
O(h^2): 3.5503988478575575e-17
O(h^3): 3.2446536494841773e-28
F[0,3]: O(h^2): -4.7716434298246375e-20
O(h^3): -2.258831655355356e-33
F[0,4]: O(h^2): -1.3417211407031199e-14
O(h^3): -2.1570095030619515e-26
F[2,2]: 
O(h^0): 1
O(h^1): 7.100797695715115e-14
O(h^2): 9.733960948452534e-25
F[2,3] 
O(h^1): -9.543286859649276e-17
O(h^2): -6.77649496606607e-30
F[2,4] 
O(h^1): -2.6834422814062403e-11
O(h^2): -1.9054580857843482e-24
Exception occured at particle 0
Data added to pickle list for particle: 0