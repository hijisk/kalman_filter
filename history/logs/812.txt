This is the start of the log file generated at 02_11_23h31m55s

magfield_ shape: cs, sim_stepsize: 0.01, detector_length: 11
15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 T_station-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 100 particles, from 1 to 100 GeV. Brem = False, ms = True

Particle 0:
eta 4.803122396695555, phi -0.7787271314387088,
p 1.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.01152545]
 [ 0.01168026]
 [ 0.2998085 ]]

Simulating particle
Initiating Kalman filtering for particle: 0
Particle 0:
eta 4.755255893593496, phi -0.7303345268264578,
p 1.1617957309205333, q 1.60217662e-19,
brem = False
output State:
[[ 0.00075168]
 [ 0.0012138 ]
 [-0.01148376]
 [ 0.01282356]
 [ 0.25805612]]
Data added to pickle list for particle: 0
Data added to condensed list for particle: 0
Particle 1:
eta 2.04046376226594, phi -2.3586526915026855,
p 1.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.18650106]
 [-0.18742024]
 [-0.2998085 ]]

Simulating particle
Initiating Kalman filtering for particle: 1
Skip particle 1 because:('overflow encountered in double_scalars',)
Data added to pickle list for particle: 1
Data added to condensed list for particle: 1
Particle 2:
eta 2.269962526828513, phi 0.2618088053568079,
p 1.0, q 1.60217662e-19,
brem = False
input State:
[[0.001     ]
 [0.001     ]
 [0.05405924]
 [0.20174424]
 [0.2998085 ]]

Simulating particle
Initiating Kalman filtering for particle: 2
Skip particle 2 because:('overflow encountered in double_scalars',)
Data added to pickle list for particle: 2
Data added to condensed list for particle: 2
Particle 3:
eta 4.228584984778109, phi 2.828052178654792,
p 1.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.00899136]
 [-0.02773094]
 [-0.2998085 ]]

Simulating particle
Initiating Kalman filtering for particle: 3
Particle 3:
eta 4.200933597909119, phi 2.8178031432962003,
p 0.4482194461613444, q 1.60217662e-19,
brem = False
output State:
[[ 1.16947428e-03]
 [ 4.78279130e-04]
 [ 9.53526426e-03]
 [-2.84125547e-02]
 [ 6.68887755e-01]]
Data added to pickle list for particle: 3
Data added to condensed list for particle: 3
Particle 4:
eta 3.2190748652443846, phi 0.7657987800738087,
p 1.0, q 1.60217662e-19,
brem = False
input State:
[[0.001     ]
 [0.001     ]
 [0.05552681]
 [0.0577472 ]
 [0.2998085 ]]

Simulating particle
Initiating Kalman filtering for particle: 4
Data added to pickle list for particle: 4
Data added to condensed list for particle: 4
2 particles skipped because: FloatingPointError