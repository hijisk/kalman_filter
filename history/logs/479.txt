This is the start of the log file generated at 11_04_19h33m08s

magfield_ shape: cs, sim_stepsize: 0.001, detector_length: 11
15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 90 particles, from 10 to 100 GeV. Brem = False, ms = False

Particle 0:
eta 2.4338938087775976, phi -1.8104844721864768,
p 10.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [-0.001     ]
 [-0.17169578]
 [-0.04196008]
 [ 0.02998085]]

Simulating particle
Initiating Kalman filtering for particle: 0
z = 0
F[0,2]: 
O(h): 0.001
O(h^2): -2.1925669803513123e-16
O(h^3): 3.871378729132442e-28
F[0,3]: O(h^2): -1.7111028038666618e-17
O(h^3): 5.005118260094449e-30
F[0,4]: O(h^2): -1.4061094282459617e-14
O(h^3): -2.297424815288955e-26
F[2,2]: 
O(h^0): 1
O(h^1): -4.3851339607026253e-13
O(h^2): 1.1614136187397328e-24
F[2,3] 
O(h^1): -3.4222056077333234e-14
O(h^2): 1.501535478028335e-26
F[2,4] 
O(h^1): -2.8122188564919237e-11
O(h^2): 1.2338961678575882e-23
Exception occured at particle 0
Data added to pickle list for particle: 0