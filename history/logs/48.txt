This is the start of the log file generated at 08_09_23h34m04s

15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 20 particles...
Particle 0:
eta 3.185953667625532, phi 1.40311463815683,
p 3.0, q 1.60217662e-19,
brem = False
input State:
[[0.        ]
 [0.        ]
 [0.08165754]
 [0.01382227]
 [0.09993617]]

Simulating particle
Particle track 0 is saved
Initiating Kalman filtering for particle: 0
Particle 0:
eta 2.971743185150684, phi 1.4473547483743183,
p 2.788457992149569, q 1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.0019876607346664896]
y:   [0.001578874340562888]
tx:  [0.1019158272575739]
ty:  [0.01264494305891209]
q/p: [0.10751766750496405]]
Covariance matrix:
[[ 4.5e-06  1.0e-10 -2.8e-06 -7.7e-10 -9.8e-07]
 [ 1.0e-10  3.9e-06 -4.5e-10 -1.5e-06 -8.7e-10]
 [-2.8e-06 -4.5e-10  8.1e-06  4.1e-09  2.3e-06]
 [-7.7e-10 -1.5e-06  4.1e-09  9.6e-06  2.3e-09]
 [-9.8e-07 -8.7e-10  2.3e-06  2.3e-09  2.4e-06]]
Data added to list for particle: 0
Particle 1:
eta 3.3780254018443285, phi 0.5432199881804151,
p 3.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.03530859]
 [ 0.05847583]
 [-0.09993617]]

Simulating particle
Particle track 1 is saved
Initiating Kalman filtering for particle: 1
Particle 1:
eta 3.1568473227130656, phi 0.8318066992301387,
p 4.949293466740916, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.005805676186780547]
y:   [0.008233100131038165]
tx:  [0.06303013721979288]
ty:  [0.05743548691204289]
q/p: [-0.06057601984327661]]
Covariance matrix:
[[ 4.2e-06  5.8e-10 -2.3e-06 -2.2e-09 -7.0e-07]
 [ 5.8e-10  4.2e-06 -2.5e-09 -2.3e-06  5.7e-09]
 [-2.3e-06 -2.5e-09  7.5e-06  1.1e-08  1.8e-06]
 [-2.2e-09 -2.3e-06  1.1e-08  9.7e-06 -1.8e-08]
 [-7.0e-07  5.7e-09  1.8e-06 -1.8e-08  2.0e-06]]
Data added to list for particle: 1
Particle 2:
eta 2.792452008010418, phi 0.49844549385056236,
p 12.999999999999998, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.05880308]
 [-0.10803714]
 [ 0.02306219]]

Simulating particle
Particle track 2 is saved
Initiating Kalman filtering for particle: 2
Too small a gyroradius in calculating jacobian at z = 7.99
at vector index 0 and state vector
[[ 4.20418877]
 [-0.74338069]
 [19.30464731]
 [-0.09236734]
 [-2.2664104 ]]
Skip particle 2 because:The particle gets a very steep slope
                in the taylor expansion at z=7.99, with state =[[ 4.20418877]
 [-0.74338069]
 [19.30464731]
 [-0.09236734]
 [-2.2664104 ]]
Data added to list for particle: 2
Particle 3:
eta 4.3941896905644, phi -0.260779804678414,
p 12.999999999999998, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.0063689 ]
 [-0.02386635]
 [-0.02306219]]

Simulating particle
Particle track 3 is saved
Initiating Kalman filtering for particle: 3
Particle 3:
eta 3.7857873024189765, phi -1.020491369430288,
p 77.34482735980791, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.011115756480095496]
y:   [-0.010673343014554937]
tx:  [0.03870194396779218]
ty:  [-0.023744605605849064]
q/p: [-0.0038762579151775563]]
Covariance matrix:
[[ 5.4e-06 -2.7e-10 -2.4e-06  4.6e-10 -9.4e-07]
 [-2.7e-10  7.9e-06  6.3e-10 -1.6e-06 -5.1e-10]
 [-2.4e-06  6.3e-10  4.5e-06 -1.1e-09  2.2e-06]
 [ 4.6e-10 -1.6e-06 -1.1e-09  3.0e-06  8.7e-10]
 [-9.4e-07 -5.1e-10  2.2e-06  8.7e-10  1.3e-06]]
Data added to list for particle: 3
Particle 4:
eta 2.861827902001851, phi 0.8256317394603052,
p 23.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.08430411]
 [-0.07777939]
 [ 0.01303515]]

Simulating particle
Particle track 4 is saved
Initiating Kalman filtering for particle: 4
Particle 4:
eta 1.6537207194778076, phi -1.3739743694951139,
p 0.9918003096903625, q 1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.16212727285300696]
y:   [-0.0055633367046709]
tx:  [0.3895462535480584]
ty:  [-0.07767689482672088]
q/p: [0.3022871603509546]]
Covariance matrix:
[[ 9.3e-06 -5.2e-09 -1.8e-05  1.6e-08 -1.1e-05]
 [-5.2e-09  3.9e-06  1.6e-08 -8.0e-07  1.0e-08]
 [-1.8e-05  1.6e-08  5.3e-05 -4.8e-08  3.2e-05]
 [ 1.6e-08 -8.0e-07 -4.8e-08  2.4e-06 -3.2e-08]
 [-1.1e-05  1.0e-08  3.2e-05 -3.2e-08  2.0e-05]]
Data added to list for particle: 4
Particle 5:
eta 3.664389445055795, phi -0.7260470834246608,
p 23.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.03404129]
 [ 0.03834239]
 [-0.01303515]]

Simulating particle
Particle track 5 is saved
Initiating Kalman filtering for particle: 5
Particle 5:
eta 3.9159777300026573, phi -0.3010945362540646,
p 3471.5272745608468, q 1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.01571058251155946]
y:   [0.006325743582839507]
tx:  [-0.011820497362657459]
ty:  [0.03806483023461395]
q/p: [8.636213272713612e-05]]
Covariance matrix:
[[ 3.6e-06  1.7e-10 -1.7e-06 -3.9e-10 -7.0e-07]
 [ 1.7e-10  3.3e-06 -3.5e-10 -9.4e-07  2.1e-10]
 [-1.7e-06 -3.5e-10  2.9e-06  8.1e-10  1.5e-06]
 [-3.9e-10 -9.4e-07  8.1e-10  2.4e-06 -5.2e-10]
 [-7.0e-07  2.1e-10  1.5e-06 -5.2e-10  8.8e-07]]
Data added to list for particle: 5
Particle 6:
eta 3.43245582090051, phi 1.0259997249224337,
p 33.0, q 1.60217662e-19,
brem = False
input State:
[[0.        ]
 [0.        ]
 [0.05531861]
 [0.03352134]
 [0.00908511]]

Simulating particle
Particle track 6 is saved
Initiating Kalman filtering for particle: 6
Particle 6:
eta 3.4007002617216076, phi -1.050238495476955,
p 5.107362280539885, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.04715386289250172]
y:   [0.004694301779158096]
tx:  [-0.05792931319352463]
ty:  [0.033211035746580575]
q/p: [-0.058701240049845275]]
Covariance matrix:
[[ 2.0e-06 -5.9e-10  4.0e-06  1.4e-09  2.7e-06]
 [-5.9e-10  4.0e-06  1.7e-09 -9.2e-07  7.4e-10]
 [ 4.0e-06  1.7e-09 -1.2e-05 -4.1e-09 -7.6e-06]
 [ 1.4e-09 -9.2e-07 -4.1e-09  2.3e-06 -1.8e-09]
 [ 2.7e-06  7.4e-10 -7.6e-06 -1.8e-09 -4.6e-06]]
Data added to list for particle: 6
Particle 7:
eta 3.742966465227259, phi -1.0592681786297669,
p 33.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.04132751]
 [-0.02319989]
 [-0.00908511]]

Simulating particle
Particle track 7 is saved
Initiating Kalman filtering for particle: 7
Particle 7:
eta 3.4534993727609877, phi -1.1997272457679664,
p 188.70791657442274, q 1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.0018322615042137237]
y:   [-0.003843641755085777]
tx:  [0.05902244133245867]
ty:  [-0.022965253210093666]
q/p: [0.001588743623976466]]
Covariance matrix:
[[ 3.5e-06 -7.9e-11 -1.5e-06  2.0e-10 -5.7e-07]
 [-7.9e-11  3.3e-06  2.0e-10 -9.3e-07 -7.9e-11]
 [-1.5e-06  2.0e-10  2.4e-06 -5.1e-10  1.2e-06]
 [ 2.0e-10 -9.3e-07 -5.1e-10  2.3e-06  2.0e-10]
 [-5.7e-07 -7.9e-11  1.2e-06  2.0e-10  7.2e-07]]
Data added to list for particle: 7
Particle 8:
eta 2.961429555715369, phi 0.5479617152236916,
p 43.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.05405758]
 [-0.08857481]
 [ 0.00697229]]

Simulating particle
Particle track 8 is saved
Initiating Kalman filtering for particle: 8
Particle 8:
eta 2.692836717567484, phi 0.9379265627109199,
p 11.160193441493426, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.01440965399147194]
y:   [-0.007404454141997003]
tx:  [-0.10966169702080641]
ty:  [-0.08043897253848506]
q/p: [-0.026864095216917447]]
Covariance matrix:
[[ 2.9e-06  1.7e-08  1.5e-06 -1.4e-07  1.3e-06]
 [ 1.7e-08  2.4e-05 -1.8e-07 -1.7e-04 -3.1e-09]
 [ 1.5e-06 -1.8e-07 -5.1e-06  1.5e-06 -3.9e-06]
 [-1.4e-07 -1.7e-04  1.5e-06  1.4e-03  2.2e-08]
 [ 1.3e-06 -3.1e-09 -3.9e-06  2.2e-08 -2.3e-06]]
Data added to list for particle: 8
Particle 9:
eta 2.145161118957254, phi -0.7152996908879289,
p 43.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.15566461]
 [ 0.17917487]
 [-0.00697229]]

Simulating particle
Particle track 9 is saved
Initiating Kalman filtering for particle: 9
Particle 9:
eta 2.1945230680694756, phi -0.6639675479537005,
p 244.36223715565126, q 1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.015022739565001546]
y:   [0.008457380742883188]
tx:  [-0.13903992998480347]
ty:  [0.17769091283243718]
q/p: [0.0012269019253598017]]
Covariance matrix:
[[ 7.1e-06  4.5e-10 -1.8e-06 -1.1e-09 -5.1e-07]
 [ 4.5e-10  5.0e-06 -9.3e-10 -7.9e-07  3.4e-10]
 [-1.8e-06 -9.3e-10  2.2e-06  2.0e-09  1.0e-06]
 [-1.1e-09 -7.9e-07  2.0e-09  2.3e-06 -8.8e-10]
 [-5.1e-07  3.4e-10  1.0e-06 -8.8e-10  5.8e-07]]
Data added to list for particle: 9
Particle 10:
eta 2.096619749842594, phi -0.4415971581849832,
p 53.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.10663616]
 [ 0.22557369]
 [ 0.00565676]]

Simulating particle
Particle track 10 is saved
Initiating Kalman filtering for particle: 10
Particle 10:
eta 2.030402581407883, phi -0.5764858784139905,
p 17.85762251594941, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.014703715642549332]
y:   [0.01133091104342603]
tx:  [-0.1456293406710833]
ty:  [0.22399089163044456]
q/p: [-0.016788824995249238]]
Covariance matrix:
[[ 5.5e-06 -2.2e-09  1.4e-06  5.0e-09  1.3e-06]
 [-2.2e-09  7.4e-06  4.3e-09 -1.0e-06  1.2e-09]
 [ 1.4e-06  4.3e-09 -4.2e-06 -1.0e-08 -2.6e-06]
 [ 5.0e-09 -1.0e-06 -1.0e-08  2.3e-06 -2.8e-09]
 [ 1.3e-06  1.2e-09 -2.6e-06 -2.8e-09 -1.5e-06]]
Data added to list for particle: 10
Particle 11:
eta 2.7045719417865985, phi -0.8862954700141935,
p 53.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.10412389]
 [-0.08497892]
 [-0.00565676]]

Simulating particle
Particle track 11 is saved
Initiating Kalman filtering for particle: 11
Particle 11:
eta 2.6377200155761464, phi -0.9453777227158612,
p 145.9111752520602, q 1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.0034068855704400868]
y:   [-0.006276806674176836]
tx:  [0.11656819054268533]
ty:  [-0.08417635434900245]
q/p: [0.0020547329478607834]]
Covariance matrix:
[[ 3.7e-06 -1.5e-10 -9.7e-07  4.7e-10 -2.9e-07]
 [-1.5e-10  3.9e-06  4.4e-10 -7.8e-07 -1.2e-10]
 [-9.7e-07  4.4e-10  1.8e-06 -1.4e-09  8.7e-07]
 [ 4.7e-10 -7.8e-07 -1.4e-09  2.3e-06  3.7e-10]
 [-2.9e-07 -1.2e-10  8.7e-07  3.7e-10  5.2e-07]]
Data added to list for particle: 11
Particle 12:
eta 3.618985496571098, phi 0.7764507255035297,
p 62.99999999999999, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.03760114]
 [-0.0382801 ]
 [ 0.00475887]]

Simulating particle
Particle track 12 is saved
Initiating Kalman filtering for particle: 12
Particle 12:
eta 3.3080371058985727, phi 1.0292069144778955,
p 29.041114754127126, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.005974178605212878]
y:   [-0.0063989878984334475]
tx:  [-0.06278773163271228]
ty:  [-0.03777260952738779]
q/p: [-0.010323587843985578]]
Covariance matrix:
[[ 2.4e-06  2.0e-10  9.1e-07 -5.0e-10  8.2e-07]
 [ 2.0e-10  3.3e-06 -4.4e-10 -9.2e-07 -9.6e-11]
 [ 9.1e-07 -4.4e-10 -2.6e-06  1.1e-09 -1.7e-06]
 [-5.0e-10 -9.2e-07  1.1e-09  2.3e-06  2.4e-10]
 [ 8.2e-07 -9.6e-11 -1.7e-06  2.4e-10 -1.0e-06]]
Data added to list for particle: 12
Particle 13:
eta 4.917929865318659, phi -0.07993556420718907,
p 62.99999999999999, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.00116816]
 [-0.01458258]
 [-0.00475887]]

Simulating particle
Particle track 13 is saved
Initiating Kalman filtering for particle: 13
Particle 13:
eta 4.5863829427618, phi -0.7944699400284255,
p 111.60151640459975, q 1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.005335472361951368]
y:   [-0.008691417166758026]
tx:  [0.014541981376689928]
ty:  [-0.014280503062893156]
q/p: [0.0026864195838036133]]
Covariance matrix:
[[ 6.9e-06 -5.0e-11 -1.7e-06  1.1e-10 -4.1e-07]
 [-5.0e-11  7.5e-06  1.1e-10 -1.2e-06 -1.8e-11]
 [-1.7e-06  1.1e-10  2.0e-06 -2.5e-10  9.0e-07]
 [ 1.1e-10 -1.2e-06 -2.5e-10  2.6e-06  4.0e-11]
 [-4.1e-07 -1.8e-11  9.0e-07  4.0e-11  5.0e-07]]
Data added to list for particle: 13
Particle 14:
eta 4.863301629782874, phi 0.9680312195169988,
p 73.0, q 1.60217662e-19,
brem = False
input State:
[[0.        ]
 [0.        ]
 [0.01272792]
 [0.0087594 ]
 [0.00410697]]

Simulating particle
Particle track 14 is saved
Initiating Kalman filtering for particle: 14
Particle 14:
eta 5.219552381404116, phi -0.6532664620437784,
p 45.655978616613645, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.016273742710530406]
y:   [0.005190686305121737]
tx:  [-0.006576106680417842]
ty:  [0.008592046150404261]
q/p: [-0.006566686518080688]]
Covariance matrix:
[[ 6.1e-06 -4.0e-11  1.6e-07  8.8e-11  6.0e-07]
 [-4.0e-11  7.5e-06  9.4e-11 -1.1e-06  1.2e-11]
 [ 1.6e-07  9.4e-11 -2.0e-06 -2.1e-10 -1.3e-06]
 [ 8.8e-11 -1.1e-06 -2.1e-10  2.6e-06 -2.6e-11]
 [ 6.0e-07  1.2e-11 -1.3e-06 -2.6e-11 -7.2e-07]]
Data added to list for particle: 14
Particle 15:
eta 2.2396655708844886, phi 1.0134562295907283,
p 73.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.18282912]
 [-0.1139483 ]
 [-0.00410697]]

Simulating particle
Particle track 15 is saved
Initiating Kalman filtering for particle: 15
Particle 15:
eta 2.289731674930594, phi 0.9853071489841783,
p 182.49689148979718, q 1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.014006901932414762]
y:   [-0.005350500789944251]
tx:  [-0.1705950903842862]
ty:  [-0.11311174887623988]
q/p: [0.0016428142792133987]]
Covariance matrix:
[[ 6.9e-06 -1.1e-10 -1.5e-06  2.6e-10 -3.6e-07]
 [-1.1e-10  5.0e-06  4.3e-10 -7.9e-07 -9.2e-11]
 [-1.5e-06  4.3e-10  1.6e-06 -1.1e-09  7.2e-07]
 [ 2.6e-10 -7.9e-07 -1.1e-09  2.2e-06  2.5e-10]
 [-3.6e-07 -9.2e-11  7.2e-07  2.5e-10  4.1e-07]]
Data added to list for particle: 15
Particle 16:
eta 2.6674129707080376, phi 0.7124729332654267,
p 83.0, q 1.60217662e-19,
brem = False
input State:
[[0.        ]
 [0.        ]
 [0.09121561]
 [0.10559346]
 [0.00361215]]

Simulating particle
Particle track 16 is saved
Initiating Kalman filtering for particle: 16
Particle 16:
eta 2.7556334451811915, phi 0.6065249784183018,
p 43.515830146817926, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.012618926583773322]
y:   [0.007750792287112007]
tx:  [0.07276440060292039]
ty:  [0.10488428488776494]
q/p: [-0.006889642188600655]]
Covariance matrix:
[[ 3.4e-06 -2.3e-10  1.6e-07  6.5e-10  3.8e-07]
 [-2.3e-10  3.9e-06  6.7e-10 -7.8e-07  1.2e-10]
 [ 1.6e-07  6.7e-10 -1.7e-06 -1.9e-09 -1.2e-06]
 [ 6.5e-10 -7.8e-07 -1.9e-09  2.3e-06 -3.5e-10]
 [ 3.8e-07  1.2e-10 -1.2e-06 -3.5e-10 -6.9e-07]]
Data added to list for particle: 16
Particle 17:
eta 4.533961172026806, phi 0.757808499924559,
p 83.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.01476291]
 [-0.01560085]
 [-0.00361215]]

Simulating particle
Particle track 17 is saved
Initiating Kalman filtering for particle: 17
Particle 17:
eta 4.832999902819029, phi 0.27390862218929163,
p 130.99404164928902, q 1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.010947554699281561]
y:   [-0.006978171486712179]
tx:  [-0.004307981625969614]
ty:  [-0.01533249323076702]
q/p: [0.0022887185972486884]]
Covariance matrix:
[[ 4.9e-06 -5.3e-11 -1.2e-06  8.9e-11 -3.0e-07]
 [-5.3e-11  7.9e-06  1.2e-10 -1.6e-06 -1.6e-11]
 [-1.2e-06  1.2e-10  1.6e-06 -2.0e-10  7.0e-07]
 [ 8.9e-11 -1.6e-06 -2.0e-10  2.6e-06  2.7e-11]
 [-3.0e-07 -1.6e-11  7.0e-07  2.7e-11  4.0e-07]]
Data added to list for particle: 17
Particle 18:
eta 3.1460154529697375, phi 1.1617704178344197,
p 93.0, q 1.60217662e-19,
brem = False
input State:
[[0.        ]
 [0.        ]
 [0.07909475]
 [0.03428548]
 [0.00322375]]

Simulating particle
Particle track 18 is saved
Initiating Kalman filtering for particle: 18
Particle 18:
eta 3.3288955066803245, phi 1.0773294839533891,
p 51.50283447787175, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.013806362212278645]
y:   [0.0035358030033758557]
tx:  [0.06319650204968401]
ty:  [0.03399021235134457]
q/p: [-0.005821203867532962]]
Covariance matrix:
[[ 3.4e-06 -4.0e-11  1.7e-07  1.2e-10  4.0e-07]
 [-4.0e-11  3.8e-06  1.8e-10 -7.1e-07  2.8e-11]
 [ 1.7e-07  1.8e-10 -1.5e-06 -5.5e-10 -1.0e-06]
 [ 1.2e-10 -7.1e-07 -5.5e-10  2.2e-06 -8.6e-11]
 [ 4.0e-07  2.8e-11 -1.0e-06 -8.6e-11 -6.1e-07]]
Data added to list for particle: 18
Particle 19:
eta 2.348822533702834, phi -0.5146292055053167,
p 93.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.09485913]
 [ 0.16775802]
 [-0.00322375]]

Simulating particle
Particle track 19 is saved
Initiating Kalman filtering for particle: 19
Particle 19:
eta 2.3775094495068707, phi -0.4748125820480271,
p 173.08215338175808, q 1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.0077436372596821675]
y:   [0.007903187709025995]
tx:  [-0.08557066819971597]
ty:  [0.16646851505998167]
q/p: [0.0017321745390480865]]
Covariance matrix:
[[ 4.8e-06  2.2e-10 -1.0e-06 -5.9e-10 -2.3e-07]
 [ 2.2e-10  5.0e-06 -5.3e-10 -7.9e-07  8.9e-11]
 [-1.0e-06 -5.3e-10  1.3e-06  1.4e-09  5.8e-07]
 [-5.9e-10 -7.9e-07  1.4e-09  2.2e-06 -2.4e-10]
 [-2.3e-07  8.9e-11  5.8e-07 -2.4e-10  3.4e-07]]
Data added to list for particle: 19
data saved to data/toy_output/48
1 particles skipped because: Taylor steep slope