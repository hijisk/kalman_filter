This is the start of the log file generated at 08_09_23h33m20s

15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 20 particles...
Particle 0:
eta 3.9699052844205314, phi -0.4698042129255075,
p 3.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.01709616]
 [-0.03367244]
 [ 0.09993617]]

Simulating particle
Particle track 0 is saved
Initiating Kalman filtering for particle: 0
Exception occured at particle 0
Data added to list for particle: 0
data saved to data/toy_output/47