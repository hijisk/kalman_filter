This is the start of the log file generated at 12_01_18h32m37s

magfield_ shape: cs, sim_stepsize: 0.001, detector_length: 11
15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 2 particles, from 30 to 100 GeV. Brem = False, ms = True

Particle 0:
eta 3.506446076952921, phi -1.2633887934780583,
p 30.000000000000004, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [-0.001     ]
 [-0.0572452 ]
 [ 0.01817371]
 [ 0.00999362]]

Simulating particle
Initiating Kalman filtering for particle: 0
Particle 0:
eta 4.19784665617989, phi -1.2621616797616235,
p 109.69643689960179, q 1.60217662e-19,
brem = False
output State:
[[ 0.00100419]
 [-0.00101192]
 [-0.02864211]
 [ 0.00913176]
 [ 0.00273307]]
Data added to pickle list for particle: 0
Particle 1:
eta 3.592468922415832, phi -0.4226605575079663,
p 30.000000000000004, q -1.60217662e-19,
brem = False
input State:
[[-0.001     ]
 [-0.001     ]
 [-0.02260233]
 [ 0.05025336]
 [-0.00999362]]

Simulating particle
Initiating Kalman filtering for particle: 1
Particle 1:
eta 4.275711120969126, phi -0.4178390026595832,
p 105.59586073009731, q -1.60217662e-19,
brem = False
output State:
[[-0.00102533]
 [-0.00117495]
 [-0.01128479]
 [ 0.02541717]
 [-0.00283921]]
Data added to pickle list for particle: 1