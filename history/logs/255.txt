This is the start of the log file generated at 09_16_16h19m14s

15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Particle 0:
eta 0.6584789484624085, phi -0.7853981633974483,
p 5.9961699850299395, q -1.60217662e-19,
brem = False
input State:
[[ 0.1 ]
 [ 0.1 ]
 [-1.  ]
 [ 1.  ]
 [-0.05]]

Simulating particle
Particle track 0 is saved
Initiating Kalman filtering for particle: 0
Particle 0:
eta 0.35385253514006354, phi 1.201285389502887,
p 5.694100130198743, q -1.60217662e-19,
brem = False
output State:
[[-4.91747525]
 [ 0.10046187]
 [ 2.58108818]
 [ 0.99965708]
 [-0.05265248]]
Data added to list for particle: 0
data saved to data/toy_output/255