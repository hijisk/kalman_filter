This is the start of the log file generated at 11_23_22h21m08s

magfield_ shape: cs, sim_stepsize: 0.01, detector_length: 11
15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 140 particles, from 30 to 100 GeV. Brem = False, ms = False

Particle 0:
eta 3.7644665442330782, phi -0.13885499650390112,
p 30.000000000000004, q 1.60217662e-19,
brem = False
input State:
[[-0.001     ]
 [-0.001     ]
 [-0.00642009]
 [ 0.04593842]
 [ 0.00999362]]

Simulating particle
Initiating Kalman filtering for particle: 0
Particle 0:
eta 3.7880975533976584, phi -0.14059245377282018,
p 30.263711066531553, q 1.60217662e-19,
brem = False
output State:
[[-0.00098967]
 [-0.00096177]
 [-0.00634794]
 [ 0.0448535 ]
 [ 0.00990653]]
Data added to pickle list for particle: 0
Particle 1:
eta 3.782630333802046, phi -3.0633899020283666,
p 30.000000000000004, q -1.60217662e-19,
brem = False
input State:
[[-0.001     ]
 [ 0.001     ]
 [-0.00355843]
 [-0.04540987]
 [-0.00999362]]

Simulating particle
Initiating Kalman filtering for particle: 1
Particle 1:
eta 3.8064850662213208, phi -3.0640897821074455,
p 30.67184034958478, q -1.60217662e-19,
brem = False
output State:
[[-0.00102703]
 [ 0.00094076]
 [-0.00344344]
 [-0.0443408 ]
 [-0.00977472]]
Data added to pickle list for particle: 1
Particle 2:
eta 4.248694020855483, phi -2.189781664604893,
p 31.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.02327062]
 [-0.01657749]
 [ 0.00967124]]

Simulating particle
Initiating Kalman filtering for particle: 2
Particle 2:
eta 4.261814287966576, phi -2.1878407396732933,
p 31.718418511604856, q 1.60217662e-19,
brem = False
output State:
[[ 0.00085868]
 [ 0.00091041]
 [-0.02299889]
 [-0.01631671]
 [ 0.00945219]]
Data added to pickle list for particle: 2
Particle 3:
eta 3.9794808798883428, phi -2.409907490544147,
p 31.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.02499036]
 [-0.02783025]
 [-0.00967124]]

Simulating particle
Initiating Kalman filtering for particle: 3
Particle 3:
eta 3.9979085170939253, phi -2.412392034729623,
p 32.08413325536196, q -1.60217662e-19,
brem = False
output State:
[[ 0.00087656]
 [ 0.00113014]
 [-0.0244658 ]
 [-0.02738262]
 [-0.00934445]]
Exception occured at particle 3
Data added to pickle list for particle: 3