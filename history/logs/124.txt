This is the start of the log file generated at 09_02_14h10m49s

15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 400 particles...
Particle 0:
eta 4.7511299222386, phi -0.7046753109839151,
p 40.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.01119708]
 [ 0.0131682 ]
 [ 0.00749521]]

Simulating particle
Exception occured at particle 0
Data added to list for particle: 0
data saved to data/toy_output/124