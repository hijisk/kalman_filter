This is the start of the log file generated at 08_14_16h07m42s

20 test-type subdetectors added at z-locations: [500, 1000, 1500, 2000, 2500, 3000, 3500, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 750, 1750, 3750, 6750, 7750, 8750]
Generating 10 particles...
Particle 0:
eta 3.017891374851228, phi -0.0019123806454881796,
p 50.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.0001875 ]
 [ 0.09804276]
 [ 0.00599617]]

Simulating particle
Particle track 0 is saved
Initiating Kalman filtering for particle: 0
Exception occured at particle 0
Data added to list for particle: 0
data saved to data/toy_output/83