This is the start of the log file generated at 08_09_23h32m41s

15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 20 particles...
Particle 0:
eta 2.2521057504662125, phi 0.0215827186795,
p 3.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.00459046]
 [-0.21265853]
 [ 0.09993617]]

Simulating particle
Particle track 0 is saved
Initiating Kalman filtering for particle: 0
Too small a gyroradius in calculating jacobian at z = 4.149999999999968
at vector index 0 and state vector
[[-1.15671407]
 [-0.88172831]
 [-8.52739443]
 [-0.21179542]
 [ 1.00403148]]
Skip particle 0 because:The particle gets a very steep slope
                in the taylor expansion at z=4.149999999999968, with state =[[-1.15671407]
 [-0.88172831]
 [-8.52739443]
 [-0.21179542]
 [ 1.00403148]]
Data added to list for particle: 0
Particle 1:
eta 2.4155629326750905, phi 1.1388838117009072,
p 3.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.16353415]
 [-0.07537908]
 [-0.09993617]]

Simulating particle
Particle track 1 is saved
Initiating Kalman filtering for particle: 1
Too small a gyroradius in calculating jacobian at z = 3.8299999999999748
at vector index 0 and state vector
[[-1.54824536]
 [-0.2921404 ]
 [-8.29141411]
 [-0.07638529]
 [ 1.23019776]]
Skip particle 1 because:The particle gets a very steep slope
                in the taylor expansion at z=3.8299999999999748, with state =[[-1.54824536]
 [-0.2921404 ]
 [-8.29141411]
 [-0.07638529]
 [ 1.23019776]]
Data added to list for particle: 1
Particle 2:
eta 2.3964357563262064, phi -1.4280986821830441,
p 12.999999999999998, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.1817394 ]
 [ 0.02611126]
 [ 0.02306219]]

Simulating particle
Particle track 2 is saved
Initiating Kalman filtering for particle: 2
Too small a gyroradius in calculating jacobian at z = 3.8999999999999733
at vector index 0 and state vector
[[-1.68845844]
 [ 0.10007203]
 [-7.89127167]
 [ 0.02544936]
 [ 1.09214047]]
Skip particle 2 because:The particle gets a very steep slope
                in the taylor expansion at z=3.8999999999999733, with state =[[-1.68845844]
 [ 0.10007203]
 [-7.89127167]
 [ 0.02544936]
 [ 1.09214047]]
Data added to list for particle: 2
Particle 3:
eta 4.991045102008536, phi -0.49950087335246246,
p 12.999999999999998, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.00651315]
 [-0.01193639]
 [-0.02306219]]

Simulating particle
Data added to list for particle: 3