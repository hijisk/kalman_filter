This is the start of the log file generated at 02_11_13h05m24s

magfield_ shape: cs, sim_stepsize: 0.001, detector_length: 11
15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 3960 particles, from 1 to 100 GeV. Brem = False, ms = True

Particle 0:
eta 4.562457113829659, phi -1.7300794239647304,
p 1.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.02061079]
 [-0.003311  ]
 [ 0.2998085 ]]

Simulating particle
Initiating Kalman filtering for particle: 0
Skip particle 0 because:('overflow encountered in double_scalars',)
Data added to pickle list for particle: 0
Data added to condensed list for particle: 0
Particle 1:
eta 4.604172606258553, phi 0.6760244028079047,
p 1.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.01252768]
 [ 0.01561846]
 [-0.2998085 ]]

Simulating particle
Initiating Kalman filtering for particle: 1
Skip particle 1 because:('overflow encountered in double_scalars',)
Data added to pickle list for particle: 1
Data added to condensed list for particle: 1
Particle 2:
eta 4.058451567756896, phi 0.5369300639000884,
p 1.05, q 1.60217662e-19,
brem = False
input State:
[[0.001     ]
 [0.001     ]
 [0.01767838]
 [0.02969838]
 [0.2855319 ]]

Simulating particle
Initiating Kalman filtering for particle: 2
Skip particle 2 because:('overflow encountered in double_scalars',)
Data added to pickle list for particle: 2
Data added to condensed list for particle: 2
Particle 3:
eta 2.966806218972995, phi 1.0755899212250108,
p 1.05, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.0908099 ]
 [ 0.04904592]
 [-0.2855319 ]]

Simulating particle
Initiating Kalman filtering for particle: 3
Skip particle 3 because:('overflow encountered in double_scalars',)
Data added to pickle list for particle: 3
Data added to condensed list for particle: 3
Particle 4:
eta 3.62269469099626, phi -0.1034519619788963,
p 1.1, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.00552061]
 [ 0.05317353]
 [ 0.27255318]]

Simulating particle
Data added to pickle list for particle: 4
Data added to condensed list for particle: 4
4 particles skipped because: FloatingPointError