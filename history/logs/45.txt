This is the start of the log file generated at 08_09_18h03m40s

15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 20 particles...
Particle 0:
eta 4.642003953118464, phi -0.5539517368421775,
p 3.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.0101415 ]
 [-0.01639546]
 [ 0.09993617]]

Simulating particle
Particle track 0 is saved
Initiating Kalman filtering for particle: 0
Too small a gyroradius in calculating jacobian at z = 1.7200000000000009
at vector index 0 and state vector
[[ 9.14532139e-01]
 [-2.67156586e-02]
 [ 1.81485216e+01]
 [-1.48530208e-02]
 [-1.87888446e+01]]
Skip particle 0 because:The particle gets a very steep slope
                in the taylor expansion at z=1.7200000000000009, with state =[[ 9.14532139e-01]
 [-2.67156586e-02]
 [ 1.81485216e+01]
 [-1.48530208e-02]
 [-1.87888446e+01]]
Data added to list for particle: 0
Particle 1:
eta 2.921324764812343, phi 0.4416943487601041,
p 3.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.0461832 ]
 [-0.09766945]
 [-0.09993617]]

Simulating particle
Particle track 1 is saved
Initiating Kalman filtering for particle: 1
Too small a gyroradius in calculating jacobian at z = 1.5400000000000011
at vector index 0 and state vector
[[ -0.94847674]
 [ -0.14757852]
 [-10.95772767]
 [ -0.09546125]
 [ 18.76144128]]
Skip particle 1 because:The particle gets a very steep slope
                in the taylor expansion at z=1.5400000000000011, with state =[[ -0.94847674]
 [ -0.14757852]
 [-10.95772767]
 [ -0.09546125]
 [ 18.76144128]]
Data added to list for particle: 1
Particle 2:
eta 4.652283754557389, phi 0.2523876988133547,
p 12.999999999999998, q 1.60217662e-19,
brem = False
input State:
[[0.        ]
 [0.        ]
 [0.00476492]
 [0.0184768 ]
 [0.02306219]]

Simulating particle
Particle track 2 is saved
Initiating Kalman filtering for particle: 2
Too small a gyroradius in calculating jacobian at z = 0.8200000000000001
at vector index 0 and state vector
[[ 1.37417869e-01]
 [ 1.48171229e-02]
 [ 4.30591192e+00]
 [ 1.43324503e-02]
 [-8.27124868e+01]]
Skip particle 2 because:The particle gets a very steep slope
                in the taylor expansion at z=0.8200000000000001, with state =[[ 1.37417869e-01]
 [ 1.48171229e-02]
 [ 4.30591192e+00]
 [ 1.43324503e-02]
 [-8.27124868e+01]]
Data added to list for particle: 2
Particle 3:
eta 4.985637895769113, phi -0.5673262170433514,
p 12.999999999999998, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.00734676]
 [-0.01152971]
 [-0.02306219]]

Simulating particle
Particle track 3 is saved
Initiating Kalman filtering for particle: 3
Too small a gyroradius in calculating jacobian at z = 0.7
at vector index 0 and state vector
[[-1.60870046e-01]
 [-8.04188954e-03]
 [-8.34549702e+00]
 [-1.03125185e-02]
 [ 8.24498101e+01]]
Skip particle 3 because:The particle gets a very steep slope
                in the taylor expansion at z=0.7, with state =[[-1.60870046e-01]
 [-8.04188954e-03]
 [-8.34549702e+00]
 [-1.03125185e-02]
 [ 8.24498101e+01]]
Data added to list for particle: 3
Particle 4:
eta 2.7315230642137007, phi 0.19859864568487895,
p 23.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.02580524]
 [-0.12822382]
 [ 0.01303515]]

Simulating particle
Particle track 4 is saved
Initiating Kalman filtering for particle: 4
Too small a gyroradius in calculating jacobian at z = 0.6500000000000004
at vector index 0 and state vector
[[ 2.32664092e-01]
 [-8.32869353e-02]
 [ 7.36821786e+00]
 [-1.28063507e-01]
 [-1.45282175e+02]]
Skip particle 4 because:The particle gets a very steep slope
                in the taylor expansion at z=0.6500000000000004, with state =[[ 2.32664092e-01]
 [-8.32869353e-02]
 [ 7.36821786e+00]
 [-1.28063507e-01]
 [-1.45282175e+02]]
Data added to list for particle: 4
Particle 5:
eta 4.66455273759792, phi 0.9098677641724858,
p 23.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.01487951]
 [ 0.01157021]
 [-0.01303515]]

Simulating particle
Particle track 5 is saved
Initiating Kalman filtering for particle: 5
Too small a gyroradius in calculating jacobian at z = 0.6
at vector index 0 and state vector
[[-1.99881920e-01]
 [ 6.91795952e-03]
 [-4.93022627e+00]
 [ 1.19602854e-02]
 [ 1.44768036e+02]]
Skip particle 5 because:The particle gets a very steep slope
                in the taylor expansion at z=0.6, with state =[[-1.99881920e-01]
 [ 6.91795952e-03]
 [-4.93022627e+00]
 [ 1.19602854e-02]
 [ 1.44768036e+02]]
Data added to list for particle: 5
Particle 6:
eta 4.533428639044394, phi 0.47544674028879425,
p 33.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.00983676]
 [-0.01910654]
 [ 0.00908511]]

Simulating particle
Particle track 6 is saved
Initiating Kalman filtering for particle: 6
Too small a gyroradius in calculating jacobian at z = 0.54
at vector index 0 and state vector
[[ 1.57642747e-01]
 [-1.02881684e-02]
 [ 1.22357074e+01]
 [-1.92769159e-02]
 [-2.08482346e+02]]
Skip particle 6 because:The particle gets a very steep slope
                in the taylor expansion at z=0.54, with state =[[ 1.57642747e-01]
 [-1.02881684e-02]
 [ 1.22357074e+01]
 [-1.92769159e-02]
 [-2.08482346e+02]]
Data added to list for particle: 6
Particle 7:
eta 3.0755414812116872, phi -0.6698514468708685,
p 33.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.05744685]
 [ 0.07253276]
 [-0.00908511]]

Simulating particle
Particle track 7 is saved
Initiating Kalman filtering for particle: 7
Too small a gyroradius in calculating jacobian at z = 0.56
at vector index 0 and state vector
[[-1.54935437e-01]
 [ 4.05961173e-02]
 [-3.17093696e+00]
 [ 6.93060069e-02]
 [ 2.07975734e+02]]
Skip particle 7 because:The particle gets a very steep slope
                in the taylor expansion at z=0.56, with state =[[-1.54935437e-01]
 [ 4.05961173e-02]
 [-3.17093696e+00]
 [ 6.93060069e-02]
 [ 2.07975734e+02]]
Data added to list for particle: 7
Particle 8:
eta 4.389416413853456, phi -1.204404431231003,
p 43.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.02317237]
 [-0.00889166]
 [ 0.00697229]]

Simulating particle
Particle track 8 is saved
Initiating Kalman filtering for particle: 8
Too small a gyroradius in calculating jacobian at z = 0.46
at vector index 0 and state vector
[[ 4.25885649e-02]
 [-4.59892422e-03]
 [ 4.41010359e+00]
 [-8.60508781e-03]
 [-2.73718495e+02]]
Skip particle 8 because:The particle gets a very steep slope
                in the taylor expansion at z=0.46, with state =[[ 4.25885649e-02]
 [-4.59892422e-03]
 [ 4.41010359e+00]
 [-8.60508781e-03]
 [-2.73718495e+02]]
Data added to list for particle: 8
Particle 9:
eta 4.2671437749149925, phi 0.6536342913132667,
p 43.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.01705595]
 [-0.02226757]
 [-0.00697229]]

Simulating particle
Particle track 9 is saved
Initiating Kalman filtering for particle: 9
Too small a gyroradius in calculating jacobian at z = 0.46
at vector index 0 and state vector
[[-3.42019137e-02]
 [-1.04357380e-02]
 [-3.29124280e+00]
 [-2.17530607e-02]
 [ 2.72654617e+02]]
Skip particle 9 because:The particle gets a very steep slope
                in the taylor expansion at z=0.46, with state =[[-3.42019137e-02]
 [-1.04357380e-02]
 [-3.29124280e+00]
 [-2.17530607e-02]
 [ 2.72654617e+02]]
Data added to list for particle: 9
Particle 10:
eta 2.9839541912732903, phi 0.413567025186112,
p 53.0, q 1.60217662e-19,
brem = False
input State:
[[0.        ]
 [0.        ]
 [0.0407683 ]
 [0.09289197]
 [0.00565676]]

Simulating particle
Particle track 10 is saved
Initiating Kalman filtering for particle: 10
Too small a gyroradius in calculating jacobian at z = 0.48000000000000015
at vector index 0 and state vector
[[ 1.83679881e-01]
 [ 4.42510714e-02]
 [ 1.09757523e+01]
 [ 9.12482087e-02]
 [-3.38972711e+02]]
Skip particle 10 because:The particle gets a very steep slope
                in the taylor expansion at z=0.48000000000000015, with state =[[ 1.83679881e-01]
 [ 4.42510714e-02]
 [ 1.09757523e+01]
 [ 9.12482087e-02]
 [-3.38972711e+02]]
Data added to list for particle: 10
Particle 11:
eta 3.499094939887442, phi 0.035719141954647315,
p 53.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.00216072]
 [ 0.06046613]
 [-0.00565676]]

Simulating particle
Particle track 11 is saved
Initiating Kalman filtering for particle: 11
Too small a gyroradius in calculating jacobian at z = 0.46
at vector index 0 and state vector
[[-2.91606808e-02]
 [ 2.78194616e-02]
 [-4.30217278e+00]
 [ 5.61988500e-02]
 [ 3.39871451e+02]]
Skip particle 11 because:The particle gets a very steep slope
                in the taylor expansion at z=0.46, with state =[[-2.91606808e-02]
 [ 2.78194616e-02]
 [-4.30217278e+00]
 [ 5.61988500e-02]
 [ 3.39871451e+02]]
Data added to list for particle: 11
Particle 12:
eta 3.9822621738980812, phi 0.8389581868881137,
p 62.99999999999999, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.02774909]
 [-0.02492515]
 [ 0.00475887]]

Simulating particle
Particle track 12 is saved
Initiating Kalman filtering for particle: 12
Too small a gyroradius in calculating jacobian at z = 0.4400000000000001
at vector index 0 and state vector
[[ 1.27945466e-01]
 [-1.10479579e-02]
 [ 3.53432594e+00]
 [-2.52971652e-02]
 [-4.05869573e+02]]
Skip particle 12 because:The particle gets a very steep slope
                in the taylor expansion at z=0.4400000000000001, with state =[[ 1.27945466e-01]
 [-1.10479579e-02]
 [ 3.53432594e+00]
 [-2.52971652e-02]
 [-4.05869573e+02]]
Data added to list for particle: 12
Particle 13:
eta 4.33046364607003, phi 1.1040241206505423,
p 62.99999999999999, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.02351108]
 [ 0.01184751]
 [-0.00475887]]

Simulating particle
Particle track 13 is saved
Initiating Kalman filtering for particle: 13
Too small a gyroradius in calculating jacobian at z = 0.44
at vector index 0 and state vector
[[-1.61256950e-01]
 [ 5.16251064e-03]
 [-5.11673525e+00]
 [ 1.18379824e-02]
 [ 3.96478639e+02]]
Exception occured at particle 13
Data added to list for particle: 13