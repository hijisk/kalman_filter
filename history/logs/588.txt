This is the start of the log file generated at 11_23_22h22m15s

magfield_ shape: cs, sim_stepsize: 0.01, detector_length: 11
15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 140 particles, from 30 to 100 GeV. Brem = False, ms = False

Particle 0:
eta 3.7754620957933835, phi -1.8872066353653445,
p 30.000000000000004, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [-0.001     ]
 [-0.04359969]
 [-0.01427499]
 [ 0.00999362]]

Simulating particle
Initiating Kalman filtering for particle: 0
Particle 0:
eta 3.7943798071695687, phi -1.8848247950993777,
p 30.682227168711147, q 1.60217662e-19,
brem = False
output State:
[[ 0.00084145]
 [-0.00102342]
 [-0.04281505]
 [-0.01390526]
 [ 0.00977141]]
Data added to pickle list for particle: 0
Particle 1:
eta 2.5449541268292397, phi -0.5214387035023895,
p 30.000000000000004, q -1.60217662e-19,
brem = False
input State:
[[-0.001     ]
 [-0.001     ]
 [-0.07866734]
 [ 0.13693809]
 [-0.00999362]]

Simulating particle
Initiating Kalman filtering for particle: 1
Exception occured at particle 1
Data added to pickle list for particle: 1