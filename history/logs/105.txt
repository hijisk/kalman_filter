This is the start of the log file generated at 08_18_14h53m10s

20 test-type subdetectors added at z-locations: [500, 1000, 1500, 2000, 2500, 3000, 3500, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 750, 1750, 3750, 6750, 7750, 8750]
Generating 5 particles...
Particle 0:
eta 4.648751714257166, phi -0.05136172696277522,
p 50.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.00098309]
 [ 0.01912359]
 [ 0.00599617]]

Simulating particle
Particle track 0 is saved
Initiating Kalman filtering for particle: 0
Particle 0:
eta 4.650176020597821, phi -0.053470006960381164,
p 50.46340986003876, q 1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[9.056776676030705e-05]
y:   [6.924842574385734e-05]
tx:  [-0.001021944191312334]
ty:  [0.01909425744763209]
q/p: [0.0059411066371262205]]
Covariance matrix:
[[ 2.0e-03  5.0e-13 -1.9e-03 -6.6e-13 -5.7e-05]
 [ 5.0e-13  4.1e-03 -9.9e-13 -3.0e-03  7.4e-10]
 [-1.9e-03 -9.9e-13  3.7e-03  1.3e-12  1.1e-04]
 [-6.6e-13 -3.0e-03  1.3e-12  4.4e-03 -9.7e-10]
 [-5.7e-05  7.4e-10  1.1e-04 -9.7e-10  1.1e-02]]
Exception occured at particle 0
Data added to list for particle: 0
data saved to data/toy_output/105