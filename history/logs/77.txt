This is the start of the log file generated at 08_14_15h46m36s

20 test-type subdetectors added at z-locations: [500, 1000, 1500, 2000, 2500, 3000, 3500, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 750, 1750, 3750, 6750, 7750, 8750]
Generating 10 particles...
Particle 0:
eta 3.7213676028661133, phi 0.21664252158173467,
p 50.0, q 1.60217662e-19,
brem = False
input State:
[[0.        ]
 [0.        ]
 [0.01041013]
 [0.04729799]
 [0.00599617]]

Simulating particle
ZeroDivisonError traceback:
<traceback object at 0x7fdc72e24f00>

The weird error with division by zero popped up at z=0.0, state=[[0.        ]
 [0.        ]
 [0.01041013]
 [0.04729799]
 [0.00599617]]
Exception occured at particle 0
Data added to list for particle: 0
data saved to data/toy_output/77