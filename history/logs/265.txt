This is the start of the log file generated at 09_17_10h02m41s

15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 1 particles...
Particle 0:
eta 2.4965966142475673, phi 1.0491185992336298,
p 50.0, q 1.60217662e-19,
brem = False
input State:
[[0.001     ]
 [0.001     ]
 [0.14379354]
 [0.08265134]
 [0.00599617]]

Simulating particle
Particle track 0 is saved
Initiating Kalman filtering for particle: 0
Particle 0:
eta 2.4912876892649383, phi -1.0538966528410743,
p 47.467912338725476, q 1.60217662e-19,
brem = False
output State:
[[-0.00053419]
 [ 0.00110697]
 [-0.1449649 ]
 [ 0.08240565]
 [ 0.00631602]]
Data added to list for particle: 0
data saved to data/toy_output/265