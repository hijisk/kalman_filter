This is the start of the log file generated at 01_29_15h20m31s

magfield_ shape: cs, sim_stepsize: 0.01, detector_length: 11
15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Particle 0:
eta 0.6584789484624083, phi -0.7853981633974483,
p 59.961699850299404, q -1.60217662e-19,
brem = False
input State:
[[ 0.1  ]
 [ 0.1  ]
 [-1.   ]
 [ 1.   ]
 [-0.005]]

Simulating particle
Initiating Kalman filtering for particle: 0
Skip particle 0 because:('overflow encountered in double_scalars',)
Data added to pickle list for particle: 0
Particle 0:
eta 0.6584789484624083, phi -0.7853981633974483,
p 59.961699850299404, q -1.60217662e-19,
brem = False
input State:
[[ 0.1  ]
 [ 0.1  ]
 [-1.   ]
 [ 1.   ]
 [-0.005]]

Simulating particle
Initiating Kalman filtering for particle: 0
Particle 0:
eta 0.658521420032341, phi -0.7852070079623872,
p 60.258727988380706, q -1.60217662e-19,
brem = False
output State:
[[ 0.09997352]
 [ 0.09998797]
 [-0.99973528]
 [ 1.00011756]
 [-0.00497535]]
Data added to pickle list for particle: 0
1 particles skipped because: FloatingPointError