This is the start of the log file generated at 08_24_09h26m33s

15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 5 particles...
Particle 0:
eta 4.72027435681669, phi -1.5466640170520272,
p 50.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.01782169]
 [ 0.00043016]
 [ 0.00599617]]

Simulating particle
Particle track 0 is saved
Initiating Kalman filtering for particle: 0
Particle 0:
eta 4.723259383956183, phi -1.5475264962629773,
p 50.03644901015004, q 1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.04411869226294788]
y:   [2.4120973877015733e-05]
tx:  [-0.01776892732096284]
ty:  [0.0004135545747222833]
q/p: [0.005991802079933371]]
Covariance matrix:
[[ 2.7e-05  1.6e-05 -3.7e-06 -6.2e-06 -1.1e-06]
 [ 1.6e-05  4.8e-05  5.3e-10 -6.0e-06  1.6e-10]
 [-3.7e-06  5.3e-10 -3.5e-06 -2.1e-10 -2.1e-06]
 [-6.2e-06 -6.0e-06 -2.1e-10  2.4e-06 -6.4e-11]
 [-1.1e-06  1.6e-10 -2.1e-06 -6.4e-11 -1.1e-06]]
Data added to list for particle: 0
Particle 1:
eta 3.991451676143159, phi 1.4585315609932479,
p 50.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.03672571]
 [ 0.00414041]
 [-0.00599617]]

Simulating particle
Particle track 1 is saved
Initiating Kalman filtering for particle: 1
Particle 1:
eta 3.9961272872499043, phi 1.459985872195151,
p 50.526681395441905, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.0021403747559177724]
y:   [-5.2759427905470945e-05]
tx:  [0.03656023653536897]
ty:  [0.004067920014415124]
q/p: [-0.005933666945293249]]
Covariance matrix:
[[ 2.5e-05  1.6e-06 -6.2e-06 -9.7e-07 -2.0e-06]
 [ 1.6e-06  2.7e-05  2.5e-10 -4.3e-06  7.0e-12]
 [-6.2e-06  2.5e-10  2.8e-06 -1.6e-10  1.2e-06]
 [-9.7e-07 -4.3e-06 -1.6e-10  2.7e-06 -4.4e-12]
 [-2.0e-06  7.0e-12  1.2e-06 -4.4e-12  6.2e-07]]
Data added to list for particle: 1
Particle 2:
eta 3.728225863032625, phi 2.9489333785845457,
p 50.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [ 0.00920943]
 [-0.04720877]
 [ 0.00599617]]

Simulating particle
Particle track 2 is saved
Initiating Kalman filtering for particle: 2
Particle 2:
eta 3.7278340128995904, phi 2.9502054014765426,
p 49.890967051555656, q 1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[0.0007300455406101008]
y:   [1.2051840253964074e-05]
tx:  [0.009152966603130547]
ty:  [-0.047238976123041336]
q/p: [0.006009274162629178]]
Covariance matrix:
[[ 1.6e-05  1.6e-08  3.3e-06 -1.2e-08  3.4e-06]
 [ 1.6e-08  2.2e-05  2.8e-09 -3.7e-06  5.0e-10]
 [ 3.3e-06  2.8e-09 -3.7e-06 -2.1e-09 -2.2e-06]
 [-1.2e-08 -3.7e-06 -2.1e-09  2.7e-06 -3.7e-10]
 [ 3.4e-06  5.0e-10 -2.2e-06 -3.7e-10 -1.2e-06]]
Data added to list for particle: 2
Particle 3:
eta 4.585948817240958, phi -0.7908017426672566,
p 50.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.01449579]
 [ 0.01433998]
 [-0.00599617]]

Simulating particle
Particle track 3 is saved
Initiating Kalman filtering for particle: 3
Particle 3:
eta 4.582452552487538, phi -0.7824248023723193,
p 50.16918014226386, q -1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.004132190410131856]
y:   [-0.00021706852394781553]
tx:  [-0.014425518425130202]
ty:  [0.0145115590570552]
q/p: [-0.005975949744471313]]
Covariance matrix:
[[ 2.8e-05  2.0e-06 -6.8e-06 -8.2e-07 -2.0e-06]
 [ 2.0e-06  5.4e-05  1.2e-09 -8.3e-06 -1.0e-10]
 [-6.8e-06  1.2e-09  3.1e-06 -5.0e-10  1.3e-06]
 [-8.2e-07 -8.3e-06 -5.0e-10  3.3e-06  4.0e-11]
 [-2.0e-06 -1.0e-10  1.3e-06  4.0e-11  6.5e-07]]
Data added to list for particle: 3
Particle 4:
eta 2.513370902782316, phi -2.901267522388781,
p 50.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.03881109]
 [-0.15837298]
 [ 0.00599617]]

Simulating particle
Particle track 4 is saved
Initiating Kalman filtering for particle: 4
Particle 4:
eta 2.514026086371538, phi -2.9045415343136067,
p 49.27275194455428, q 1.60217662e-19,
brem = False
output State:
State Vector:
x:  [[-0.32198861365866605]
y:   [6.865990009515272e-05]
tx:  [-0.0382669605842074]
ty:  [-0.1583940190483382]
q/p: [0.006084671292337517]]
Covariance matrix:
[[ 3.2e-05 -7.5e-06 -3.6e-06  5.5e-06 -4.8e-07]
 [-7.5e-06  2.5e-05  9.3e-09 -3.5e-06  1.7e-09]
 [-3.6e-06  9.3e-09 -3.9e-06 -6.9e-09 -2.3e-06]
 [ 5.5e-06 -3.5e-06 -6.9e-09  2.6e-06 -1.3e-09]
 [-4.8e-07  1.7e-09 -2.3e-06 -1.3e-09 -1.2e-06]]
Data added to list for particle: 4
data saved to data/toy_output/113