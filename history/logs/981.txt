This is the start of the log file generated at 03_05_11h27m27s

magfield_ shape: cs, sim_stepsize: 0.01, detector_length: 9.5
15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 T_station-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 39400 particles, from 1.5 to 100 GeV. Brem = False, ms = True

Particle 0:
eta 3.8289594152324935, phi 0.39023428294571455,
p 1.5, q 1.60217662e-19,
brem = False
input State:
[[0.001     ]
 [0.001     ]
 [0.01654191]
 [0.04021577]
 [0.19987233]]

Simulating particle
Initiating Kalman filtering for particle: 0
Particle 0:
eta 3.8324099455984673, phi 0.39720352016088717,
p 1.4972200198960401, q 1.60217662e-19,
brem = False
output State:
[[0.00133842]
 [0.00181002]
 [0.01676378]
 [0.03996126]
 [0.20024345]]
Data added to pickle list for particle: 0
Data added to condensed list for particle: 0
Particle 1:
eta 4.552250205014502, phi -0.13596623945598096,
p 1.5, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.0028586 ]
 [ 0.02089461]
 [-0.19987233]]

Simulating particle
Initiating Kalman filtering for particle: 1
Particle 1:
eta 4.485503827207217, phi -0.1452596309024657,
p 1.4797083296237454, q -1.60217662e-19,
brem = False
output State:
[[ 0.00095643]
 [ 0.00135299]
 [-0.00326341]
 [ 0.02230784]
 [-0.20261324]]
Data added to pickle list for particle: 1
Data added to condensed list for particle: 1
Particle 2:
eta 2.245380334715311, phi 0.45306632449716105,
p 1.5, q 1.60217662e-19,
brem = False
input State:
[[0.001     ]
 [0.001     ]
 [0.09375004]
 [0.19256749]
 [0.19987233]]

Simulating particle
Initiating Kalman filtering for particle: 2
Particle 2:
eta 2.247886089049003, phi 0.45138792163492836,
p 1.495944661317366, q 1.60217662e-19,
brem = False
output State:
[[0.00287645]
 [0.00485819]
 [0.09318761]
 [0.19223136]
 [0.20041416]]
Data added to pickle list for particle: 2
Data added to condensed list for particle: 2
Particle 3:
eta 3.1769257541525784, phi 1.048394522150756,
p 1.5, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.07242614]
 [ 0.04169974]
 [-0.19987233]]

Simulating particle
Initiating Kalman filtering for particle: 3
Skip particle 3 because:magnet loss in prediction
Data added to pickle list for particle: 3
Data added to condensed list for particle: 3
Particle 4:
eta 2.0725386963243846, phi -1.7290273848374376,
p 1.5, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.2525885 ]
 [-0.04030428]
 [ 0.19987233]]

Simulating particle
Initiating Kalman filtering for particle: 4
Skip particle 4 because:magnet loss in prediction
Data added to pickle list for particle: 4
Data added to condensed list for particle: 4
Particle 5:
eta 3.480942104624284, phi 0.6188080097040045,
p 1.5, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.03574078]
 [ 0.0501899 ]
 [-0.19987233]]

Simulating particle
Initiating Kalman filtering for particle: 5
Particle 5:
eta 3.4744924484655804, phi 0.6128053016732622,
p 1.486441351653259, q -1.60217662e-19,
brem = False
output State:
[[ 0.00172111]
 [ 0.00195885]
 [ 0.03566861]
 [ 0.05073029]
 [-0.20169548]]
Data added to pickle list for particle: 5
Data added to condensed list for particle: 5
Particle 6:
eta 4.416672761629267, phi 2.3825733663138404,
p 1.5, q 1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.01662179]
 [-0.01752269]
 [ 0.19987233]]

Simulating particle
Initiating Kalman filtering for particle: 6
Particle 6:
eta 4.389491072649373, phi 2.396319060405505,
p 1.49877814363109, q 1.60217662e-19,
brem = False
output State:
[[ 0.00133041]
 [ 0.00066477]
 [ 0.01683083]
 [-0.01823874]
 [ 0.20003528]]
Data added to pickle list for particle: 6
Data added to condensed list for particle: 6
Particle 7:
eta 3.5220654880598663, phi 1.0014923369460582,
p 1.5, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.04980238]
 [ 0.03187288]
 [-0.19987233]]

Simulating particle
Initiating Kalman filtering for particle: 7
Data added to pickle list for particle: 7
Data added to condensed list for particle: 7
2 particles skipped because: magnet loss in prediction