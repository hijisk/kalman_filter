This is the start of the log file generated at 02_03_12h16m27s

magfield_ shape: cs, sim_stepsize: 0.01, detector_length: 11
15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 196 particles, from 2 to 100 GeV. Brem = False, ms = True

Particle 0:
eta 3.2505943820456524, phi 1.23043762923871,
p 2.0, q 1.60217662e-19,
brem = False
input State:
[[0.001     ]
 [0.001     ]
 [0.07316628]
 [0.02591115]
 [0.14990425]]

Simulating particle
Initiating Kalman filtering for particle: 0
Exception occured at particle 0
Data added to pickle list for particle: 0