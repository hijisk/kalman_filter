This is the start of the log file generated at 09_04_17h14m48s

15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 1 particles...
Particle 0:
eta 4.092733044301782, phi -2.757962722523611,
p 50.0, q 1.60217662e-19,
brem = False
input State:
[[ 0.        ]
 [ 0.        ]
 [-0.01249991]
 [-0.03096889]
 [ 0.00599617]]

Simulating particle
Particle track 0 is saved
Initiating Kalman filtering for particle: 0
z: 0.26
The prediction: [[-0.00322127]
 [-0.00804824]]
The measurement: [-0.00308816]
The filtered state: [[-0.00308829]
 [-0.00804824]]
z: 0.29
The prediction: [[-0.00346008]
 [-0.00897754]]
The measurement: [-0.00889381]
The filtered state: [[-0.00346008]
 [-0.0088939 ]]
z: 0.45
The prediction: [[-0.00545628]
 [-0.01388119]]
The measurement: [-0.00558287]
The filtered state: [[-0.00550097]
 [-0.01388119]]
z: 0.6
The prediction: [[-0.00733397]
 [-0.01852774]]
The measurement: [-0.01858438]
The filtered state: [[-0.00733397]
 [-0.01854388]]
z: 0.65
The prediction: [[-0.00795794]
 [-0.02011972]]
The measurement: [-0.00798711]
The filtered state: [[-0.00765734]
 [-0.02011972]]
z: 0.7
The prediction: [[-0.00819108]
 [-0.02166466]]
The measurement: [-0.0216846]
The filtered state: [[-0.00819108]
 [-0.02166284]]
z: 0.75
The prediction: [[-0.00873601]
 [-0.02323615]]
The measurement: [-0.00932303]
The filtered state: [[-0.00934862]
 [-0.02323615]]
z: 2.33
The prediction: [[-0.02992165]
 [-0.0719781 ]]
The measurement: [-0.02933405]
The filtered state: [[-0.02933968]
 [-0.07197811]]
z: 2.37
The prediction: [[-0.02987421]
 [-0.07324293]]
The measurement: [-0.0730604]
The filtered state: [[-0.02987421]
 [-0.07303964]]
z: 2.6
The prediction: [[-0.03289742]
 [-0.08010944]]
The measurement: [-0.03278672]
The filtered state: [[-0.03283443]
 [-0.08010944]]
z: 2.64
The prediction: [[-0.03337747]
 [-0.08136971]]
The measurement: [-0.08168464]
The filtered state: [[-0.03337747]
 [-0.08155711]]
z: 7.86
The prediction: [[-0.16326355]
 [-0.24251035]]
The measurement: [-0.16258121]
The filtered state: [[-0.16278313]
 [-0.24250932]]
z: 7.87
The prediction: [[-0.16314101]
 [-0.2428176 ]]
The measurement: [-0.24350867]
The filtered state: [[-0.16314104]
 [-0.2432729 ]]
z: 7.91
The prediction: [[-0.1646101 ]
 [-0.24453945]]
The measurement: [-0.16414614]
The filtered state: [[-0.164417  ]
 [-0.24453931]]
z: 7.92
The prediction: [[-0.1647755 ]
 [-0.24484822]]
The measurement: [-0.24671693]
The filtered state: [[-0.16477554]
 [-0.24559678]]
z: 7.98
The prediction: [[-0.16696588]
 [-0.24748748]]
The measurement: [-0.16668295]
The filtered state: [[-0.16688174]
 [-0.24748744]]
z: 7.99
The prediction: [[-0.16724128]
 [-0.24779739]]
The measurement: [-0.24727229]
The filtered state: [[-0.16724127]
 [-0.2476451 ]]
z: 8.03
The prediction: [[-0.16868097]
 [-0.24888407]]
The measurement: [-0.16849292]
The filtered state: [[-0.16863748]
 [-0.24888405]]
z: 8.04
The prediction: [[-0.16899774]
 [-0.24919379]]
The measurement: [-0.25149429]
The filtered state: [[-0.16899775]
 [-0.24971652]]
z: 8.54
The prediction: [[-0.18718237]
 [-0.26523929]]
The measurement: [-0.18710799]
The filtered state: [[-0.18716734]
 [-0.26523928]]
z: 8.55
The prediction: [[-0.18757075]
 [-0.26558078]]
The measurement: [-0.26336148]
The filtered state: [[-0.18757073]
 [-0.2651227 ]]
z: 8.59
The prediction: [[-0.18903875]
 [-0.26636217]]
The measurement: [-0.18865142]
The filtered state: [[-0.18897326]
 [-0.26636214]]
z: 8.6
The prediction: [[-0.18934048]
 [-0.26667201]]
The measurement: [-0.26532857]
The filtered state: [[-0.18934047]
 [-0.2664397 ]]
z: 8.66
The prediction: [[-0.19158273]
 [-0.26832809]]
The measurement: [-0.19089505]
The filtered state: [[-0.19148252]
 [-0.26832805]]
z: 8.67
The prediction: [[-0.1918504 ]
 [-0.26863762]]
The measurement: [-0.27225582]
The filtered state: [[-0.19185042]
 [-0.2691793 ]]
z: 8.71
The prediction: [[-0.19335975]
 [-0.27045135]]
The measurement: [-0.1931881]
The filtered state: [[-0.19333781]
 [-0.27045135]]
z: 8.72
The prediction: [[-0.19370617]
 [-0.2707616 ]]
The measurement: [-0.26804273]
The filtered state: [[-0.19370616]
 [-0.27040357]]
z: 9.23
The prediction: [[-0.21260445]
 [-0.28620373]]
The measurement: [-0.21210121]
The filtered state: [[-0.21254695]
 [-0.28620369]]
z: 9.24
The prediction: [[-0.21291944]
 [-0.2865135 ]]
The measurement: [-0.28633022]
The filtered state: [[-0.21291944]
 [-0.28648965]]
z: 9.28
The prediction: [[-0.21441008]
 [-0.28772877]]
The measurement: [-0.21390341]
The filtered state: [[-0.21435824]
 [-0.28772873]]
z: 9.29
The prediction: [[-0.21473108]
 [-0.28803851]]
The measurement: [-0.28687754]
The filtered state: [[-0.21473107]
 [-0.28790345]]
z: 9.35
The prediction: [[-0.2170067 ]
 [-0.28979213]]
The measurement: [-0.21644415]
The filtered state: [[-0.21695476]
 [-0.28979209]]
z: 9.36
The prediction: [[-0.21732806]
 [-0.29010171]]
The measurement: [-0.2894709]
The filtered state: [[-0.21732805]
 [-0.29003498]]
z: 9.4
The prediction: [[-0.21885917]
 [-0.29130409]]
The measurement: [-0.21815895]
The filtered state: [[-0.21880025]
 [-0.29130406]]
z: 9.41
The prediction: [[-0.21917387]
 [-0.2916136 ]]
The measurement: [-0.29084014]
The filtered state: [[-0.21917387]
 [-0.29153879]]
Particle 0:
eta 4.09789022738458, phi -2.7690486839029416,
p 50.38120873583336, q 1.60217662e-19,
brem = False
output State:
[[-3.26743393e-04]
 [ 6.01574102e-05]
 [-1.20932616e-02]
 [-3.09454616e-02]
 [ 5.95080005e-03]]
Exception occured at particle 0
Data added to list for particle: 0
data saved to data/toy_output/146