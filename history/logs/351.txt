This is the start of the log file generated at 09_24_15h16m19s

15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 3 particles...
Particle 0:
eta 4.768781094173763, phi 0.6738152500560034,
p 3.0, q 1.60217662e-19,
brem = False
input State:
[[-0.001     ]
 [-0.001     ]
 [ 0.01059671]
 [ 0.01327105]
 [ 0.09993617]]

Simulating particle
Particle track 0 is saved
Skip particle 0 because:The particle did not register in any subdetector
Data added to pickle list for particle: 0
Particle 1:
eta 3.8494317266632123, phi 1.7570679216124834,
p 36.0, q -1.60217662e-19,
brem = False
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.04186601]
 [-0.00788991]
 [-0.00832801]]

Simulating particle
Particle track 1 is saved
Skip particle 1 because:The particle did not register in any subdetector
Data added to pickle list for particle: 1
Particle 2:
eta 3.243486315968332, phi -0.9711498494587123,
p 69.0, q 1.60217662e-19,
brem = False
input State:
[[-0.001     ]
 [-0.001     ]
 [-0.0645356 ]
 [ 0.0441177 ]
 [ 0.00434505]]

Simulating particle
Particle track 2 is saved
Skip particle 2 because:The particle did not register in any subdetector
Data added to pickle list for particle: 2
data saved to data/toy_output/351
3 particles skipped because: The particle did not register in any subdetector