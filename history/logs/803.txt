This is the start of the log file generated at 02_11_20h30m46s

magfield_ shape: cs, sim_stepsize: 0.01, detector_length: 11
15 VELO-type subdetectors added at z-locations: [20, 50, 80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700, 750]
4 TT-type subdetectors added at z-locations: [2330, 2370, 2600, 2640]
24 OT-type subdetectors added at z-locations: [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590, 8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360, 9400, 9410]
The detector is built.
Generating 39200 particles, from 2 to 100 GeV. Brem = True, ms = True

Particle 0:
eta 2.5097562786323913, phi 0.796509087935679,
p 2.0, q 1.60217662e-19,
brem = True
input State:
[[0.001     ]
 [0.001     ]
 [0.11700195]
 [0.11443042]
 [0.14990425]]

Simulating particle
Initiating Kalman filtering for particle: 0
Particle 0:
eta 2.5083691038038265, phi 0.791065170710615,
p 1.6280322503235298, q 1.60217662e-19,
brem = True
output State:
[[0.00335585]
 [0.00326285]
 [0.11654098]
 [0.11522753]
 [0.18415391]]
Data added to pickle list for particle: 0
Data added to condensed list for particle: 0
Particle 1:
eta 4.1645371877811845, phi -2.4291468242084266,
p 2.0, q -1.60217662e-19,
brem = True
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.02031744]
 [-0.02352126]
 [-0.14990425]]

Simulating particle
Initiating Kalman filtering for particle: 1
Particle 1:
eta 4.116545497552076, phi -2.449950656134313,
p 1.4359091186293793, q -1.60217662e-19,
brem = True
output State:
[[ 0.00060927]
 [ 0.00055564]
 [-0.02079882]
 [-0.02511631]
 [-0.20879351]]
Data added to pickle list for particle: 1
Data added to condensed list for particle: 1
Particle 2:
eta 3.649425788750312, phi 1.0763917764610131,
p 2.0, q 1.60217662e-19,
brem = True
input State:
[[0.001     ]
 [0.001     ]
 [0.04581472]
 [0.02469685]
 [0.14990425]]

Simulating particle
Initiating Kalman filtering for particle: 2
Skip particle 2 because:('overflow encountered in double_scalars',)
Data added to pickle list for particle: 2
Data added to condensed list for particle: 2
Particle 3:
eta 2.1786429124386872, phi 1.0538592544357563,
p 2.0, q -1.60217662e-19,
brem = True
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.19936375]
 [ 0.11333867]
 [-0.14990425]]

Simulating particle
Skip particle 3 because:Too much Brem loss
Data added to pickle list for particle: 3
Data added to condensed list for particle: 3
Particle 4:
eta 2.289085763151952, phi -2.837197104279694,
p 2.0, q 1.60217662e-19,
brem = True
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.06138868]
 [-0.19540641]
 [ 0.14990425]]

Simulating particle
Initiating Kalman filtering for particle: 4
Particle 4:
eta 2.288884394814812, phi -2.8359843880767133,
p 1.3716586535512598, q 1.60217662e-19,
brem = True
output State:
[[-0.00022642]
 [-0.00290461]
 [-0.06163828]
 [-0.19537197]
 [ 0.21857369]]
Data added to pickle list for particle: 4
Data added to condensed list for particle: 4
Particle 5:
eta 3.7355284370472805, phi 1.5078848001495813,
p 2.0, q -1.60217662e-19,
brem = True
input State:
[[ 0.001     ]
 [ 0.001     ]
 [ 0.04765384]
 [ 0.00300194]
 [-0.14990425]]

Simulating particle
large brem at z = 6.67
initial q/p: -0.2883424616868834
  final q/p: -37.02753467565775
large brem at z = 7.21
initial q/p: -37.42308629318629
  final q/p: -177.425637526562
large brem at z = 7.33
initial q/p: -177.4256375265754
  final q/p: -978.7784309152212
Skip particle 5 because:Too much Brem loss
Data added to pickle list for particle: 5
Data added to condensed list for particle: 5
Particle 6:
eta 2.3191237069819564, phi -0.5337608402586044,
p 2.0, q 1.60217662e-19,
brem = True
input State:
[[ 0.001     ]
 [ 0.001     ]
 [-0.10106364]
 [ 0.17101021]
 [ 0.14990425]]

Simulating particle
large brem at z = 1.1500000000000001
initial q/p: 0.15033235781940607
  final q/p: 0.35253999492597327
large brem at z = 5.61
initial q/p: 0.3575552981141244
  final q/p: 410.00176676529276
Data added to pickle list for particle: 6
Data added to condensed list for particle: 6
1 particles skipped because: FloatingPointError
2 particles skipped because: bremloss