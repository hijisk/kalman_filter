import mod.globals as glob
from mod.build.particle import particlegen, Particle
from mod.objects import State_Vector
from mod.run import run
import numpy as np

# np.set_printoptions(suppress=True, precision=3)

#input('Changes')


# particlegen(      start, stop, step, binsize, brem, ms)
np.seterr(all='raise', under='warn')
particles = particlegen(1.5,  100,  5,     2, False, True)

#particle = Particle(0, State_Vector([.1, .1, -1, 1, -.005]), False, True)

run(particles, plot=False)
