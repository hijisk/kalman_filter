import sys
import collections
import analysis as anal
from analysis.handling import list_of_dicts_to_df, average_tracks
import numpy as np
import pandas as pd
import mod.globals as glob
import matplotlib.pyplot as plt
import os
#glob.init(False, 'cs', 1/1000, 11, 1)
mmx_y = np.array([[0, 1, 0, 0, 0]])


# we open a saved dataframe, the number of the run of which we need the data
# can be specified after the file name.
run = sys.argv[1]
if not os.path.isdir('output/' + run):
    os.mkdir('output/' + run)
df = anal.importdata('data/' + run, False)
full = len(df)
dfred = df[df.chi_sq < 100]
reduced = len(df)
print("Amount of by chi squared filtered particles: ", full-reduced)
print("Amount of particles: {}".format(len(df)))
print("Lowest p: {}, Highest p: {}".format(df.real_p.min(), df.real_p.max()))
# print('unselected average eta', df.real_eta.mean())

# print('average delta p', df.del_p.mean())
x_axis_min = -4
x_axis_max = 4

bins = np.linspace(x_axis_min, x_axis_max, num=50)


#print(df[df.del_p_over_p>.1]['del_p_over_p'])
#df = df[abs(df.del_p_over_p)<1]
anal.delphist(df, run, text='_over_err', bins=bins)
# print(df)
# print(df['real_qp'][0])
exit()

def draw_sd_background(ax):
    names = ['VELO', '' , 'Rich', 'TT', 'magnet', 'OT']
    x_values = [0, .830, .978, 2.250, 2.750, 7.600, 9.300, 11]
    for index, name in enumerate(names):
        c1, c2 = ['y', 'white'][::(-1)**index]
        xmin, xmax = x_values[index], x_values[index+1]
        ax.axvspan(xmin, xmax, facecolor=c1, alpha=0.4)
        ax.text((xmin + xmax)/2, ax.get_ylim()[1], name, va='top', ha='center', color=c2, weight='black')
    return ax


'''
track = pd.DataFrame.from_records(df.iloc[1]['track'])
print(track)
np.set_printoptions(suppress=False, precision=1)
for i in range(0, len(track)):
    print(track.iloc[i]['filt']['cov'])
'''
'''
avg_dict = {'q_all': average_tracks(df),
            'q_neg': average_tracks(df[df['charge']<0]),
            'q_pos': average_tracks(df[df['charge']>0])}
'''
#avg = average_tracks(df)
'''

#print(avg[['filt_p', 'filt_del_p', 'filt_cov[p,p]', 'filt_del_p_over_cov_p', 'filt_del_qp', 'filt_cov[qp,qp]']])
#print(avg[['smthed_p', 'smthed_del_p', 'smthed_cov[p,p]', 'smthed_del_p_over_cov_p']])
#print(avg[['pred_p', 'pred_del_p', 'pred_cov[p,p]', 'pred_del_p_over_cov_p']])


all_steps = [['filt', 'filtered'], ['smthed', 'smoothed'], ['pred', 'predicted']]
all_attrs = ['x', 'y', 'tx', 'ty', 'p' ,'qp']
d_or_over_cov = [['_del_{0}_over_cov_{0}'.format, ' over covariance'],
                         ['_del_{}'.format,'']]
#select which ones you want from the lists above
for stext, ltext in [['filt', 'filtered']]:#[['smthed', 'smoothed']]:
    for sselec, lselec in [['_cov[{0},{0}]'.format, 'covariance']]:
        fig, axes = plt.subplots()
        for attr in ['x', 'y']:
            axes.plot(avg[stext + sselec(attr)], label=attr)
                #axes.plot(avg_dict[q]['gain_qp'], label='gain_qp '+q)
        axes.set_xlabel('z, meters along beam axis')
        axes.legend()
        axes.set_ylabel(ltext + ' covariance' + lselec)
        axes.set_title(ltext + ' covariance' + lselec)
        draw_sd_background(axes)
        fig.savefig('output/' + run + '/' + stext + sselec('xy'))


for stext, ltext in all_steps:#[['smthed', 'smoothed']]:
    for sselec, lselec in [['_del_{0}_over_cov_{0}'.format, ' over covariance']]:
        fig, axes = plt.subplots()
        for attr in ['p', 'x', 'y']:
            for q in avg_dict.keys():
                axes.plot(avg_dict[q][stext + sselec(attr)], label=attr + ' ' + q)
                #axes.plot(avg_dict[q]['gain_qp'], label='gain_qp '+q)
        axes.set_xlabel('z, meters along beam axis')
        axes.legend()
        axes.set_ylabel(ltext + ' difference' + lselec)
        axes.set_title(ltext + ' difference' + lselec)
        draw_sd_background(axes)
        fig.savefig('output/' + run + '/' + stext + sselec('qpp'))


for stext, ltext in all_steps:
    for attr in ['p', 'x']:
        fig, axes = plt.subplots()
        axes.plot(avg_dict[q][stext + '_cov[{0},{0}]'.format(attr)],
                  label='cov[{0},{0}]'.format(attr))
        axes.set_xlabel('z, meters along beam axis')
        axes.legend()
        axes.set_ylabel(ltext + ' covariance')
        axes.set_title(ltext + ' covariance')
        draw_sd_background(axes)
        fig.savefig('output/' + run + '/' + stext + '_cov_p')
'''
'''
fig, axes = plt.subplots()
axes.plot(avg['chi_sq'], label='chi_sq')
axes.set_title(' chi_sq ')
axes.set_xlabel('z')
#axes.set_ylabel(ltext + ' difference over covariance')
axes.legend()
draw_sd_background(axes)
fig.savefig('output/' + 'chi_sq')
'''
'''
fig, axes = plt.subplots()
axes.plot(avg[text + '_del_x'], label='x')
axes.plot(avg[text + '_del_y'], label='y')
axes.plot(avg[text + '_del_p'], label='p')
axes.set_xlabel('z')
axes.set_ylabel(text + ' difference with real')
axes.legend()
draw_sd_background(axes)
axes.set_title(text + ' difference')
fig.savefig('output/' + text + '_del.png')
'''
'''
fig, ax = plt.subplots()

ax.plot(track['z'], track['gain_qp'], label='qp portion of Gain')

ax.set_xlabel('z in m')
ax.set_ylabel('qp component of gain')
fig.savefig('output/tracking_gain_mx')

fig, ax = plt.subplots()
#ax.plot(track['z'], track['filt_x'] - track['pred_x'])
ax.plot(track['z'], track['filt_momentumGeV'])
ax.set_xlabel('z in m')
ax.set_ylabel('filteredmomentum')
fig.savefig('output/tracking_momentum')
'''
'''
fig, ax = plt.subplots()
ax.hist(df['del_p_over_err'], bins=20)
plt.savefig('output/del_p_over_err')


anal.delphist(df, run, 'seed')
anal.delphist(df, run)

#anal.scatter(df[df.real_p > 5], run, ['real_qp', 'del_p_over_p'])
'''
'''
anal.scatter(
    df, run,
    ['real_phi', 'del_p_over_p'], ylim=[-.06, .04]
    )

anal.scatter(
    df, run,
    ['real_p', 'seed_del_p_over_p']
    )
'''
#print(df[['seed_del_ty_over_ty', 'del_ty_over_ty', 'seed_del_p_over_p', 'del_p_over_p']])



too_high = df[df.seed_del_p > 0]
too_low = df[df.seed_del_p < 0]

print('amount too high: {}, amount too low: {}'.format(
    len(too_high.index), len(too_low.index)))

#print('average eta where momentum is too high', too_high.real_eta.mean())

#print('average eta where momentum is too low', too_low.real_eta.mean())


#anal.makeplots(df, run)

anal.delpoverp(df, run)

anal.delphist(df[df.real_p > 95], run, bins=bins)
# anal.scatter(df, run, 'real_eta', 'del_p_over_p')



print('Mean del_p/p: ', df.del_p_over_p.mean())
print('Variance on del_p/p', df.del_p_over_p.var())
print('Standard deviation on del_p/p', df.del_p_over_p.std())

print('Mean del_x: ', df.del_x.mean())
print('Variance on del_x', df.del_x.var())
print('Standard deviation on del_x', df.del_x.std())

print('Mean del_y: ', df.del_y.mean())
print('Variance on del_y', df.del_y.var())
print('Standard deviation on del_y', df.del_y.std())

print('Mean del_tx: ', df.del_tx.mean())
print('Variance on del_tx', df.del_tx.var())
print('Standard deviation on del_tx', df.del_tx.std())

print('Mean del_ty: ', df.del_ty.mean())
print('Variance on del_ty', df.del_ty.var())
print('Standard deviation on del_ty', df.del_ty.std())




'''
print('Mean seed_del_p/p: ', df.seed_del_p_over_p.mean())
print('Variance on seed_del_p/p', df.seed_del_p_over_p.var())
'''
#print('Mode del_p/p: ', df.del_p_over_p.mode())
print('Median del_p/p:', df.del_p_over_p.median())



'''
print('Mean seed_del_qp/qp: ', df.seed_del_qp_over_qp.mean())
print('Variance on seed_del_qp/qp', df.seed_del_qp_over_qp.var())

print('Mean seed_del_p: ', df.seed_del_p.mean())
'''


anal.scatter(
    df, run,
    ['seed_del_p_over_p', 'del_p_over_p'], ylim=[-.3, .3]
    )

anal.scatter(
    df, run,
    ['real_eta', 'del_p_over_p'], ylim=[-.1, .1],
    colours=(abs(df.del_p_over_p) < .025).replace({True:'b', False:'r'})
    )

anal.scatter(
    df, run,
    ['total_sd_hits', 'real_eta'],
    colours=(abs(df.del_p_over_p) < .025).replace({True:'b', False:'r'})
    )

anal.scatter(
    df, run,
    ['chi_sq', 'del_p_over_p']
)

anal.scatter(
    df, run,
    ['real_eta', 'real_phi'],
    colours=(abs(df.del_p_over_p) < .025).replace({True:'b', False:'r'})
    )

# print(df[00:50].to_string())
