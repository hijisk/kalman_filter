import sys
import collections
import analysis as anal
from analysis.handling import list_of_dicts_to_df, average_tracks
import numpy as np
import pandas as pd
import mod.globals as glob
import matplotlib.pyplot as plt
import pickle
import os

run = sys.argv[1]

listdata = []
inputfile = 'data/' + run + '.condensed'
with (open(inputfile, "rb")) as file:
    while True:
        try:
            listdata.append(pickle.load(file))
        except EOFError:
            break

print('amount of particles:',len(listdata))
data = pd.DataFrame(listdata)
data.columns = ['id', 'real_p', 'del_p_over_p']
df = data.dropna()
print('amount of above 100% p deviation:', len(df[df.del_p_over_p > 1]))
#print(df[df.del_p_over_p > 0.5])


x_axis_min = -.008
x_axis_max = .008
bins = np.linspace(x_axis_min, x_axis_max, num=100)

anal.delphist(df, run, bins=bins, BH=False)
df = df[df.del_p_over_p < .1]



anal.delpoverp(df, run)
