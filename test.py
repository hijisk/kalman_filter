
import mod.globals as glob
import math
glob.init(False, 'cs', 1/100, 11, 1)
import mod.kalman.propagation as prop
import mod.objects as obj
import mod.build.particle as part
import numpy as np
import mod.build.magfield as mag
import pandas as pd
np.set_printoptions(precision=3)
import matplotlib.pyplot as plt
from mod.processes.magnetic import track_differential, RK_propagation
from scipy.integrate import RK45
from mod.processes.multiplescat import calc_theta_0

#print(calc_theta_0(part.to_state(3, 1, 100, -1), 3, 7))

'''
def test():
    state = part.to_state(3, 1, 100, -1).flatten()
    h = np.array([1e-6, 1e-6, 1e-6, 1e-6, 1e-7/state.qp**2])*1e-5
    print(h)
    endstate, mx1 = RK_propagation(state, 4, 4.2, 'kalman', h)
    h *= 1.01
    t, mx2 = RK_propagation(state, 0, .1, 'kalman', h)
    
    print(endstate)
    print(mx1)
    print(np.sum(abs(mx1-mx2)))


test()
'''
'''
def diff_differential(column):
    def fn(z, state):
        return tr_dif_derivative(z, state, column)
    return fn


def tr_dif_derivative(z, state, column):
    x = state
    B = mag.magnetic_field_strength(z)
    ddzd = np.zeros((5, 5))
    ddzd[0, 2] = 1
    ddzd[1, 3] = 1

    dtxdtx = ((B * x[2] * (-1 - x[2]**2) * x[4])/math.sqrt(1 + x[2]**2 + x[3]**2) -
     2 * B * x[2] * math.sqrt(1 + x[2]**2 + x[3]**2) * x[4])
    dtxdty = (B * x[3]*(-1 - x[2]**2) * x[4])/math.sqrt(1 + x[2]**2 + x[3]**2)
    dtxdqp = B * (-1 - x[2]**2) * math.sqrt(1 + x[2]**2 + x[3]**2)

    dtydtx = -((B * x[2]**2 * x[3] * x[4])/math.sqrt(1 + x[2]**2 + x[3]**2) +
 B * x[3] * math.sqrt(1 + x[2]**2 + x[3]**2) * x[4])
    dtydty = -((B * x[2] * x[3]**2 * x[4])/math.sqrt(1 + x[2]**2 + x[3]**2) +
 B * x[2] * math.sqrt(1 + x[2]**2 + x[3]**2) * x[4])
    dtydqp = - B * x[2] * x[3] * math.sqrt(1 + x[2]**2 + x[3]**2)

    ddzd[2, 2] = dtxdtx
    ddzd[2, 3] = dtxdty
    ddzd[2, 4] = dtxdqp
    ddzd[3, 2] = dtydtx
    ddzd[3, 3] = dtydty
    ddzd[3, 4] = dtydqp
    print(ddzd)
    return ddzd[:, column].flatten()

test_state=obj.State_Vector([0, 0, 0, 0, 0])
np.identity(5)
mx = np.empty((5,5))
for i in range(5):
    rksolver = RK45(diff_differential(i), 4, np.identity(5)[:,i].flatten(), 5, atol=1e-10, rtol=1e-10)
    while rksolver.status == 'running':
        rksolver.step()
        print(rksolver.t, rksolver.y)
    mx[:, i] = rksolver.y.T
print(mx)


'''
'''

test_start_state = obj.State_Vector([1, 0.01, 1, 1, 0.0000000000000005])
start_z = 4
end_z = 4.01

testpart = part.Particle(0, test_start_state, brem=False, ms=False)
print('simulation\n')
track = part.tracksim(start_z, end_z, [test_start_state], False, False)
glob.compare = track
print('\nTaylor approximation\n')
prop_obj = prop.Kalman_propagation(start_z, end_z, test_start_state, brem=False, ms=False)
Fmx_end_state = prop_obj.matrix @ test_start_state

np.set_printoptions(suppress=False)
print(track)
print('sim end state', track[-1])
print('Fmx_end_state', Fmx_end_state)
print('difference', Fmx_end_state - track[-1])

'''
'''
a = pd.DataFrame(np.array([[12, 13, 14, 10], [1, 2, 3, 4]]).T,
    columns=['number', 'id'])

a=a.set_index('id')

b = pd.DataFrame(np.array([[12, 12, 21], [1, 2, 4]]).T,
    columns=['number', 'id']).set_index('id')
fig, ax= plt.subplots()
ax.plot(a.add(b, fill_value=0.))

#def patch_background(ax, division_list):
names = ['VELO', 'TT', 'magnet', 'OT']
x_values = [1, 2, 3, 4, 5]
for index, name in enumerate(names):
    c1, c2 = ['y', 'white'][::(-1)**index]
    xmin, xmax = x_values[index], x_values[index+1]
    ax.axvspan(xmin, xmax, facecolor=c1, alpha=0.4)
    ax.text((xmin + xmax)/2, ax.get_ylim()[1]*.99, name, va='top', ha='center', color=c2, weight='black')

fig.savefig('testplot')

'''
def step_test_track_gen():
    id = 0
    eta = 3
    phi = 1
    p = 10
    q = -1
    part.stepsize = 1e-5
    statevec = part.to_state(eta, phi, p, q)
    particle = part.Particle(id, statevec, False, False)
    particle.simulate()
    original = particle.get_track()[-1][0]
    save = []
    
    for test_stepsize in np.logspace(-1, -4, num=4):
        eta = 3
        phi = 1
        p = 10
        q = -1
        statevec = part.to_state(eta, phi, p, q)
        part.stepsize = test_stepsize
        particle = part.Particle(id, statevec, False, False)
        id += 1
        particle.simulate()
        save.append((original - particle.get_track()[-1][0]).flatten())
    table = np.array(save)
    print(table)
    np.save('output/steptesttable', table)


#test_track_gen()
'''
table = np.load('output/steptesttable.npy')
plt.rcParams.update({'font.size': 15})
fig = plt.figure(figsize=[7, 23])
axs = fig.subplots(5, 1)
for i in range(5):
    ax = axs.flatten()[i]
    labels = [r'$x$', r'$y$', r'$t_x$', r'$t_y$', r'$\frac{q}{p}$']
    for param in range(5):
        y = abs(table[i*9:(i+1)*9])[:, param]
        if 0 in y:
            continue
        ax.loglog(np.logspace(-1,-9,9), y, label=labels[param])
    
    ax.loglog(np.logspace(-1,-9,9), np.logspace(-1,-9,9), ':k', label='$x=y$')
    ax.grid(True)
    ax.legend(prop={'size': 12})
    ax.set_title('Dependence of parameters at z=10 on '+labels[i]+' at z=0')
    ax.set_ylabel(r'output $\Delta$')
    ax.set_xlabel('input ' + r'$\Delta$'+labels[i])

fig.tight_layout()
fig.savefig('output/steptest')
'''
def stab_test_track_gen():
    id = 0
    eta = 3
    phi = 1
    p = 10
    q = -1
    statevec = part.to_state(eta, phi, p, q)
    particle = part.Particle(id, statevec, False, False)
    particle.simulate()
    original = particle.get_track()[-1][0]
    save = []
    for vindex in range(5):
        for increment in np.logspace(-1, -9, num=9):
            eta = 3
            phi = 1
            p = 10
            q = -1
            statevec = part.to_state(eta, phi, p, q)
            statevec[vindex][0] += increment
            particle = part.Particle(id, statevec, False, False)
            id += 1
            particle.simulate()
            save.append((original - particle.get_track()[-1][0]).flatten())
    table = np.array(save)
    print(table)
    np.save('output/stabtesttable', table)


#step_test_track_gen()


table = np.load('output/stabtesttable.npy')
plt.rcParams.update({'font.size': 14})
for i in range(5):
    fig, ax = plt.subplots()
    labels = [r'$x$', r'$y$', r'$t_x$', r'$t_y$', r'$\frac{q}{p}$']
    for param in range(5):
        y = abs(table[i*9:(i+1)*9])[:, param]
        if 0 in y:
            continue
        ax.loglog(np.logspace(-1,-9,9), y, label=labels[param])
    
    ax.loglog(np.logspace(-1,-9,9), np.logspace(-1,-9,9), ':k', label='$x=y$')
    ax.grid(True)
    ax.legend(prop={'size': 12})
    ax.set_title('Dependence of parameters at z=10 on '+labels[i]+' at z=0')
    ax.set_ylabel(r'output $\Delta$')
    ax.set_xlabel('input ' + r'$\Delta$'+labels[i])

    fig.tight_layout()
    fig.savefig('output/stabtest'+str(i))

