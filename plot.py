import matplotlib.pyplot as plt
import numpy as np
import mod.globals as glob
import mod.objects as obj


folder = "output/plots/"


def plotreal(plt, coordinate, particle):
    '''this function plots the actual trajectory'''
    # load the states from file
    states = particle.get_track()
    # initialise lists
    coorlist = []
    z = []
    for (count, state) in enumerate(states):
        # append list
        z.append(count * glob.sim_stepsize)
        coorlist.append(state[0][obj.xy[coordinate]])

    plt.plot(z, coorlist, label="real " + coordinate + " location")
    return


def plot_coord(z, meas, meas_error, smth, filt, pred, string, particle):
    '''this function plots everything for one coordinate'''
    plt.clf()  # clear previous
    plt.style.use('seaborn-whitegrid')  # nice style
    plotreal(plt, string, particle)
    plt.errorbar(z, meas, yerr=meas_error, fmt='.k')
    plt.plot(z, smth, label='smoothed states')
    plt.plot(z, filt, label='filtered states')
    plt.plot(z, pred, linestyle=':', label='predicted states')
    # plt.xlim(7, 8)
    # plt.ylim(2.2, 2.4)
    plt.legend()

    plt.savefig(folder + "p_" + str(particle.id) + "_plot_z_" + string + ".png")


def plot_all(particle, measurements):
    smthed, coords, err, z = [], [[0, 0]], [[0, 0]], [0]
    filt, pred = [], []
    for state in particle.track_fit:
        smthed.append(state['smthed']['vec'][:2])
        filt.append(state['filt']['vec'][:2])
        pred.append(state['filt']['vec'][:2])
    for measurement in measurements:
        matrix = measurement.mx.flatten()
        [x, y] = matrix[:2] * measurement.value
        coords.append([x, y])
        z.append(measurement.z)
        [x_err, yerr] = matrix[:2] * measurement.var
        err.append([x_err, yerr])

    coords = np.array(list(zip(*coords)))
    err = list(zip(*err))
    smthed = list(zip(*smthed))
    filt = list(zip(*filt))
    pred = list(zip(*pred))

    zeroes = np.argwhere(coords == 0.0)
    for zero in zeroes:
        coords[zero[0], zero[1]] = float("Nan")

    for string, number in obj.xy.items():
        plot_coord(z, coords[number], err[number], smthed[number],
                   filt[number], pred[number], string, particle)


def plotlines(data, line, name, *args):
    z, coor, err = data
    a, b = line

    plt.clf()  # clear previous
    plt.style.use('seaborn-whitegrid')  # grid
    plt.errorbar(z, coor, yerr=err, fmt='.k')
    plt.plot(z, a * np.array(z) + b, label="Fitted line")

    for arg in args:
        a, b = arg
        plt.plot(z, a * np.array(z) + b, label="Fitted line")

    plt.savefig(folder + name + ".png")
