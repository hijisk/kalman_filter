''' This file contains the functions needed to go from measurements to a track
seed. This means that we use the measurements to make a quick initial guess
about the momentum and other initial conditions (at z=0). This is important to
to make sure the filter doesn't predict the momentum to be completely off,
thereby making the calculations impossible.
'''
import numpy as np
# import plot
import math
from mod.build.magfield import bending_power, midpoint
import mod.objects as obj
# from mod.globals import timer_start, timer_end


def seed(measurements_array, direction="forward"):
    '''find a starting state from the measurements'''
    meas_x, meas_y = measurements_array

    yline = Linear_track_fit(meas_y)
    # timer_start("seedplot")
    # plot.plotlines(list_each_attr(meas_y), yline, name='y_seed_plot')
    # timer_end("seedplot")
    # We find a straight line through T-stations after Magnet.
    ds_x = []
    for meas in meas_x:
        if meas['z'] > 7:
            ds_x.append(meas)
    xline_ds = Linear_track_fit(ds_x)
    midlocation = xline_ds[0] * midpoint + xline_ds[1]
    xline_us = [midlocation/midpoint, 0]
    # timer_start("seedplot")
    # plot.plotlines(list_each_attr(meas_x), xline_us,
    #                  'x_seed_plot', xline_ds)
    # timer_end("seedplot")
    ty, y = yline
    qp = ptkick(xline_us[0], xline_ds[0], yline[0])

    if direction == "forward":
        tx, x = xline_us

    elif direction == "backward":
        z = max(meas_x[-1]['z'], meas_y[-1]['z'])
        tx, x = xline_ds
        x += tx * z
        y += ty * z

    covariance = [100, 100, 100, 100, 100]

    starting_state = obj.State([0, 0, tx, ty, qp/(-1.04663654228)],
                               np.diagflat(covariance))

    return starting_state


def Linear_track_fit(measurements):

    z, coor, err = list_each_attr(measurements)

    A = np.vstack([z, np.ones(len(z))]).T
    a, b = np.linalg.lstsq(A, coor, rcond=None)[0]
    'ax + b'
    return [a, b]


def list_each_attr(measurements):
    z = [meas['z'] for meas in measurements]
    coor = [meas['value'] for meas in measurements]
    err = [meas['error'] for meas in measurements]

    return [z, coor, err]


def ptkick(us_tx, ds_tx, ty):
    qp = (ds_tx / math.sqrt(1 + ds_tx**2 + ty**2) -
          us_tx / math.sqrt(1 + us_tx**2 + ty**2)) / bending_power(2.75, 7.6)

    return qp
