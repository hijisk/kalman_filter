import math
import numpy as np
import mod.objects as obj
from mod.build.material import x0

'''this file contains the multiple scattering functionality, it is used in both
the modeling and the kf-analysis.'''


def calc_theta_0(state, z1, z2):
    ''' This function calculates the standard deviation of the (gaussian) angle
    distribution for the multiple scattering process. This only works for unit
    or negative unit charge (from Jeroen van Tilburg eq 4.8)'''
    z_distance = z2 - z1
    momentum = state.momentum
    beta = state.beta
    distance = z_distance * state.lengthfactor
    x_x0=0
    for z in np.arange(z1, z2, 1/100):
        x_x0 += 1/100 / x0(z)
    #x_x0 = distance / x0(z1)
    c = 299792458
    bcp = beta * c * momentum
    # 13.6 MeV = 13.6 / 6.242e+12 J = 2.17896e-12 J
    t = 2.17896e-12 / bcp * math.sqrt(x_x0) * (1 + 0.038 * math.log(x_x0))
    # print("momentum: ", momentum, "\nbeta: ", beta, "\nt: ", t)
    return t


def picknewdirection(state, theta_0):
    '''This function gets a state and the calculated standard deviation of the
    angle distribution and returns a new state with the propagation angles
    changed'''
    initial_slopes = state.tx, state.ty
    initial_angles = np.arctan(initial_slopes)
    # choose an angle randomly
    theta = np.random.normal(0, theta_0)
    #print(theta)
    # the other angle has to be chosen
    phi = np.random.uniform(0, 2 * math.pi)
    angle_changes = theta * np.array([math.cos(phi), math.sin(phi)])

    final_angles = np.add(initial_angles, angle_changes)

    tx, ty = np.tan(final_angles)
    newstate = obj.State_Vector([state.x, state.y, tx, ty, state.qp])

    return newstate


def multiple_scattering(state, z1, z2):
    '''The ms modeling calculates the p, beta and actual travelled (straight)
    distance, then calculates the sigma for the scattering distribution, then
    returns the state for a new randomly picked direction using that
    distribution'''
    # calculate the scattering distribution parameter
    theta_0 = calc_theta_0(state, z1, z2)
    # randomly pick a new direction and calculate the state from that
    newstate = picknewdirection(state, theta_0)
    return newstate
