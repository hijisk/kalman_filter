import math
import numpy as np
from mod.build.magfield import magnetic_field_strength
import mod.objects as obj
from mod.globals import log
from scipy.integrate import RK45


def combine_track_diff(z, states):
    output = []
    for i in range(6):
        output += track_differential(z, states[i*5:(i+1)*5])
    return output


def track_differential(z, state):
    Bfield = magnetic_field_strength(z)
    # differential equations
    dxdz = state[2]
    dydz = state[3]
    try:  # this will fail when z velocity=0
        dtxdz = (state[4] * np.sqrt(1 + state[2]**2 + state[3]**2) *
                 (-(1. + state[2]**2) * Bfield))
        dtydz = (state[4] * np.sqrt(1 + state[2]**2 + state[3]**2) *
                 (-state[2] * state[3] * Bfield))
    except FloatingPointError as e:
        if np.any(abs(state[2:4]) > 1e50):
            raise obj.MagnetLoss
        raise e
    dqpdz = 0.
    return [dxdz, dydz, dtxdz, dtydz, dqpdz]


def RK_propagation(state, start_z, end_z, mode, h='default'):
    state = state.flatten()
    if mode == 'sim':
        diff_function = track_differential
        vector = state

    if mode == 'kalman':
        states = list(state)*6
        if h == 'default':
            h = np.array([1e-6, 1e-6, 1e-6, 1e-6, 1e-5])*1e-3
        for i in range(5):
            states[5*(i + 1) + i] += h[i]
        vector = states
        diff_function = combine_track_diff

    rksolver = RK45(diff_function, start_z, vector, end_z,
                    atol=1e-6, rtol=1e-6)

    while rksolver.status == 'running':
        rksolver.step()

    if mode == 'sim':
        return obj.State_Vector(rksolver.y)

    if mode == 'kalman':
        pred_vec = obj.State_Vector(rksolver.y[0:5])
        pred_cov = np.zeros((5, 5))
        for i in range(5):
            # Take derivative with the other states in the bundle
            pred_cov[:, i] = \
                (rksolver.y[5*(i+1):5*(i+2)] - rksolver.y[0:5])/h[i]
        return pred_vec, pred_cov
