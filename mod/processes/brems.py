'''This file contains the functions used to have the electrons lose energy
corresponding to bremsstrahlung by the Bethe-Heitler model.'''
import numpy as np
from mod.objects import State_Vector, Skip_particle
import mod.calculations as calc
from mod.build.material import x0
from mod.globals import log
import math


def c(t):
    return t/np.log(2)


def Bethe_Heitler_pdf(z, t):
    '''this is the probability density function, the Bethe-Heitler distribution
    from  Electron Reconstruction with the ATLAS Inner Detector
    -T.M. Atkinson eq. 3.5'''
    assert (z > 0).all()
    return (-np.log(z))**(c(t)-1) / math.gamma(c(t))


def pick_energy_factor(t):
    '''This function picks a value between 0 and 1 using the Bethe-Heitler
    distribution, which corresponds to a "skewed" gamma distribution'''
    z = np.exp(-np.random.gamma(c(t)))
    return z


def brems_E_loss(state, z1, z2):
    z_distance = z2 - z1
    length = state.lengthfactor * z_distance
    t = length / x0(z1)
    new_E = state.energy * pick_energy_factor(t)
    try:
        p = calc.momentum_from_energy(new_E)
    except ValueError as e:
        raise Skip_particle('Too much Brem loss').settype('bremloss')
    qp = state.q / p
    return State_Vector([state.x, state.y, state.tx, state.ty, qp])


def mean_brems_corr(state, z1, z2):
    '''this function calculates the mean energy loss due to bremsstrahlung in a
    z_distance ( << 1m) thick layer of material with radiation lenght x0 -- it
    is not used as it induces a bias in the result'''
    z_distance = z2 - z1
    exit()
    new_qp = state.qp * np.exp(z_distance/x0(z1) * state.lengthfactor)
    return State_Vector([state.x, state.y, state.tx, state.ty, new_qp])
