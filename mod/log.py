import time


def setlogvariables(newfile):

    timestamp = time.localtime()
    timestr = time.strftime("%m_%d_%Hh%Mm%Ss", timestamp)

    if newfile:
        with open("history/run_timestamp.txt", 'r') as file:
            lines = file.read().splitlines()
            run_number = str(int(lines[-1].split(', ')[0]) + 1)

        with open("history/run_timestamp.txt", 'a') as file:
            #params = input('Please state changes')
            file.write('\n' + run_number + ', ' + timestr)

        filename = "history/logs/" + run_number + ".txt"
        print('starting run ' + run_number + ", logging to new file")

    else:
        filename = "history/templog.txt"
        with open("history/run_timestamp.txt", 'r') as file:
            lines = file.read().splitlines()
            run_number = lines[-1].split(', ')[0]
        print('logging to templog.txt: ' + run_number)

    with open(filename, 'w') as file:
        file.write("This is the start of the log file generated at "
                   + timestr + '\n')# + 'parameters are' + params)

    return run_number, filename, timestr
