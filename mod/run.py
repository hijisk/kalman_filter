from mod.seeding import seed
from mod.build.measurements import generate_measurements
from mod.build.detector import detector
from mod.globals import log, datafile
from mod.objects import Skip_particle
import collections
import pickle as pickle
import time
import datetime
from plot import plot_all


def runparticle(particle, simulate=True, plot=False):
    print(particle.id)
    particle.logvars()
    if simulate is True:
        particle.simulate()

    meas_kf, meas_array = generate_measurements(particle, detector)

    # Check if it is possible to do the full reconstruction
    if particle.hits_amount == 0:
        string = "The particle did not register in any subdetector"
        raise Skip_particle(string).settype(string)

    if particle.subdetector_hits['T_station'] < 2:
        raise Skip_particle("Does not pass magnet").settype("upstream track")

    seed_state = seed(meas_array, 'forward')

    log("Initiating Kalman filtering for particle: ", particle.id)

    particle.fit_from(seed_state, meas_kf)

    particle.logvars('output')
    if plot:
        plot_all(particle, meas_kf)



def timing(remaining_length, starttime, total, final_id):
    particlenumber = total - remaining_length
    if particlenumber == 0:
        return
    elapsed = time.time() - starttime
    fraction_done = particlenumber / total
    est_tot = elapsed / fraction_done
    to_go = est_tot - elapsed
    end = datetime.datetime.now() + datetime.timedelta(seconds=to_go)

    print("Elapsed: {} minutes, To go: {} minutes".format(elapsed/60,
                                                          to_go/60))
    print("\n Predicted end time (particle {}): {}".format(final_id, end))


def run(particles, plot=False):
    # p_dataset = collections.defaultdict(list)
    particle_amount = len(particles)
    # initialise some empty things
    skipped = collections.Counter()
    try:
        starttime = time.time()
        while particles:
            # remove first praticle from list, use it.
            particle = particles.pop(0)
            try:
                runparticle(particle, plot=plot)
            except Skip_particle as error:
                print(log("Skip particle {} because:{}".format(particle.id,
                                                               str(error))))
                skipped[error.the_type] += 1
                continue

            except Exception as error:
                print(log("Exception occured at particle {}".format(
                    particle.id)))
                raise error

            finally:
                if not particle.id % 20:
                    timing(len(particles), starttime, particle_amount,
                           particles[-1].id)
                # log all data, also for particles that didn't work
                with open(datafile, 'ab') as file:
                    pickle.dump(particle.get_data(), file, protocol=2)
                log('Data added to pickle list for particle: ', particle.id)
                with open(datafile + '.condensed', 'ab') as file:
                    pickle.dump(particle.get_condensed_data(), file, protocol=2)
                log('Data added to condensed list for particle: ', particle.id)
                particle.del_data()
    
    # End of the loop over the particles
    finally:
        print('End of run. Data saved to {}'.format(datafile))
        for error, amount in skipped.items():
            print(log("{} particles skipped because: {}".format(amount,
                                                                error)))
