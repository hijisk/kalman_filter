import time as time
import collections
from mod.log import setlogvariables


# this file contains some global variables that are used by some parts of the
# code. For example, if a new log file will be created

# some code that helps time different parts of my program
added_timers = collections.defaultdict(float)
ongoing_timer = {}

# do we generate a new data and log file?
newfile = True
# the shape of the magnetic field (default is cubic spline ('cs'))
magfield_shape = 'cs'
# the stepsize in m at which Multiple Scattering and bremsstrahlung calculated
sim_stepsize = 1/100
# Up to this z value in m the track has to be simulated
detector_length = 9.5
# quickly change the amount of material for testing purposes
materialfactor = 1

# set some log and savefile variables
run_number, logfile, timestr = setlogvariables(newfile)
datafile = "data/" + run_number


def log(*args):
    with open(logfile, 'a') as log:
        string = "\n"
        for arg in args:
            string += str(arg)
        log.write(string)
    return string


log("magfield_ shape: ", magfield_shape, ", sim_stepsize: ", sim_stepsize,
    ", detector_length: ", detector_length)

def timer_start(text):
    ongoing_timer.update({text: time.clock()})


def timer_end(text):
    added_timers[text] += time.clock() - ongoing_timer.pop(text)

