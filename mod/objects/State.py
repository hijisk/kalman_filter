import mod.calculations as calc
import numpy as np

d = {'x': 0, 'y': 1, 'tx': 2, 'ty': 3, 'qp': 4}


class State:

    def __init__(self, state_vector, covariance_matrix):
        self.vec = State_Vector(state_vector)
        self.cov = np.array(covariance_matrix)

    def __getitem__(self, key):
        return getattr(self, key)

    def __repr__(self):
        np.set_printoptions(suppress=False, precision=1)
        string = ("State Vector:\n" +
                  "x:  [[" + str(self.vec.x) + "]\n" +
                  "y:   [" + str(self.vec.y) + "]\n" +
                  "tx:  [" + str(self.vec.tx) + "]\n" +
                  "ty:  [" + str(self.vec.ty) + "]\n" +
                  "q/p: [" + str(self.vec.qp) + "]]" +
                  "\nCovariance matrix:\n" + str(self.cov))
        #np.set_printoptions(edgeitems=3, infstr='inf',
        #                    linewidth=75, nanstr='nan', precision=8,
        #                    suppress=True, threshold=1000, formatter=None)
        return string

    def covariance(self, name):
        '''gets a diagonal element from the covariance matrix'''
        return self.cov[d[name]][d[name]]


class State_Vector(np.ndarray):

    def __new__(cls, inputarr):
        if type(inputarr) == State_Vector:
            return inputarr
        arr = np.array([inputarr]).T
        obj = np.asarray(arr, float).view(cls)
        if np.shape(arr) != (5, 1):
            print(np.shape(arr), arr)
            raise ValueError("State_Vector is wrong")
        return obj

    def __getattr__(self, name):
        # To access the individual parts in a readable manner where no matrices
        # are invovled.
        if name in d:
            return self.item(d[name])
        if name == 'q':
            return calc.charge(self)
        if name == 'p' or name == 'momentum':
            return calc.momentum(self)
        if name == 'eta':
            return calc.pseudorapidity(self)
        else:
            # a lot of functions in calc are useful for our state, this makes
            # them attributes
            return getattr(calc, name)(self)
