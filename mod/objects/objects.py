import numpy as np

xy = {'x': 0, 'y': 1}


class Coordinate:
    '''this represents either x or y, it can be represented as a
    0 or 1 or as x or y'''
    def __init__(self, coordinate):
        if type(coordinate) != str:
            self.num = int(coordinate)
        elif coordinate == 'x':
            self.num = 0
        elif coordinate == 'y':
            self.num = 1
        else:
            raise ValueError("Error in Coordinate class ")

    def __repr__(self):
        if self.num == 0:
            return 'x'
        if self.num == 1:
            return 'y'


class Measurement:
    def __init__(self, z_location, measurement_value, measurement_matrix,
                 measurement_error, real):
        self.z = z_location
        self.value = measurement_value
        self.mx = measurement_matrix
        self.var = measurement_error**2
        self.real = real


class MagnetLoss(Exception):
    '''The class used to express if the particle flies out of the
    detector or goes too slow and goes in a helical path'''
    def setinfo(self, z, state):
        self.z = z
        self.state = state
        return self


def residual_dict(state_vector, covariance_mx):
    obj = {'vec': np.array(state_vector), 'cov': np.array(covariance_mx)}
    return obj


class Skip_particle(Exception):
    '''gets raised if a particle does not get registered in any of the sub-
    detectors, nothing can be done then and we will continue'''
    def settype(self, the_type):
        self.the_type = the_type
        return self