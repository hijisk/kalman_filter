import numpy as np
import math
from mod.processes import calc_theta_0
from mod.globals import log
import mod.globals as glob
from .Taylor_approx import single_propagation
from mod.build.magfield import magnetic_field_strength
from mod.processes.magnetic import RK_propagation
import mod.objects as obj


class Kalman_propagation:
    '''this class handles the propagation function and noise matrix. '''
    stepsize = glob.sim_stepsize

    def __init__(self, z1, z2, reference_state, brem, ms):
        self.z1 = z1
        self.z2 = z2
        self.ref_state = reference_state
        self.step = math.copysign(self.stepsize, z2 - z1)
        self.noise = noise_matrix(reference_state, z1, z2, ms)

    def function(self, input_state):
        try:
            state, mx = RK_propagation(input_state, self.z1, self.z2, 'kalman')
        except obj.MagnetLoss:
            raise obj.Skip_particle('magnet loss in prediction').settype('magnet loss in prediction')

        self.matrix = mx
        return state

    def __repr__(self):
        s = "The propagation matrix:\n{}\nNoise Matrix\n{}".format(
            self.matrix, self.noise
        )
        return s


def noise_matrix(state, z1, z2, ms):
    '''adds the multiple scattering directional uncertainty'''
    del_z = z2 - z1
    assert type(ms) is bool
    if ms is False:
        Q = np.diagflat([1e-6, 1e-13, 1e-6, 1e-13, 1e-13]) * del_z*0
        return Q

    th0 = calc_theta_0(state, z1, z2)
    tx, ty = state.tx, state.ty
    Q_33 = (1 + tx**2) * (1 + tx**2 * ty**2) * th0**2
    Q_44 = (1 + ty**2) * (1 + tx**2 * ty**2) * th0**2
    Q_34 = tx * ty * (1 + tx**2 * ty**2) * th0**2

    def el(direction_component, power, divide_by):
        # To write the matrix elements short and structured.
        return direction_component * del_z**power / divide_by

    # collect all in the matrix
    Q = np.array([
        [el(Q_33, 2, 3), el(Q_34, 2, 3), el(Q_33, 1, 3), el(Q_34, 1, 2), 0],
        [el(Q_34, 2, 3), el(Q_44, 2, 3), el(Q_34, 1, 2), el(Q_44, 1, 2), 0],
        [el(Q_33, 1, 3), el(Q_34, 1, 2), Q_33,           Q_34,           0],
        [el(Q_34, 1, 2), el(Q_44, 1, 2), Q_34,           Q_44,           0],
        [0,              0,              0,              0,              0]
        ])
    return Q

