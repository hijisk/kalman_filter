import numpy as np
import mod.objects as obj


def calc_smoother_mx(filt_cov_mx, prop_rel, next_pred_cov_mx):
    # Calclutes the smoother matrix (6.21 v Tilburg)
    smoother_mx = filt_cov_mx @ prop_rel.T @ np.linalg.inv(next_pred_cov_mx)
    return smoother_mx


def smoothe_state_vector(smoother_mx, filt_state, next_smthed_state,
                         next_pred_state):
    # smoothes the state vector (6.19 v Tilburg)
    smthed_st_vec = (filt_state + smoother_mx
                     @ (next_smthed_state - next_pred_state))
    return smthed_st_vec


def smoothe_covariance_matrix(smoother_mx, filt_cov_mx,
                              next_smthed_cov_mx, next_pred_cov_mx):
    # smoothes the covariance matrix (6.20 v Tilburg)
    smthed_cov_mx = (filt_cov_mx + smoother_mx
                     @ (next_smthed_cov_mx - next_pred_cov_mx) @ smoother_mx.T)
    return smthed_cov_mx


def residual(meas, smthed_state_vec):
    # calculate the residual (6.22 v Tilburg)
    return meas.value - meas.mx @ smthed_state_vec


def residual_var(meas, smthed_cov_mx):
    # calculate the error on the residual (6.23 v Tilburg)
    return meas.var - meas.mx @ smthed_cov_mx @ meas.mx.T


def chi_squared(residual, residual_var):
    # calculate the chi squared contribution (6.24 v Tilburg)
    return residual @ np.linalg.inv(residual_var) @ residual.T


def smoothestep(filt_state, next_smthed_state, next_pred_state, prop_fn,
                measurement=None):
    # does one smooth step
    smoother_mx = calc_smoother_mx(filt_state['cov'], prop_fn.matrix,
                                   next_pred_state['cov'])

    smthed_st_vec = smoothe_state_vector(
        smoother_mx, filt_state['vec'], next_smthed_state['vec'],
        next_pred_state['vec'])

    smthed_cov_mx = smoothe_covariance_matrix(
        smoother_mx, filt_state['cov'], next_smthed_state['cov'],
        next_pred_state['cov'])
    
    smthed_state = obj.State(smthed_st_vec, smthed_cov_mx)

    if measurement is None:
        return smthed_state

    smthed_residual = residual(measurement, smthed_st_vec)
    smthed_residual_var = residual_var(measurement, smthed_cov_mx)
    smthed_chi_sq = chi_squared(smthed_residual, smthed_residual_var)

    

    return smthed_state, smthed_chi_sq


def smoother(States_info):
    # States_info is the list where for every state all the information is
    # listed. This list will be updated by this function to contiain the
    # smoothed states as well. prop_fn is the funtion return a propagation
    # matrix and measurement noise after location input.

    # the filtered states for the last state are also the smoothed states.
    States_info.add_smooth(-1, States_info[-1]['filt'],
                           States_info[-1]['filt_chi_sq'])

    # run a for loop backwards over the states, starting with the second to
    # last one.

    for k in range(-2, -len(States_info), -1):
        # run the smoother algorithms for the state, the smoothed state and
        # covariance matrix are returned.
        prev = States_info[k]
        this = States_info[k+1]
        smoothed_state, chi_sq = smoothestep(
            prev['filt'],
            this['smthed'],
            this['pred'],
            this['prop_mx'],
            prev['meas']
            )

        States_info.add_smooth(k, smoothed_state, chi_sq)
        # Put the smoothed state and covariance matrix in the list at the right
        # point.
    # the first node is not a measurement, so we smooth without chi squared
    # calculations
    States_info[0].update({'smthed': smoothestep(
            prev['filt'],
            this['smthed'],
            this['pred'],
            this['prop_mx'])})

    return States_info
