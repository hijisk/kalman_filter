# this file contains all the functions that do the
# linear algebra for the filter step

import numpy as np


def state_filter(pred_state, gain_mx, pred_residual):
    # calculates the Filtered state (6.13 v Tilburg)
    filtered_state = pred_state + gain_mx @ pred_residual
    return filtered_state


def state_cov_filter(gain_mx, meas_mx, cov_pred):
    # calculates the filtered covariance matrix (6.14 v Tilburg)
    filtered_cov = (np.identity(5) - gain_mx @ meas_mx) @ cov_pred
    return filtered_cov


def gain_mx_calc(cov_mx, meas_mx, pred_res_cov):
    # calculates the gain matrix (6.15 v Tilburg)
    gain_mx = cov_mx @ meas_mx.T @ np.linalg.inv(pred_res_cov)
    return gain_mx


def residual_filter(meas_mx, gain_mx, residual_pred):
    # calculates the filtered residual (6.16 v Tilburg)
    filtered_res = (1 - meas_mx @ gain_mx) @ residual_pred
    return filtered_res


def residual_cov_filter(meas_cov, meas_mx, filt_cov):
    # calculates the filtered residual covariance matrix (6.17 v Tilburg)
    filt_res_cov = meas_cov - meas_mx @ filt_cov @ meas_mx.T
    return filt_res_cov


def chi_sq_calc(residual, residual_variance):
    # calculate the chi squared contribution (6.12 v Tilburg)
    chi_sq = residual @ np.linalg.inv(residual_variance) @ residual.T
    return chi_sq
