from math import sqrt
import numpy as np
from ..globals import log
from ..objects import Skip_particle


def single_propagation(state_vector, Bfield, stepsize):
    '''This generates a propagation matrix, calculated by taking the partial
    derivatives of the propagation function als given in
    processes.magnetic.py. The partial derivatives are calculated from taylor
    approximations. The calculation was done with mathematica and can be found
    in helices.nb'''
    
    try:
        # Easier use of the variables
        B = Bfield
        x0, x1, x2, x3, x4 = state_vector.flatten()
        h = stepsize

        # Initialise the propagation matrix with all values equal to zero
        F = np.zeros((5, 5))

        # Most diagonal values are simple:
        F[0, 0] = 1
        F[1, 1] = 1
        F[3, 3] = 1
        F[4, 4] = 1

        # The calculation of y in terms of ty is simple:
        F[1, 3] = h

        # Below we calculate F[0,2], the influence of tx on the x value

        F02_order_1 = h
        F02_order_2 = - h**2 * B * x4 * x2 * (3 + 3*x2**2 + 2*x3**2) / (
                            2 * sqrt(1 + x2**2 + x3**2))

        F02_order_3 = (1/2) * h**3 * (B * x4)**2 * (1 + 5*x2**4 + x3**2 +
                                                    3*x2**2 * (2 + x3**2))

        F[0, 2] = F02_order_1 + F02_order_2 + F02_order_3

        # Below we calculate F[0,3], the influence of ty on the x-value

        F03_order_2 = - h**2 * B * x4 * x3 * (1 + x2**2) / (
                            2 * sqrt((1 + x2**2 + x3**2)))

        F03_order_3 = h**3 * (B * x4)**2 * x2 * (1 + x2**2) * x3

        F[0, 3] = F03_order_2 + F03_order_3

        # Below we calculate F[0,4], the influence of qp on the x-value
        F04_order_2 = - (1/2) * h**2 * B * (1 + x2**2) * sqrt(1 + x2**2 +
                                                              x3**2)

        F04_order_3 = h**3 * B**2 * x4 * (1 + x2**2 + x3**2) * (1 + x2**2)

        F[0, 4] = F04_order_2 + F04_order_3

        # Here we calculate F[2, 2], the relation between prev tx and new tx

        F22_order_0 = 1
        F22_order_1 = -h * B * x4 * x2 * (3 + 3*x2**2 + 2*x3**2)/(
                        sqrt((1 + x2**2 + x3**2)))

        F22_order_2 = (3/2) * (h * B * x4)**2 * (1 + 5*x2**4 + x3**2 +
                                                 3*x2**2 * (2 + x3**2))

        F[2, 2] = F22_order_0 + F22_order_1 + F22_order_2

        # Here we calculate F[2,3]: The relation between ty and new tx
        F23_order_1 = -h * B * x4 * x3 * (1 + x2**2)/(
                            sqrt(1 + x2**2 + x3**2))

        F23_order_2 = 3 * (h * B * x4)**2 * x2 * (1 + x2**2) * x3

        F[2, 3] = F23_order_1 + F23_order_2

        # Here we calculate F[2,4]: The relation between qp and new tx
        F24_order_1 = -h * B * (1 + x2**2) * sqrt(1 + x2**2 + x3**2)
        F24_order_2 = 3 * (h * B)**2 * x4 * x2 * (1 + x2**2) * (1 + x2**2 +
                                                                x3**2)

        F[2, 4] = F24_order_1 + F24_order_2

        #state_vector = F @ state_vector
        return F

    except(FloatingPointError) as e:
        st = ("Kak er is een FloatingPointError, hoe fix, wtf is er aan de hand." +
              "vec = {}, B = {}".format(state_vector, B))
        print(st)
        raise e
'''
text = 0

if (state_vector.tx * x2) < 0:
    text = "tx changes sign at z = {}".format(z)
if z % 1000 == 0 or z == 4697:
    text = "z = {}".format(z)

if text != 0:
    log(text)
    log('F[0,2]: \nO(h): ', F02_order_1, '\nO(h^2): ',
        F02_order_2, "\nO(h^3): ", F02_order_3)
    log('F[0,3]: O(h^2): ', F03_order_2,
        '\nO(h^3): ', F03_order_3)
    log('F[0,4]: O(h^2): ', F04_order_2,
        '\nO(h^3): ', F04_order_3)
    log('F[2,2]: \nO(h^0): ', F22_order_0, '\nO(h^1): ', F22_order_1,
        '\nO(h^2): ', F22_order_2)
    log("F[2,3] \nO(h^1): ", F23_order_1, "\nO(h^2): ", F23_order_2)
    log("F[2,4] \nO(h^1): ", F24_order_1, "\nO(h^2): ", F24_order_2)'''
