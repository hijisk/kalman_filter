# This file contains the main functions, the steps used in the kalman filter

from ..kalman import prediction as pred
from ..kalman import filter as filt
from mod.objects import State, residual_dict

# ====================================================
# PREDICTION


def prediction(filtered_state, propagation, measurement):
    # This function serves to do the prediction step for the kalman filter.
    #
    # We do a state prediction
    predicted_state_vec = pred.state_prediction(propagation.function,
                                                filtered_state['vec'])
    # We estimate our error on this prediction
    predicted_state_covariance_mx = \
        pred.cov_prediction(
            propagation.matrix,
            filtered_state['cov'],
            propagation.noise)

    # combine the two in a state object
    pred_state = State(predicted_state_vec,
                       predicted_state_covariance_mx)

    #
    # we find the residual with regard to the actual measurement
    predicted_residual_vec = pred.residual_prediction(
                                measurement.value,
                                measurement.mx,
                                pred_state['vec'])
    #
    # we find the covariance matrix of this residual
    predicted_residual_cov = pred.residual_cov_prediction(
            measurement.var,
            measurement.mx,
            pred_state['cov'])

    pred_residual = residual_dict(predicted_residual_vec,
                                  predicted_residual_cov)

    #
    return pred_state, pred_residual,


# =======================================================
# FILTER

def filter(pred_state, pred_residual, measurement):
    # This function calls the filter step of the kalman filter.
    # It uses the filter.py module

    # first we find the gain matrix
    gain_mx = filt.gain_mx_calc(pred_state['cov'], measurement.mx,
                                pred_residual['cov'])
    # Then we filter the state
    filtered_state_vec = filt.state_filter(pred_state['vec'], gain_mx,
                                           pred_residual['vec'])
    # we get the filtered covariance matrix
    filtered_state_covariance_mx = filt.state_cov_filter(
        gain_mx, measurement.mx, pred_state['cov'])

    filt_state = State(filtered_state_vec,
                       filtered_state_covariance_mx)

    # calculate the filtered resiudal
    filtered_residual_vec = filt.residual_filter(measurement.mx, gain_mx,
                                                 pred_residual['vec'])

    # calculate the filtered resiudal's covariance
    filtered_residual_covariance = filt.residual_cov_filter(
        measurement.var, measurement.mx, filt_state['cov'])

    filt_residual = residual_dict(filtered_residual_vec,
                                  filtered_residual_covariance)

    filt_chi_squared = filt.chi_sq_calc(filtered_residual_vec,
                                        filtered_residual_covariance)

    return filt_state, filt_residual, filt_chi_squared, gain_mx
