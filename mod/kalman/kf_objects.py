class Node(dict):
    '''Stores the kalman filter information at a node.
    It is an expanded dict.'''
    def __lt__(self, other):
        return self['z'] < other['z']


class Track_fit:
    # The Track_fit is constructed from a Seed State, after that nodes will be
    # added. The chi_squared value is added when the nodes are updated with the
    # smoothed states. The functionality to determine the highest chi_squared
    # node is used for refitting.
    def __init__(self, seed_state):
        first_node = Node({"filt": seed_state, 'z': 0})
        self.internal_list = [first_node]
        self.total_chi_sq = 0
        self.seed_state = self[0]['filt']
        self.highest_chi_sq = 0
    
    def append(self, node):
        self.internal_list.append(node)
    
    def add_smooth(self, iter, state, chi_sq):
        self.internal_list[iter].update({'smthed': state, 'chi_sq': chi_sq})
        self.total_chi_sq += chi_sq[0][0]
        if chi_sq[0][0] > self.highest_chi_sq:
            self.highest_chi_sq = chi_sq[0][0]
            self.outlier_z = self.internal_list[iter]['z']

    def __getitem__(self, iter):
        return self.internal_list[iter]

    def __len__(self):
        return len(self.internal_list)

    def sort(self):
        self.internal_list.sort()

    def process(self):
        self.sort()

    def zero(self):
        return self[0]

    def need_refit(self):
        if self.highest_chi_sq > 9:
            return True
        else:
            return False
