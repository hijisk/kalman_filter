# this file contains all the functions that do the linear algebra for the
# prediction step


def state_prediction(prop_function, filt_state):
    # predicts the state vector (6.8 Thesis v Tilburg)
    pred_state = prop_function(filt_state)
    return pred_state


def cov_prediction(prop_mx, filt_cov_mx, process_noise_mx):
    # updates the covariance prediction matrix (6.9 Thesis v Tilburg)
    pred_cov_mx = (prop_mx @ filt_cov_mx @ prop_mx.T) + process_noise_mx
    return pred_cov_mx


def residual_prediction(measurement, meas_mx, pred_state):
    # predicts the residual (6.10 Thesis v Tilburg)
    pred_residual = measurement - meas_mx @ pred_state
    return pred_residual


def residual_cov_prediction(meas_var, meas_mx, pred_state_cov_mx):
    # covariance of the predicted residual (6.11 thes v Tilburg)
    pred_residual_cov = meas_var + (meas_mx @ pred_state_cov_mx @ meas_mx.T)
    return pred_residual_cov
