from .iterations import filter, prediction
from .smoother import smoother
from .kf_objects import Node, Track_fit
from .propagation import Kalman_propagation
from mod.objects import Skip_particle


def Runkf(starting_state, measurements, brem, ms, refit=0):
    track_fit = Track_fit(starting_state)
    # the info from the steps is initialised with first state

    step_info = track_fit[0]  # initialise so we go from the first node
    old_z = step_info['z']

    for measurement in measurements:
        # We need to know the propagation matrix to do the kalman filter
        new_z = measurement.z

        # the propagation relation is calculated from the locations
        propagation = Kalman_propagation(
            old_z, new_z, step_info['filt'].vec, brem,
            ms)

        # one_step returns the next filtered state with the previous as input
        step_info = one_step(step_info['filt'], propagation, measurement)
        # the filtered state is appended to the list
        track_fit.append(step_info)
        old_z = new_z
    # smoother just uses data from the steps. We don't have to return because
    # smoother updates track_fit
    smoother(track_fit)
    if track_fit.need_refit():
        if refit == 5:
            return track_fit
        print('refitting')
        delete_z = track_fit.outlier_z
        refit_meas = [m for m in measurements if m.z != delete_z]
        return Runkf(starting_state, refit_meas, brem, ms, refit=refit+1)
    return track_fit


def one_step(prev_state, propagation, measurement):  # one step is all it takes
    # one step consists of running a prediction algorithm

    predicted_state, predicted_residual = prediction(prev_state, propagation,
                                                     measurement)

    # and a filter algorithm
    filtered_state, filtered_residual, filt_chi_squared, gain_mx = filter(
        predicted_state, predicted_residual, measurement)

    # add the z location as well
    z = measurement.z
    # a dict to store everything
    node_info = Node({
        "pred": predicted_state, "pred_res": predicted_residual,
        "filt": filtered_state,  "filt_res": filtered_residual, 'z': z,
        'filt_chi_sq': filt_chi_squared, 'prop_mx': propagation,
        'gain_mx': gain_mx, 'meas': measurement})

    return node_info
