# file contains functions able to calculate stuff (state properties)
import math

# this defines the charge of the electron
e = 1.60217662e-19
m = 9.1093837015e-31
c = 299792458
c2 = c*c


def charge(state):
    charge = math.copysign(e, state[4][0])
    return charge


def momentum(state):
    p = charge(state) / state[4][0]
    return p


def momentumGeV(state):
    return momentum(state) / 5.344e-19


def p_z_squared(state):
    # calculates the p_z_squared of a given state
    p_z_squared = state.p**2 / (state.tx**2 + state.ty**2 + 1)
    return p_z_squared


def p_z(state):
    p_z = math.sqrt(p_z_squared(state))
    return p_z


def pseudorapidity(state):
    eta = math.atanh(p_z(state) / momentum(state))
    return eta


def phi(state):
    phi = math.atan2(state.tx, state.ty)
    return phi


def p_x_squared(state):
    return state.p_z_squared * state.tx**2


def p_perp(state):
    return math.sqrt((1 + state.tx**2) * state.p_z_squared)


def gamma(state):
    return 1/math.sqrt(1 - state.beta**2)


def beta(state):
    beta = math.sqrt(1 / ((m * c / state.p)**2 + 1))
    return beta


def lengthfactor(state):
    '''this function calculates the actual length of the trajectory if del_z=1
    for the specific tx and ty'''
    length = math.sqrt(state.tx**2 + state.ty**2 + 1)
    return length


def energy(state):
    return math.sqrt(state.p**2 * c2 + (m * c2)**2)


def energyMeV(state):
    #SI momentum units to MeV/c
    return state.energy * 6.242e12


def momentum_from_energy(E):
    p = math.sqrt(E**2/c2 - m**2 * c2)
    return p
