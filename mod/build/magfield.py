'''originally by Maarten van Veghel some edits by Jasper Somsen'''
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import CubicSpline
from mod.globals import magfield_shape
# create magnetic field funcion with spline
# z [m] versus By [T]
Bfield_plotguess = [(0,  0), (0.5,  0.02), (1.0, 0.05), (1.5, 0.055),
                    (2.0, 0.07), (2.5, 0.15), (2.75, 0.25), (3.25, 0.5),
                    (3.75, 0.75), (4.0, 0.9), (4.8, 1.05), (6.0, 0.85),
                    (7.0, 0.5), (8.0, 0.25), (9.0, 0.125), (10.0, 0.05),
                    (12, 0)]

Bfield = np.array(Bfield_plotguess)
zmin = Bfield[0][0]
zmax = Bfield[-1][0]

# creat a cubic spline according to knots x, y with derivatives at zero for
# start and end ('clamped')
cs = CubicSpline(Bfield[:, 0], Bfield[:, 1], bc_type='clamped')
xs = np.arange(zmin, zmax, (zmax-zmin)/120.)


def simple_magnetic_field_strength(z_location):
    return 1 if 4 < z_location < 6 else 0


def cs_magnetic_field_strength(z_location):
    field_strength = cs(z_location)
    return field_strength


def cs_bending_power(zmin, zmax):
    power = cs.integrate(zmin, zmax)
    return power


def simple_bending_power(zmin, zmax):
    def integral(z):
        return simple_magnetic_field_strength(z) * (z-4) if z < 6 else 2
    return integral(zmax) - integral(zmin)


if magfield_shape == "cs":
    bending_power = cs_bending_power
    magnetic_field_strength = cs_magnetic_field_strength

if magfield_shape == "simple":
    bending_power = simple_bending_power
    magnetic_field_strength = simple_magnetic_field_strength

# calculate bending power
bp_to_9 = bending_power(0, 9)
midpoints = cs.antiderivative().solve(bp_to_9 / 2)
# can have multiple solutions, we need to get the right one
for point in midpoints:
    if 3 < point < 7:
        midpoint = point
    else:
        midpoint = 5.5


def plot():
    fig,  ax = plt.subplots(figsize=(6, 4))
    ax.plot(Bfield[:, 0],  Bfield[:, 1],  'o',  label='guess from plot')
    ax.plot(xs,  cs(xs),  label='spline')

    plt.title('magnetic field approximation of LHCb')
    plt.xlabel(r'$z$ [m]')
    plt.ylabel(r'$B_y$ [T]')
    plt.xlim(0.00, 12.0)
    plt.ylim(0.00, 1.30)
    plt.text(0.5, 1.05,
             ('from spline:\n'+r'$\int | d\vec{l} \times \vec{B} |_{x}$ = '
              + '{0:.1f}'.format(magint)))
    plt.legend(loc='upper right')
    plt.savefig("output/magfield.png")
