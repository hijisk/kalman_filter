'''This file contains the functions that handle measurement generation. To
the form that the KF uses and also the more readable form.'''
import mod.globals as glob
from mod.objects import Measurement, Skip_particle
import numpy as np


def generate_measurements(particle, detector):
    '''Generate the measurements from the simulated particle
    '''
    # load particle states
    states = particle.get_track()
    kf_measurements, x_meas, y_meas = [], [], []  # initialise empty lists
    for subdet in detector:
        # note that parameters of detectors are saved in mm, while everything
        # else is done in m. For all subdetectors in order generate a
        # measurement at every subdetector

        z = subdet.z/1000   # z is the z-location of our subdetector in m

        # z_step is the z-location in amount of steps
        # for example it's the z-loc in mm if the stepsize is 1/1000
        z_step = int(round(z / glob.sim_stepsize))
        if z_step > len(states):  # if the particle didn't get this far
            break
        
        # the code below checks that detector.z is the same as the z of
        # the state vector.
        if abs(states[z_step][1] - z) > glob.sim_stepsize/100:
            print('Warning, Detector does not align with simulation')
            print(states[z_step][1], z)
        
        if subdet.checkhit(states[z_step][0]) is False:
            # only add measurements if particle goes trough the right part of
            # the detector plane.
            continue
        
        # after this we are certain that there will be a hit in this
        # subdetector for this particle, and the error will be determined
        # correctly for the Tracking stations, as it depends on the location.
        matrix = subdet.meas_mx
        # this is the error in m:
        error = subdet.error(matrix, states[z_step][0])/1000

        # track the amount of hits for each subdetector
        particle.subdetector_hits[subdet.sdtype] += 1
        # we generate a random distubrance to the measurement
        # according to detector precision
        meas_deviation = np.random.normal(0, error)

        # get the measurement value by multiplying with the measurement matrix
        # also add the variance
        meas_value = (subdet.meas_mx @ states[z_step][0]) + meas_deviation

        # The measurement is stored as a dict with a z-location, a measurement
        # value, measurement matrix and variance information

        measurement = Measurement(z, meas_value[0], matrix, error,
                                  states[z_step][0])

        # and added to the list
        kf_measurements.append(measurement)
        particle.hits_amount += 1

        # now we want to generate two more lists, all the x and y measurements
        # in separate lists. This is more easy to handle/understand for
        # applications other than kalman filter. (such as plotting)
        [x, y] = matrix.flatten()[:2] * meas_value[0]
        [x_err, y_err] = matrix.flatten()[:2] * error
        if x != 0:
            x_meas.append({'z': z, 'value': x, 'error': x_err})
        if y != 0:
            y_meas.append({'z': z, 'value': y, 'error': y_err})


    return kf_measurements, [x_meas, y_meas]
