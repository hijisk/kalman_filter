import numpy as np
from mod.globals import materialfactor
#               Velo, velo-RICH,  RICH,   TT, magnet, t stations
rangelist = [0, .830,       .978, 2.250, 2.750, 7.600,      9.300, 11]
thicklist = [.162,         .068,  .095,  .051,  .053,       .178, .178]

thicklist = np.array(thicklist) * materialfactor

def x0(z):
    for i in range(len(thicklist)):
        if rangelist[i] <= z < rangelist[i+1]:
            return (rangelist[i+1] - rangelist[i]) / thicklist[i]
    else:
        return 1.7/.178


def x0_int(z1, z2):
    # integrates the radiation length over distance each mm
    accumulate = 0

    for z in np.arange(z1, z2, 0.001):
        accumulate += 0.001/x0(z)
    return accumulate
