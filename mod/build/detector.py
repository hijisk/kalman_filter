# This file contains the functions that determine the detectors
import numpy as np
from mod.globals import log

# Define the sort of measurement matrices that exist
mmx_x = np.array([[1, 0, 0, 0, 0]])
mmx_y = np.array([[0, 1, 0, 0, 0]])


def Mmx(version):
    '''returns a measurement matrix as a 2d numpy array. The specific
    parametrisation gets chosen from the dict below.
    '''
    mmx_dict = {'x': mmx_x, 'y': mmx_y}

    mx = mmx_dict[version]
    return mx


class Subdetector:
    '''The main subdetector class, contains some overall class
    functions relevant for all the subdetectors'''
    def __init__(self, z_location, meas_mx):
        self.z = z_location
        self.meas_mx = Mmx(meas_mx)

    def __lt__(self, other):
        return self.z < other.z

    def __repr__(self):
        return self.sdtype + ' subdetector at z=' + str(self.z) + 'mm'

    def error(self, Mmx, state):
        if self.sdtype == "T_station":
            return self.selecterror(Mmx, state[0][0], state[1][0])
        if Mmx is mmx_x:
            return self.error_x
        if Mmx is mmx_y:
            return self.error_y

    def sdcheckhit(self, x, y):
        print(x)
        return True

    hit_chance = 1
    error_x = .05
    error_y = .05
    sdtype = 'base_class'

    def checkhit(self, state):
        # self.sdcheckhit(state[0][0], state[1][0])
        # functionality is built in, but the model behaves erratic when this is
        # actually used.
        return True


class VELO(Subdetector):

    inner_radius = 8    # measurements are always 8 mm from beam
    outer_radius = 42   # outer radius of VELO detector in mm
    error_x = .015
    error_y = .015    # the error in mm
    sdtype = "VELO"
    in_r_sq = (inner_radius/1000)**2
    out_r_sq = (outer_radius/1000)**2

    def sdcheckhit(self, x, y):
        r_sq = x**2 + y**2
        if self.in_r_sq < r_sq < self.out_r_sq:
            return True
        else:
            return False


class TT(Subdetector):
    error_x = 0.050     # the error in mm
    error_y = 0.5     # the error in mm
    sdtype = "TT"

    def sdcheckhit(self, x, y):
        return True


class T_station(Subdetector):
    errordict = {'OT_error_x': 0.2, 'OT_error_y': 2,
                 'IT_error_x': 0.05, 'IT_error_y': 0.5}

    outer_x = 3
    outer_y = 2.5     # dimensions in m
    sdtype = "T_station"

    def selecterror(self, Mmx, x, y):
        # depending on IT or OT, the error is different. The IT covers about
        # an area of 1.20x0.40 m around the beam axis, the wide part in x.
        if abs(x) < .60 and abs(y) < .20:
            string = 'IT_error_'
        else:
            string = 'OT_error_'
        if Mmx is mmx_x:
            return self.errordict[string+'x']
        if Mmx is mmx_y:
            return self.errordict[string+'y']

    def sdcheckhit(self, x, y):
        if abs(x) < self.outer_x and abs(y) < self.outer_y:
            return True
        else:
            return False


def add_subdetectors(subdetector_class, location_list, meas_mx_list):
    detlist = []
    for (z, mx) in zip(location_list, meas_mx_list):
        detlist.append(subdetector_class(z, mx))
    log(len(location_list), " " + subdetector_class.sdtype +
        "-type subdetectors added at z-locations: ", location_list)
    return detlist


def testdet():
    dlist = []
    List = [[500, 1000, 1500, 2000, 2500, 3000, 3500, 6500, 7000, 7500, 8000,
             8500, 9000, 9500, 750, 1750, 3750, 6750, 7750, 8750],
            ["x", "x",  "x",  "x",  "x",  "x",  "x",  "x",  "x",  "x",  "x",
             "x",  "x",  "x", "y",  "y",  "y",  "y",  "y",  "y"]]
    # here subdetectors get added to above list
    dlist.extend(add_subdetectors(Subdetector, List[0], List[1]))
    # sort the list to be sure
    dlist.sort()
    print('Test detector built, this is a much more simplified detector')
    return dlist


def detector_main():
    '''builds the 'detector': a list of subdetector objects, parameters are all
    in this file'''
    VELO_list = [
        [20,   50,  80, 110, 140, 170, 200, 230, 260, 290, 450, 600, 650, 700,
         750],
        ['x', 'y', 'x', 'y', 'x', 'y', 'x', 'y', 'x', 'y', 'x', 'y', 'x', 'y',
         'x']
        ]

    TT_list = [
        [2330, 2370, 2600, 2640],
        ['x', 'y', 'x', 'y']
        ]

    T_station_list = [
        [7860, 7870, 7910, 7920, 7980, 7990, 8030, 8040, 8540, 8550, 8590,
         8600, 8660, 8670, 8710, 8720, 9230, 9240, 9280, 9290, 9350, 9360,
         9400, 9410],
        ['x',  'y',  'x',  'y',  'x',  'y',  'x',  'y',  'x',  'y',  'x',
         'y',  'x',  'y',  'x',  'y',  'x',  'y',  'x',  'y',  'x',  'y',
         'x',  'y']
        ]

    # initialise the list of subdetectors
    dlist = []

    # here subdetectors get added to above list
    dlist.extend(add_subdetectors(VELO, VELO_list[0], VELO_list[1]))
    dlist.extend(add_subdetectors(TT, TT_list[0], TT_list[1]))
    dlist.extend(add_subdetectors(T_station, T_station_list[0],
                                  T_station_list[1]))

    # sort the list to be sure
    dlist.sort()

    # END OF SUBDETECTOR LIST
    # ======================================
    print(log("The detector is built."))
    return dlist


detector = detector_main()
