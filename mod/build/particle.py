# this file contains functionality of generating a series of measurements for
# a particle going through the LHCb detector to use in the model
import numpy as np
import mod.globals as glob
from mod.globals import log, run_number
from mod.processes import multiple_scattering, RK_propagation, brems_E_loss
import mod.objects as obj
from mod.kalman import Runkf
import math
import collections
from mod.build.detector import detector


def particlegen(start, stop, step, binsize,
                brem, ms, charge='alternating'):

    momentumlist = []
    for i in np.arange(start, stop, step):
        momentumlist.extend([i]*binsize)
    particle_amount = len(momentumlist)

    print(log('''Generating {} particles, from {} to {} GeV. Brem = {}, ms = {}
'''.format(particle_amount, start, stop, brem, ms)))

    plist = []
    for i in range(particle_amount):
        eta = np.random.uniform(low=2, high=5)
        phi = np.random.uniform(low=-1*math.pi, high=math.pi)
        p = momentumlist[i]
        q = -1 if i % 2 else 1
        particle = Particle(i, to_state(eta, phi, p, q), brem, ms)
        plist.append(particle)
    return plist


def to_state(eta, phi, p_in_GeV, q_pos_or_neg):
    GeV = 5.344e-19  # defined so that 1*GeV is 1 GeV written in kg*m/s
    e = 1.60217662e-19

    q = math.copysign(e, q_pos_or_neg)
    p = p_in_GeV * GeV

    theta = 2 * math.atan(math.exp(-eta))
    tx = math.tan(theta * math.cos(phi))
    ty = math.tan(theta * math.sin(phi))
    qp = q/p
    x, y = .001, .001  # np.random.choice([.001, -.001], size=2)
    return obj.State_Vector([x, y, tx, ty, qp])


class Particle:
    def __init__(self, id, state_vector, brem, ms):
        self.id = id
        self.run = run_number
        self.savepath = "trackdata/track_" + str(self.id) + ".npy"
        self.primary_vertex = state_vector
        self.subdetector_hits = collections.Counter()
        self.hits_amount = 0
        self.fitted = False
        self.brem = brem
        self.multiple_scattering = ms
        self.track_exists = False

    def __getattr__(self, name):
        if name == "rec":
            return self.track_fit[0]["smthed"]
        if name == "real":
            return self.primary_vertex
        else:
            raise Exception("invalid Particle attribute")

    def save_track(self, track):
        self.simulated_track = track
        self.track_exists = True

    def get_track(self):
        return self.simulated_track

    def del_data(self):
        if self.track_exists:
            del self.simulated_track
        if self.fitted:
            del self.track_fit
        del self.subdetector_hits

    def fit_from(self, seed_state, measurements):
        self.track_fit = Runkf(seed_state, measurements, brem=self.brem,
                               ms=self.multiple_scattering)
        self.fitted = True

    def simulate(self):
        log('\nSimulating particle')
        length = glob.detector_length
        statevec = self.primary_vertex

        initialised_track = [(statevec, 0.)]  # add fist state to list
        track = tracksim(0, length, initialised_track,
                         self.brem, self.multiple_scattering)
        self.save_track(track)

    def get_data(self):
        data = [self.id, self.subdetector_hits, self.brem, self.real,
                self.fitted]

        if self.fitted is True:
            data.extend([self.rec.vec, self.rec.cov,
                         self.track_fit.seed_state.vec,
                         self.track_fit.total_chi_sq])

        else:
            data.extend([None, None, None, None])
        return data

    def get_condensed_data(self):
        real_p = self.real.momentumGeV
        data = [self.id, real_p, None]
        if self.fitted == True:
            rec_p = self.rec.vec.momentumGeV
            data[2] = (rec_p - real_p)/real_p
        return data

    def logvars(self, inorout='input'):
        if inorout == 'input':
            vec = self.real
        if inorout == 'output':
            vec = self.rec.vec
        log('''Particle {}:\neta {}, phi {},\np {}, q {},
brem = {}'''.format(self.id, vec.eta, vec.phi,
                    vec.momentumGeV, vec.q, self.brem),
            "\n{} State:\n{}".format(inorout, vec))


def tracksim(start, stop, initialised_track, brem, ms):
    stepsize = glob.sim_stepsize
    track = initialised_track  # This is an initialised list
    z1 = start
    # loop over all steps
    for next_z in np.arange(start+stepsize, stop, stepsize):
        z2 = next_z
        # the new state is calculated using the runge kutta method.
        try:
            state = RK_propagation(track[-1][0], z1, z2, 'sim')
        except obj.MagnetLoss:
            print('simulation magnetic loss at z=', z2)
            return track
        if ms:
            state = multiple_scattering(state, z1, z2)
        if brem:
            state = brems_E_loss(state, z1, z2)
        z1 = next_z
        # the track saves the state vector and the z value.
        track.append((state, next_z))
    return track


    '''
    for z_step in range(0, round(length/stepsize)):
        # example: if we have a 20m detector and a 1/1000 stepsize
        # we need to add 20.000 statevectors to the 'track' list.
        # This for loop loops over those track
        z = start + (z_step + 1) * stepsize  # z-location in m
        prev_state = track[z_step]
        try:
            state = gyration_step(prev_state, z-stepsize, z)
        except obj.MagnetLoss:
            print(log("Particle is lost at ", z, " meter due to magnetic " +
                      "bending. ", "the state vector was:\n", state))
            return track
        if ms:
            state = multiple_scattering(state, z-stepsize, z)
        if brem:
            state = brems_E_loss(state, z-stepsize, z)

        # The current state is added to the list of track
        track.append(state)
    return track
    '''