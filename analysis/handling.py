import pickle
import pandas as pd
import numpy as np
import math


def list_of_dicts_to_df(particle):
    dataframe = pd.DataFrame.from_records(particle['track'])
    nwdataframe = pd.DataFrame()
    nwdataframe['chi_sq'] = dataframe['chi_squared']
    for attr in ['x', 'y', 'tx', 'ty', 'qp', 'momentumGeV']:
        nwdataframe['real_{}'.format(attr)] = dataframe['meas'].map(
            lambda meas: np.nan if meas is np.nan else getattr(meas.real, attr))

    nwdataframe['real_p'] = nwdataframe['real_momentumGeV']

    for text in ['filt', 'pred', 'smthed']:
        for attr in ['x', 'y', 'tx', 'ty', 'qp', 'momentumGeV']:
            #extract all state values
            nwdataframe['{}_{}'.format(text, attr)] = dataframe[text].map(
                lambda state: np.nan if type(state) is float else getattr(state['vec'], attr))
        #make p the momentum in GeV
        nwdataframe['{}_p'.format(text)] = nwdataframe['{}_momentumGeV'.format(text)]
        #covariance for p
        nwdataframe['{}_cov[p,p]'.format(text)] = dataframe[text].map(
            lambda state: np.nan if type(state) is float else
            state.cov[4, 4]/particle['real_qp']**2*particle['real_p']**2)
        #extract other covariances
        for attr in ['x', 'y', 'tx', 'ty', 'qp']:
            nwdataframe['{0}_cov[{1},{1}]'.format(text, attr)] = dataframe[text].map(
                lambda state: np.nan if type(state) is float else
                state.covariance(attr))
        #get the differences
        for attr in ['x', 'y', 'tx', 'ty', 'p', 'qp']:
            nwdataframe['{}_del_{}'.format(text, attr)] = nwdataframe[
                ['{}_{}'.format(text, attr), 'real_{}'.format(attr)]].apply(
                lambda row: np.nan if row is np.nan else
                row['{}_{}'.format(text, attr)] - row['real_{}'.format(attr)],
                axis=1)
            
            nwdataframe['{0}_del_{1}_over_cov_{1}'.format(text, attr)] = \
                nwdataframe[['{}_del_{}'.format(text, attr),
                           '{0}_cov[{1},{1}]'.format(text, attr)]].apply(
                lambda value: np.nan if value is np.nan else (
                    value['{}_del_{}'.format(text, attr)]/value['{0}_cov[{1},{1}]'.format(text, attr)]
                ), axis=1)

    nwdataframe['gain_qp'] = dataframe['gain_mx'].map(
        lambda mx: np.nan if type(mx) is float else mx[4][0])
    nwdataframe['z'] = dataframe['z']
    nwdataframe['ones'] = dataframe['z'].map(lambda z: 1) #add to make averaging easy
    return nwdataframe


def average_tracks(df):
    tracks = list_of_dicts_to_df(df.iloc[0]).set_index('z')

    for i in range(1, len(df)):
        particle = df.iloc[i]
        nwtrack = list_of_dicts_to_df(particle).set_index('z')
        tracks = tracks.add(nwtrack, fill_value=0)
    return tracks.div(tracks['ones'], axis=0)


def importdata(inputfile, orginal_df=False):
    # we open a saved dataframe, the number of the run of which we need the
    # data can be specified after the file name.
    listdata = []
    with (open(inputfile, "rb")) as file:
        while True:
            try:
                listdata.append(pickle.load(file))
            except EOFError:
                break
        
    
    data = pd.DataFrame(listdata)
    columnlist=['id', 'sd_hits', 'brem', 'real_vec', 'fitted', 'rec_vec',
                'rec_cov', 'seed_state', 'chi_sq']
    if len(data.columns)==10:
        columnlist.append('track')
    data.columns = columnlist
    '''
    if data['track'].isna().all():
        data.drop(['track'], axis=1, inplace=True)
    '''
    data = data[data.fitted == True]

    
    if orginal_df:
        return data
    data['rec_p'] = data['rec_vec'].map(lambda vec: vec.momentumGeV)
    data['real_p'] = data['real_vec'].map(lambda vec: vec.momentumGeV)
    data['del_p'] = data[['rec_p', 'real_p']].apply(
        lambda row: row['rec_p'] - row['real_p'], axis=1)
        
    data['rec_rel_p_err'] = data[['rec_cov', 'rec_vec']].apply(
        lambda row: (math.sqrt(abs(row['rec_cov'][4][4])) / row['rec_vec'].qp),
        axis=1)

    data['rec_p_err'] = data[['rec_rel_p_err', 'rec_p']].apply(
        lambda row: row['rec_rel_p_err'] * row['rec_p'], axis=1)
    
    data['del_p_over_err'] = data[['del_p', 'rec_p_err']].apply(
        lambda row: row['del_p']/row['rec_p_err'], axis=1
    )
    data['del_p_over_p'] = data[['del_p', 'real_p']].apply(
        lambda row: row['del_p']/row['real_p'],
        axis=1)
    
    return data.dropna()

    # build the new dataframe
    data['total_sd_hits'] = data['sd_hits'].map(lambda c: sum(c.values()))
    data['real_qp'] = data['real_vec'].map(lambda vec: vec.qp)
    data['rec_x'] = data['rec_vec'].map(lambda vec: vec.x)
    data['rec_y'] = data['rec_vec'].map(lambda vec: vec.y)
    data['rec_tx'] = data['rec_vec'].map(lambda vec: vec.tx)
    data['real_tx'] = data['real_vec'].map(lambda vec: vec.tx)
    data['real_x'] = data['real_vec'].map(lambda vec: vec.x)
    data['real_y'] = data['real_vec'].map(lambda vec: vec.y)
    data['rec_ty'] = data['rec_vec'].map(lambda vec: vec.ty)
    data['real_ty'] = data['real_vec'].map(lambda vec: vec.ty)

    data['seed_tx'] = data['seed_state'].map(lambda vec: vec.tx)

    data['seed_ty'] = data['seed_state'].map(lambda vec: vec.ty)

    data['charge'] = data['real_vec'].map(lambda vec: vec.q)
    data['real_eta'] = data['real_vec'].map(lambda vec: vec.eta)
    data['real_phi'] = data['real_vec'].map(lambda vec: vec.phi)
    data['del_x'] = data[['real_x', 'rec_x']].apply(
        lambda row: abs(row['real_x'] - row['rec_x']), axis=1
    )

    data['del_y'] = data[['real_y', 'rec_y']].apply(
        lambda row: abs(row['real_y'] - row['rec_y']), axis=1)

    data['del_tx'] = data[['real_tx', 'rec_tx']].apply(
        lambda row: abs(row['real_tx'] - row['rec_tx']), axis=1
    )

    data['del_ty'] = data[['real_ty', 'rec_ty']].apply(
        lambda row: abs(row['real_ty'] - row['rec_ty']), axis=1
    )

    data['seed_del_tx_over_tx'] = data[['real_tx', 'seed_tx']].apply(
        lambda row: abs(row['real_tx'] - row['seed_tx'])/row['real_tx'], axis=1
    )

    data['seed_del_ty_over_ty'] = data[['real_ty', 'seed_ty']].apply(
        lambda row: abs(row['real_ty'] - row['seed_ty'])/row['real_ty'], axis=1
    )

    data['real_p'] = data['real_vec'].map(lambda vec: vec.momentumGeV)
    data['rec_p'] = data['rec_vec'].map(lambda vec: vec.momentumGeV)

    data['rec_rel_p_err'] = data[['rec_cov', 'rec_vec']].apply(
        lambda row: (math.sqrt(abs(row['rec_cov'][4][4])) / row['rec_vec'].qp),
        axis=1)

    data['rec_p_err'] = data[['rec_rel_p_err', 'rec_p']].apply(
        lambda row: row['rec_rel_p_err'] * row['rec_p'], axis=1)

    data['del_p'] = data[['rec_p', 'real_p']].apply(
        lambda row: row['rec_p'] - row['real_p'], axis=1)

    data['del_p_over_p'] = data[['del_p', 'real_p']].apply(
        lambda row: row['del_p']/row['real_p'],
        axis=1)

    data['absdel_p_over_p'] = data['del_p_over_p'].map(lambda vec: abs(vec))

    data['seed_p'] = data['seed_state'].map(lambda vec: vec.momentumGeV)

    data['seed_del_p'] = data[['seed_p', 'real_p']].apply(
        lambda row: row['seed_p'] - row['real_p'], axis=1)

    data['seed_del_p_over_p'] = data[['seed_del_p', 'real_p']].apply(
        lambda row: row['seed_del_p']/row['real_p'],
        axis=1)

    data['seed_qp'] = data['seed_state'].map(lambda vec: vec.qp)

    data['seed_del_qp'] = data[['seed_qp', 'real_qp']].apply(
        lambda row: row['seed_qp'] - row['real_qp'], axis=1)

    data['seed_del_qp_over_qp'] = data[['seed_del_qp', 'real_qp']].apply(
        lambda row: row['seed_del_qp']/row['real_qp'],
        axis=1)

    data['rec_cov[4,4]'] = data[['rec_cov']].apply(
        lambda row: row['rec_cov'][4,4], axis=1)

    data['del_p_over_err'] = data[['del_p', 'rec_cov[4,4]']].apply(
        lambda row: row['del_p']/math.sqrt(abs(6.798651242111209e-06)), axis=1
    )

    return data.dropna()

    # a list of the most relevant data
    ['id', 'sd_hits', 'real_eta', 'real_phi', 'real_p', 'rec_p',
     'rec_rel_p_err', 'rec_p_err', 'del_p', 'del_p_over_p', 'absdel_p_over_p',
     'chi_sq']
