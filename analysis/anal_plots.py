import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import linregress, norm
from scipy.optimize import curve_fit
import pandas as pd
from mod.processes.brems import Bethe_Heitler_pdf


def makeplots(df, run):
    delpoverp(df, run)
    delphist(df, run)

    # we filter out the values the model itself thinks are not that good
    # df = df[abs(df.rec_rel_p_err) < 1]
    # df = df[df.chi_sq > 20]
    # delpoverp(df, run, 'selected')
    # delphist(df, run, 'selected')


def delphist_seed(dataframe, run):
    fig, ax = plt.subplots()
    df = dataframe
    del_p = df['seed_del_p_over_p']
    ax.hist(del_p)
    median = del_p.median()
    mean = del_p.mean()
    ax.set_xlabel("(rec_p - real_p)/real_p")
    ax.set_ylabel("number in bin")
    ax.set_title("Seed histogram del_p_over_p run " +
                 "{}".format(run))
    ax.axvline(x=median, c='k')
    ax.axvline(x=mean, c='y')
    ax.legend(['median = {:.5f}'.format(median)])
    ax.legend(['mean = {:.5f}'.format(mean)])
    fig.tight_layout()
    plt.savefig('analysis/plots/{}_seed_hist_delp_over_p'.format(run))


def delphist(dataframe, run, text='_over_p', bins=None, BH=False):
    fig, ax = plt.subplots()
    df = dataframe
    del_p = df['del_p{}'.format(text)]
    histogram, bins = ax.hist(del_p, bins, density=True)[:2]
    median = del_p.median()
    mean = del_p.mean()
    gaussf = lambda x, mu, sigma: norm.pdf(x, mu, sigma)
    #histdata = np.histogram(del_p, density=True)[0]
    

    # for i in range(240):
    #    del_p = np.delete(del_p, np.argmax(del_p))
    above_max = np.count_nonzero(del_p.gt(bins[-1]))
    below_min = np.count_nonzero(del_p.le(bins[0]))

    binsize = bins[1]-bins[0]
    x = np.linspace(bins[0]+binsize/2, bins[-1]-binsize/2, len(bins)-1)
    #fit a gaussian
    mu, sigma = curve_fit(gaussf, x, histogram)[0]
    print(curve_fit(gaussf, x, histogram))
    ax.plot(x, norm.pdf(x, mu, sigma))
    if BH:
        # fit a Bethe Heitler pdf but from -1 to 0 instead of 0 to 1
        Bethe_Heitler_result = lambda x, t: Bethe_Heitler_pdf(x+1, t)
        rad_frac = curve_fit(Bethe_Heitler_result, x[x<0], histogram[x<0],
                             p0=[0.5])[0][0]
        ax.plot(x[x<0], Bethe_Heitler_result(x[x<0], rad_frac))
    '''
    hist_asym = histogram[:len(histogram)//2+1] - histogram[len(histogram)//2:][::-1]
    ax2 = ax.twinx()
    ax2.plot(x[:len(hist_asym)], hist_asym)
    ax2.axhline(y=0)
    '''

    ax.set_xlabel(r"$\frac{\Delta p}{p}$")
    if text == '_over_err':
        ax.set_xlabel(r"$\frac{\Delta p}{\sqrt{cov(p,p)}}$")
    ax.set_ylabel("probability density")
    #ax.set_title("histogram $\Delta p$")
    ax.axvline(x=median, c='k')
    ax.axvline(x=mean, c='g')
    leg = ['$\mu$ = {:.3f}, $\sigma$ = {:.3f}'.format(mu, sigma),
               'median = {:.4f}'.format(median), 'mean = {:.4f}'.format(mean)]
    if BH:
        leg.insert(1,'Bethe Heitler pdf with t= {:.3f}'.format(rad_frac))
    ax.legend(leg)
    fig.tight_layout()
    plt.savefig('analysis/plots/{}_hist_delp{}'.format(run, text))

    '''
    fig, ax = plt.subplots()
    del_p = np.asarray(df['del_p'])
    # for i in range(240):
    #    del_p = np.delete(del_p, np.argmax(del_p))

    x_axis_min = -1
    x_axis_max = 1
    bins = np.linspace(x_axis_min, x_axis_max, num=50)
    ax.hist(del_p, bins)

    above_max = np.count_nonzero(del_p > x_axis_max)
    below_min = np.count_nonzero(del_p < x_axis_min)
    ax.set_xlabel("(rec_p - real_p)")
    ax.set_ylabel("amount in bin")
    ax.set_title("histogram del_p run " +
                 "{} {}. {} lower, {} higher".format(
                    run, text, below_min, above_max))
    plt.savefig('analysis/plots/{}_{}_hist_del_p'.format(run, text))
    '''


def delpoverp(dataframe, run, text=''):
    fig, ax = plt.subplots()
    df = dataframe
    bins = [1.75, 2.25, 2.75, 3.25, 3.75]
    bins.extend(range(5, 120, 5))

    x_values = [np.mean(bins[i:i+2]) for i in range(len(bins)-1)]
    bin_size = [(bins[i+1]-bins[i])/2 for i in range(len(bins)-1)]

    dfbins = pd.cut(df['real_p'], bins)
    absdel_p = df['del_p_over_p'].abs().groupby(dfbins).agg(['mean', 'sem'])
    ax.errorbar(
        x_values,
        absdel_p['mean'],
        yerr=absdel_p['sem'],
        xerr=bin_size,
        fmt='.k'
    )

    #ax.set_ylim([0, .1])
    ax.set_xlabel("Real p in GeV/c")
    ax.set_ylabel(r"$\frac{|\Delta p|}{p}$")
    ax.set_title(r"$\frac{|\Delta p|}{p}$")
    fig.tight_layout()
    fig.savefig('analysis/plots/{}_{}_delp_over_p'.format(run, text))


def scatter(
        df, run, values_or_names, labels=[None, None], text='', ylim=False, colours='b'):
    fig, ax = plt.subplots()
    values = values_or_names[:]
    for i in range(2):
        if type(values_or_names[i]) is str:
            values[i] = df.dropna()[values_or_names[i]]
            labels[i] = values_or_names[i]

    ax.scatter(
        values[0],
        values[1],
        s=2,
        c=colours
    )
    z = linregress(np.array(values[0]), np.array(values[1]))
    p = np.poly1d(z)
    ax.plot(values[0], z[1] + z[0] * values[0], "r--", label="y=%.6f+%.6f*x"%(z[1], z[0]))
    # the line equation:

    ax.axhline(c='k', ls='-')

    ax.set_xlabel(labels[0])
    ax.set_ylabel(labels[1])
    if ylim is not False:
        ax.set_ylim(ylim)
    ax.legend()
    ax.set_title("scatter plot for run {} {}".format(run, text))
    fig.savefig('analysis/plots/{}_scatter_{}_vs_{}'.format(
        run, labels[0], labels[1]))
